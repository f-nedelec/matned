function mask=mask_circle_safe(r)

mask=zeros(2*r+1,2*r+1);
rsq=r^2;

for x=-r:r
   for y=-r:r
      d=x^2+y^2;
      if (d <= rsq) 
         mask(1+x+r,1+y+r)=1;
      end
   end
end

return;