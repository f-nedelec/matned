function mask=mask_translate(imask, d, s)

% mask = mask_translate( imask, d , s )
%
% translate imask by d and fit it in a square of size s
% if s is not given, it is set to size(imask)

   
if (nargin < 3)
   s = size(imask);
end

mask = image_crop( imask, [d, (d+s-1)] , 1 );

return;
