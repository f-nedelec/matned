function result = teresa( filename, im );

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

if ischar( filename ) & ( nargin < 2 )
    im = tiffread( filename );
    %animate (im);
else
    im = filename;
    filename = 'file not specified';
end

if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

showim( im );
[poly, is_rect ] = ROImousepoly;
close;

%==================   extract the image from the ROI:

[mask, roi] = maskpolygon( poly );

if  ( is_rect == 1 )
    subim = crop( im, roi );
else
    subim = mask .* crop( im, roi );
end
    
base1       = min(min( im ));
base2       = imbase( im );


%==================   make a summary figure:

figid=figure('Name', ['file ', filename], 'Position', [ 100, 100, 800, 400 ] );
set(gca, 'Position', [0.05 0.05 0.45 0.95]);
showim( subim, 'nofigure', 'axisticks', 'trueratio' );

total_intensity = 10^-5 * ( sum(sum( subim  )) - base2 * sum(sum( mask )) );
max_intensity = max(max( subim ));
min_intensity = min(min( subim + max_intensity * ( 1 - mask ) ));

figure(figid);
axes( 'Position', [ 0.51 0.13 0.4 0.75 ] );
title( filename, 'FontSize', 14, 'Interpreter', 'none' );

mess1 = sprintf('corrected total %5.1f  10^5 a.u.\n', total_intensity);
mess2 = sprintf(' max intensity  %5.0f  a.u.\n', max_intensity);
mess3 = sprintf('black level     %5.0f  a.u.\n', base2);
mess4 = sprintf(' min intensity  %5.0f  a.u.\n', min_intensity);
text( 0 , 0.82, [mess1, mess2, mess3, mess4], 'FontSize', 14, 'FontName', 'fixedwidth');
axis off;

axes('Position', [0.58 0.1 0.4 0.5]);
%title( 'sub-image histogram', 'FontSize', 10 );

if ( is_rect == 1 )
    sh = imhist( subim );
else
    sh = imhist( subim, mask );
end

plot( sh, 'b');
mxsh = max( sh );
hold on;
plot( [base2, base2], [ 0, mxsh ], 'k:');
ylabel( 'pixel count', 'FontSize', 12 );
xlabel( 'pixel value', 'FontSize', 12 );

return