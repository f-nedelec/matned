function oname = filename( name , w )

if (strcmp(w,'motor'))
   oname = name;
end

if (strcmp(w,'MT'))
   oname = strrep( name, 'FITC', 'XRhod');
end

if (strcmp(w,'eps'))
   oname = strrep( name, '.tif', '.eps');
end

if (strcmp(w,'data'))
   oname = strrep( name, '.tif', '-4.data');
end

return