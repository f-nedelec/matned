function [ c1, c2 ] = dotfind3( im )

% tries to split the image in two dots...

if ( isfield( im, 'data' ) ) im = im.data;  end
global debug minPixels maxPixels;

%base = valuetop( im, 2*minPixels );
base = opthr( im ); 
base = valuetop( im, maxPixels );
[cen, mom, rot, sv] = barycenter2( im > base  );

dx = rot(2) * cos( rot(1) );
dy = rot(2) * sin( rot(1) );
c1 = cen + [ dx dy ];
c2 = cen - [ dx dy ];

for i=1:10
   
   oc1 = c1;
   oc2 = c2;
   
   %split the image into 2, and find the barycenter in each half:
   a = ( c1 + c2 ) / 2;
   b = ( c1 - c2 );
   b = [ -b(2), b(1) ];
   
   [l, r] = imsplit( im, a+b, a-b );
   
   base = valuetop( l, minPixels );
   c1 = barycenter1( ( l > base ) .* l );
   
   base = valuetop( r, minPixels );
   c2 = barycenter1( ( r > base ) .* r );
   
   %test convergence:
   
   if ( dist( oc1, c1 ) < 1 ) & ( dist( oc2, c2 ) < 1 )
      break;
   end
   
   
end

if (debug==3)
   show(im);
   plot(c1(2), c1(1),'bx','MarkerSize',10,'Linewidth',3);
   plot(c2(2), c2(1),'rx','MarkerSize',10,'Linewidth',3);
end


return;

function x = dist(c, d)
  x = sqrt( (c(1)-d(1))^2 + (c(2)-d(2))^2 );
return
