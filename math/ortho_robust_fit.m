function [ fit, score ] = ortho_robust_fit(PX, PY, verbose)

%function [ fit, score ] = ortho_robust_fit(PX, PY, verbose)
%
% returns the line fitting best the cloud of points (PX, PY)
% PX and PY should be vectors of identical dimensions:
%   x1 x2 x3 ... xn
%   y1 y2 y3 ... yn
%
% This implements robust orthogonal fitting, where the sum of the absolute values
% of the distances of the points to their projection on the line is minimized.
%
% The best line is returned as:
% fit = [angle, px, py];
% where `angle` determines the direction of the line: dir = [cos(angle) sin(angle)]
% and [px, py] is a point of the line.
%
% The goodness of fit for this particular line is returned in `score`.
%
% built after Serge Dmitrieff's ortho_robust_coeff2
% F. Nedelec 7.09.2017

if nargin < 3
    verbose = 0;
end

if any( size(PX) ~= size(PY) )
    error('missmatch in size: X and Y should have similar size');
end

cen = [mean(PX, 1), mean(PY, 1)];

if numel(cen) ~= 2
    error('unexpected argument size');
end

    function res = residual(arg)
        d = [cos(arg(1)), sin(arg(1))];
        a = cen + arg(2) * [-d(2), d(1)];
        h = ( PX - a(1) )*d(2) - ( PY - a(2) )*d(1);
        % robust fitting:
        res = sum(abs(h));
        % for standard fitting, use this:
        % res = sum(h.^2);
    end

[coef, score] = fminsearch(@residual, [0,0]);

angle = coef(1);
offset = coef(2);

dir = [cos(angle), sin(angle)];
ori = cen + offset * [-dir(2), dir(1)];

fit = [angle, ori(1), ori(2)];

if verbose > 0
    
    figure('Position', [20 20 768 768]);
    s = scatter(PX, PY, 64, 'b');
    
    s.MarkerEdgeColor = [1 1 1];
    s.MarkerEdgeAlpha = 0;
    
    s.MarkerFaceColor = [0 0 1];
    s.MarkerFaceAlpha = 0.1;

    hold on;
    s = scatter(cen(1), cen(2), 256, 'k');
    s.MarkerEdgeColor = [1 1 1];
    s.MarkerEdgeAlpha = 1;
    
    s.MarkerFaceColor = [0 0.5 0];
    s.MarkerFaceAlpha = 1;

    axis equal
    %axis([-1.0 4 -3.5 1.5]);

    dif = [max(PX) - min(PX), max(PY) - min(PY)];
    len = sqrt(sum(dif.^2));
    
    % plot best line fit
    lin = vertcat(ori - len * dir, ori + len * dir);
    plot(lin(:,1), lin(:,2), '-', 'LineWidth', 2);
    
    % plot line fit going through data center:
    lin = vertcat(cen - len * dir, cen + len * dir);
    plot(lin(:,1), lin(:,2), '--', 'LineWidth', 2);
    slope = tan(angle);
    title(['fit angle = ', num2str(angle), ';  slope = ', num2str(slope)]);
    % plot linear regression
    %poly = polyfit(PTS(:,1), PTS(:,2), 1);
    %val = [min(PTS(:,1)) max(PTS(:,1))];
    %plot(val, polyval(poly,val), ':', 'LineWidth', 1);
    
end
if verbose > 1
    
    % plot residual as a function of the angle
    val = 0:0.01:pi;
    res = zeros(size(val));
    for i = 1:length(val)
        res(i) = residual([val(i), 0]);
    end
    axes('Position', [0.06 0.825 0.2 0.15]);
    plot(val, res);
    xlim([0 pi]);
    xlabel('Angle (radian)');
    ylabel('Residual');

    % plot residual as a function of the offset
    val = -1:0.01:1;
    res = zeros(size(val));
    for i = 1:length(val)
        res(i) = residual([angle, val(i)]);
    end
    axes('Position', [0.06 0.625 0.2 0.15]);
    plot(val, res);
    xlabel('Perpendicular offset');
    ylabel('Residual');
    
end
if verbose > 2
    
    % display orthogonal projections of all data points:
    a = ( PX-ori(1) )*dir(1) + ( PY-ori(2) )*dir(2);
    pX = ori(1) + a * dir(1);
    pY = ori(2) + a * dir(2);
    for n = 1 : length(PX)
        line([pX(n), PX(n)], [pY(n), PY(n)]);
    end
    
end

end



