function spin_plot_length_fig()

% function spin_plot_length_fig
%
% F. Nedelec, Feb. 2009
%
%


X = 20:1:46;

%200109_2.3pg
%Length = 36.18 +/- 5.17 (N=52)
h1 = [ 0 0 0 1 0 0 0 1 4 0 1 2 4 3 5 6 6 2 3 3 4 2 0 2 1 0 0 ];

%210109_2.3pg
%Length = 33.93 +/- 4.99 (N=22)
h2 = [ 0 0 0 0 0 1 0 0 3 1 3 1 1 0 4 2 1 1 2 0 0 0 1 0 0 0 0 ];

%290109_23pg
%Length = 35.88 +/- 4.43 (N=204)
h3=[ 0 0 0 0 0 1 2 4 1 10 6 13 17 16 15 25 17 15 17 19 4 7 4 3 1 0 0 ];


%280109_23pg (WEIRD)
%Length = 40.24 +/- 5.48 (N=70)
h4=[ 0 0 0 0 0 0 0 0 1 3 0 0 2 0 3 3 5 9 3 6 9 3 1 6 5 1 0 ];


%210508_05pg
%Length = 30.98 +/- 3.56 (N=107)
h5 = [ 0 0 1 3 2 4 3 8 7 13 5 23 11 7 11 3 1 1 1 1 1 1 0 0 0 0 0 ];

%230508_05pg
%Length = 32.20 +/- 4.17 (N=200)
h6 = [ 1 0 1 3 5 10 11 5 7 13 16 19 20 14 22 18 11 10 7 3 3 0 0 0 1 0 0 ];


ha = h1+h2+h3;
hb = h5+h6;


figure('Toolbar', 'none', 'Position', [100 100 800 500]);
h = bar(0.5+X, cat(2, ha', hb'), 1.6);
hold on;

Xfin = 20:0.25:46;

[ma, sa] = mean_std(ha, X);
fita = exp( -((Xfin-ma)/sa).^2 ./ 2 );
fita = fita * ( sum(ha) * length(Xfin) / (length(X)*sum(fita)));
plot(Xfin, fita, 'k--');
g=0.0; set(h(1), 'FaceColor', [g g g], 'LineWidth', 1);
fprintf('Length = %f +/- %f  (N=%i)\n', ma, sa, sum(ha));


[mb, sb] = mean_std(hb, X);
fitb = exp( -((Xfin-mb)/sb).^2 ./ 2 ); 
fitb = fitb * ( sum(hb) * length(Xfin) / (length(X)*sum(fitb)));
plot(Xfin, fitb, 'k--');
g=0.9; set(h(2), 'FaceColor', [g g g], 'LineWidth', 1);
fprintf('Length = %f +/- %f  (N=%i)\n', mb, sb, sum(hb));



legend('2.3pg DNA/bead', '0.5pg DNA/bead');


set(gca, 'FontSize', 16);
set(gca, 'Position', [0.2 0.2 0.6 0.6]);

xlabel('Length / um', 'FontSize', 16, 'FontWeight', 'bold');
ylabel('Count', 'FontSize', 16, 'FontWeight', 'bold');

%title('Spindle length distributions', 'FontSize', 20);
xlim([min(X), max(X)]);

    function [m,s] = mean_std(h, X)
        m = sum(h.*(X+0.5) / sum(h));
        v = sum(h.*(X+0.5).^2 / sum(h));
        s = sqrt( v - m^2 );
    end

end