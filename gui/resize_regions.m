function res = resize_regions(regions, rsize)

% regions = resize_regions(regions, rsize)
%
% load the regions from the file 'regions.txt'
%
% F. Nedelec, Dec. 2013


cX = ( regions(:,2) + regions(:,4) ) / 2;
cY = ( regions(:,3) + regions(:,5) ) / 2;

sX = rsize(1) / 2;

if numel(rsize) > 1
    sY = rsize(2) / 2;
else
    sY = sX;
end

res = cat(2, regions(:,1), cX - sX, cY - sY, cX + sX, cY + sY);