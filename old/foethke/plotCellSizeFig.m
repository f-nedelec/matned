function plotCellSizeFig()

% 
% F. Nedelec

wt  = load('wt.txt');
tea = load('tea1.txt');

%cell width:
bins = 2.5:0.25:5;

h_wt  = histc( unique(wt(:,7)), bins );
h_tea = histc( unique(tea(:,7)), bins );

figure
h = bar(bins, cat(2, h_wt, h_tea), 1.8);
axis([2.3 4.5 0 20]);

xlabel('Cell width (micro-meters)', 'FontSize', 16, 'FontWeight', 'Bold');
ylabel('Cell count', 'FontSize', 16, 'FontWeight', 'Bold');
set(gca, 'FontSize', 16);
set(gca, 'Position', [0.2 0.2 0.6 0.6]);

set(h(1), 'FaceColor', 'w', 'LineWidth', 1);
set(h(2), 'FaceColor', 'k', 'LineWidth', 1);
%title('Cell Width', 'FontSize', 18);
legend('wild-type', 'tea1\Delta');


%bar(bins, h_tea,'b');
%h = findobj(gca,'Type','patch');
%set(h,'FaceColor', 'g', 'EdgeColor', 'w');


%cell length:
bins = 8:2:16;

h_wt  = histc( unique(wt(:,8)), bins );
h_tea = histc( unique(tea(:,8)), bins );


figure
h = bar(bins, cat(2, h_wt, h_tea), 1.8);
axis([7 16 0 20]);

xlabel('Cell length (micro-meters)', 'FontSize', 16, 'FontWeight', 'Bold');
ylabel('Cell count', 'FontSize', 16, 'FontWeight', 'Bold');
set(gca, 'FontSize', 16);
set(gca, 'Position', [0.2 0.2 0.6 0.6]);

set(h(1), 'FaceColor', 'w', 'LineWidth', 1);
set(h(2), 'FaceColor', 'k', 'LineWidth', 1);
%title('Cell Length', 'FontSize', 18);

legend('wild-type', 'tea1\Delta');