function [ imf, bim ] = filtermovie1( im )

%[ imf, bim ] = filtermovie1( im )
%
%

%define ROI by mouse clicks:
poly = ROImousepoly( im(1) );
[mask, roi] = maskpolygon( poly );


% compute the background from some of the images:

i = 1;
bim = zeros( size(mask) );

for indx = 1:length(im)
   %h(i) = histmin( imhist( im(indx).data ) );\
   h(i) = double( min(min( im(indx).data )) );
   
   im2  = double( image_crop( im(indx).data, roi ) );
   bim  = bim + im2;
   
   i = i + 1;
end

backgd = mean( h );
bim = bim / length(im) - backgd;

% make the mask of the embryo:
base   = image_background( bim );
mask2  = bim > ( 0.7 * base );
bim    = bim + base/2;

bim = filtergaussian( bim, 2000 );

round( [ backgd base ] )
% filter each frame:

for indx = 1:length(im)
   
   im2  = double( image_crop( im(indx).data, roi ) );
   im2  = ( im2 - backgd ) .* mask;
   im2  = im2 .* ( im2 > 0 );
   im2  = medfilt2(im2,'symmetric');
      
   imf(indx).data = 255 * ( im2 ./ bim );   
end
