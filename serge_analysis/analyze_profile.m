function [ints,i,j] = analyze_profile( profile)
% Analyses an intensity profile yielding intens. and width of highest peak
%   Does smooth stuff
% S. Dmitrieff, december 2012
ints=0;
wid=0;
if nargin<1 
    error('No profile given')
end
lp=length(profile);
midle=ceil(lp/2);

% ----------------------->
% thresholding  :
% ----------------------/
smp=smooth(profile,5);
[mp,pos]=max(smp);
ct_left=(mp - min(smp(1:pos)))*0.2;
ct_right=(mp - min(smp(pos:end)))*0.2;
i=pos;
while smp(i)>ct_right && i<lp
    i=i+1;
end
j=pos;
while smp(j)>ct_left && j>1
    j=j-1;
end
wid_T=abs(i-j);
ints_T=sum(profile(j:i));
mea_T=(i+j)/2 - midle ;

% ----------------------->
% correlation length :
% ----------------------/
corr_func=correlation_function(profile,lp/2);
i=first_zero(corr_func);
mea_C=0;
wid_C=i;

% ------------------------>
% finding edges
% -----------------------/
smp=smooth(profile,8);
dmp=diff(smp);
[~,j]=max(dmp);
[~,i]=min(dmp);
wid_E=(i-j)*2;
ints_E=sum(profile(j:i));
mea_E=(i+j)/2 - midle ;


% ----------------------->
% adaptative thresholding  : start small
% ----------------------/
sm_size=0;
sm_step=1;
sm_max=15;
thresh=0.25;
j=sm_size+sm_step;
i=0;
while abs(j-i) > 2.5*sm_size && sm_size<sm_max
    sm_size=min(sm_size+sm_step,sm_max);
    smp=smooth(profile,sm_size);
    smp=max(smp-image_background(smp),0);
    [i,j]=find_boundaries(smp,thresh);
end
ints=sum(profile(j:i));
i=i-lp/2;
j=j-lp/2;




% ----------------------------------
% Checking if thresholding didn't mess up
% ----------------------------------------

if abs(i-j)>sqrt(2)*wid_C
    i=+wid_C/2;
    j=-wid_C/2;
    ints=sum(profile((lp/2-wid_C):(lp/2+wid_C)));
end

if abs((i+j)/2) > abs(j-i)
    wid=abs(j-i);
    i=+wid/2;
    j=-wid/2;
    ints=sum(profile((lp/2-wid):(lp/2+wid)));
end


% -----------------------------
% ---------------------
% -----------


    function [i,j]=find_boundaries(prof,thresh)
    [mp,pos]=max(prof);
    ct_left=(mp - min(prof(1:pos)))*thresh;
    ct_right=(mp - min(prof(pos:end)))*thresh;
    i=pos;
    while smp(i)>ct_right && i<lp
        i=i+1;
    end
    j=pos;
    while smp(j)>ct_left && j>1
        j=j-1;
    end
    
    
    end
        

end
