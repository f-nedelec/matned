
function analyze_dirs(arg, varargin)
%
% call another program (eg. analyzeSIM) in multiple directories
% 
% Examples:
% analyze_dirs('run*');
% analyze_dirs('run0013');
%
% F.Nedelec 13.Nov.2014 updated 4.4.2014

if nargin < 1
    arg = 'run*';
end

paths = {};

if isdir(arg)
    paths{1} = fullfile(pwd, arg);
else
    p = fileparts(arg);
    d = dir(arg);
    for i = 1:size(d,1)
        paths{i} = fullfile(pwd, p, d(i).name);
    end
end


hdir = pwd;

for i = 1:length(paths)
    
    dst = paths{i};
    
    if isdir(dst)
        cd(dst)
        fprintf(1, '================== Visiting %s\n', dst);
        %analyzeSIMposition(varargin{:}, 'output', '../analyze.txt');
        %analyzeSIMbackup(varargin{:}, 'output', '../analyze_old.txt');
        analyzeSIMangle(varargin{:}, 'output', '../angle_analyze.txt');
        cd(hdir);
    end    
end

end

