function [ Z, Xedges, Yedges ] = histc2d( P, Xedges, Yedges )

% function [ Z, Xedges, Yedges ] = histc2d(P)
% function [ Z, Xedges, Yedges ] = histc2d(P, Xedges, Yedges)
%
% calculate a 2D histogram of data points P along bins specified by Xedges,
% and Yedges
%
% P should be specified by lines as P(n,:) = { X_n, Y_n }
% or with additional Z components P(n,:) = { X, Y, Z1, Z2, Z3 ... }
%
% Z(i,j,1) returns the number of point P in bin (i,j),
% which are the points satisfying:
%       Xedges(j) <= P(:,1) < Xedges(j+1)
%       Yedges(i) <= P(:,2) < Yedges(i+1)
% Z(i,j,n+1) returns the average of P(:,2+n) in bin (i,j)
% 
% Points outside the outer edges are not counted
% if Xedges and Yedges are ommited, they are calculated automatically,
% from the min/max values of the data, to have 16 points/bin on average
%
% F. Nedelec, September 2007, August 2008

if size(P,2) < 2
    error('data should be an array P(n,:) = { X, Y, Z1, Z2...}');
end

if nargin == 2
    error('none, or both X and Y edges must be provided');
end

%% Calculate the edges if they are not provided

if nargin<2 || isempty(Xedges) || isempty(Yedges)
    
    Xmin = min(P(:,1));
    Xmax = max(P(:,1));
    
    Ymin = min(P(:,2));
    Ymax = max(P(:,2));
    
    %choose the grid to have 16 data-points in each bin, on average
    n = floor( sqrt( size(P,1) / 16 ) );
    
    Xedges = Xmin:(Xmax-Xmin)/n:Xmax;
    Yedges = Ymin:(Ymax-Ymin)/n:Ymax;

    %expand the last bin, to include the max-value
    Xedges(1,size(Xedges,2)) = Xmax + 0.001*(Xmax-Xmin)/n;
    Yedges(1,size(Yedges,2)) = Ymax + 0.001*(Ymax-Ymin)/n;
    
end

%% calculate 2D histogram

[Xn, Xbin] = histc(P(:,1), Xedges);
[Yn, Ybin] = histc(P(:,2), Yedges);

Xmax  = size(Xn,1);
Ymax  = size(Yn,1);

XYmin = Ymax + 1;
XYmax = Ymax * Xmax + Ymax;

XYbin = Ymax * Xbin + Ybin;
%XYbin = Xmax * Ybin + Xbin;

%remove out-of-range data:
out = find( Xbin == 0 | Ybin == 0 );
XYbin(out) = zeros(size(out));

psz  = size(P,2);

%calculate the 2D-histogram:
Zm  = zeros(Xmax*Ymax,  psz-1);

if psz > 2
    for b = XYmin:XYmax
        
        se = find(XYbin == b);
        ix = b - XYmin + 1;
        
        nbe = length(se);
        if ( nbe > 0 )
            Zm(ix,:) = [ nbe, mean( P(se, 3:psz), 1 ) ];
        end
    end
else
    Zm = histc(XYbin, XYmin:XYmax);
end

%% reshape data for output
Z = zeros(Ymax, Xmax, psz-1);

for c = 1:psz-1
    Z(1:Ymax, 1:Xmax, c) = reshape(Zm(:,c), Ymax, Xmax);
end


end
