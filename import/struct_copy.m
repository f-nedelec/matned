function [s_out] =  struct_copy(s_in,I)
% STRUCT_COPY: copy specified elements of structure to a new structure
%
% Usage: [S_OUT] =  struct_copy(S_IN,I)
%
%
% Copies all fields of struct S_IN into struct S_OUT.  For each field the
% values corresponding to the index I are copies, for example:
%
% foo = 
%
%    x: [1.00 2.00 3.00]
%    y: 'abc'
%
%> struct_copy(foo,[1 3]) 
%
% ans = 
%
%    x: [1.00 3.00]
%    y: 'ac'
%


% copyright 1999-2000 Adam M Kornick
% akornick@whgrp.com
%
% 11/22/1999
% This file is licensed under the GPL, see 
% http://www.gnu.ai.mit.edu/copyleft/gpl.html and the information at the
% bottom of this file for more information

if( nargin ~= 2)
  error('Usage: [s_out] =  struct_copy(s_in,I)');
end

% get fieldnames
fn = fieldnames(s_in);

% copy from old structure to new structure from 1:I each feld
for i=1:length(fn);
  eval(...
      ['s_out.' char(fn(i)) ' = s_in.' char(fn(i)) '(I);'],...
      ['s_out.' char(fn(i)) ' = s_in.' char(fn(i)) '(1:end);']...
      );
  
  
end

% end function  
%
%This program is free software; you can redistribute it and/or
%modify it under the terms of the GNU General Public License
%as published by the Free Software Foundation; either version 2
%of the License, or (at your option) any later version.
%
%This program is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU General Public License for more details.
%
%You should have received a copy of the GNU General Public License
%along with this program; if not, write to the Free Software
%Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
