function reg = register_spindle( filename, im )

% function spindle( filename, im )
% Last updated August 17, 2003

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

if ( nargin <= 1 )
    if ischar( filename )
        im        = tiffread_buf( filename );
    else
        im        = filename;
        filename  = 'file not specified';
    end
end

if ( isfield(im, 'data') ) 
    pixels = double( im.data ); 
end

%==================find the background level:

%rect       = ROImouserect;
%blackarea  = crop( pixels, rect );
%base1      = mean( mean( blackarea ));
base1      = min(min( pixels ));
base2      = imbase( pixels );
%disp( sprintf('base = %i,   %i', round(base1), round(base2) ) );

%==================   select a rectangular ROI:

showim( pixels );
drawnow;
%showim( pixels );

%roi   = ROImouserect;
poly = ROImousepoly;
[mask, roi] = maskpolygon( poly );
%showim( mask );
close;

%==================   extract the image from the ROI:
%subim  = crop( pixels, roi ) .* mask;

%base1s      = min(min( subim ));
%base2s      = imbase( subim );

subim  = ( crop( pixels, roi ) - base2 ) .* mask;
subim  = subim .* ( subim > 0 );

%==================   fit an ellipse to the sub-image

[cen, mom, rot, sv] = barycenter2( subim );

degres = - rot(1) * 180.0 / pi;
imrot = imrotate( subim, degres, 'bilinear' );

%showim( imrot );
[cen, mom, rot, sv] = barycenter2( imrot );

%disp(sprintf('angle = %.3f\n', rot(1)));
%disp(sprintf('centre = %.3f %.3f\n', cen(1), cen(2)));

bbox = [ round(cen)-70, round(cen)+70 ];
reg = crop( imrot, bbox, 1 );
reg = uint16( reg );
%showim(reg);
return;

