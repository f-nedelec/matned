function [ base, value ] = valuetop ( im, nbpixel )

h  = imhist( im );
ch = cumsum( h );
ch = ch( length(ch) ) - ch;

base = max( find( ch >= nbpixel ) ) - 1;

if ( isempty( base ) ) base = length( h ) - 1; end;

%the mean value of the pixels above the base:

value = [base:length(h)-1] * h( base+1:length(h) );
value = value ./ sum( h(base+1:length(h)) );

return
