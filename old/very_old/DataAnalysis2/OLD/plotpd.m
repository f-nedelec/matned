function plotpd()

load p;
load d;

figure('Position',[20 530 550 450],'MenuBar','None');
set(gca,'Position',[0.05 0.05 0.9 0.9]);

plot(p(:,1), d(:,1), 'bs', 'LineWidth',1);
hold on;

return