function [ bright, radius ] = astersize1( im, cen1 )


%-------------------------measure the profiles of fluorescence:
bin    = 2;
radius = 20;

grad   = round( maskgrad1(radius, bin) );

bright = zeros( length(im), 1);
diamet = zeros( length(im), 1);

figure(123);
for indx=1:length(im)
   
   [m1, s, c] = computeprofile1( im(indx).data, cen1(indx,:), bin, radius, grad );
   
   plot(m1);
   hold on;
   bright( indx ) = sum( m1(1:5) );
   diamet( indx ) = 0;
   
end