function gotha4_cut( files, groups )

for gi = 1:size(groups,1)
    
    group = groups(gi,:);
    indx = find( ~cellfun('isempty', group) );
    group = group(indx);

    [ collect, names, master ] = gohta4( group );

    tline = title_line(group);
    fprintf(1,'group %i:\n', gi);

    figure(gi);
    gohta4_plot( collect, names, master );
    title( tline );
    set(gcf, 'Name', sprintf('group %i', gi));
    drawnow;

    saveas(gcf, sprintf('image%02i.jpg', gi));
    
end


%--------------------------------------------------------------------------
function result = title_line(group)
result = '';
merge  = '';
v = 1;
for n=1:size(group,2)
    merge = [ merge, char(group(1,n)), ' ' ];
    if mod( n, 13 ) == 0
        result( v, 1:size(merge,2) ) = merge;
        merge = [];
        v = v + 1;
    end
end
if ~ isempty( merge )
    result( v, 1:size(merge,2) ) = merge;
end
return;
