function analyzeLM(filename, varargin)
%
% Modified 28.2.2015 : log-log plot have equal axes

params = struct('showplot',0,'savefig',0,'output','');
params = inputArgsToStruct(params, varargin);

%% Load data
data = load(filename);

% Clean data
data = data(:,1:4);

% Get number of time frame
nFrames = max(data(:,2)) - min(data(:,2)) + 1;

% Set dt in seconds - !! Adapt to movie !!
dt = 30; %s

% Get total time
totTime = (nFrames-1)*dt;
totTime_min = (nFrames-1)*dt/60;

%%

% Get mean axis
pos0 = data(logical(data(:,1)==0), 3:4);
pos2 = mean(data(logical(data(:,1)==1), 3:4), 1);

vec = pos2 - pos0;
vec_norm = sqrt(sum(vec.^2));
axe = vec / vec_norm;

ROT = [ axe(1) axe(2); -axe(2) axe(1) ];

%% Get number of nuclei

nrNuclei = max(data(:,1));

% Scale to pixel size - !! Adapt to movie !!
pixelSize = 0.17;

% Reoder the data
for i = 1:nrNuclei
    
    tmp = data(logical(data(:,1)==i), 2:4);

    pos = zeros(size(tmp,1), 2);
    for j = 1:length(tmp)
        %rotational matrix along hyphae axis
        pos(j,:) = ( tmp(j,2:3) - pos0 ) * ROT';
    end
    
    try
        pos(:,1) = smooth(pos(:,1),'sgolay') * pixelSize;
        pos(:,2) = smooth(pos(:,2),'sgolay') * pixelSize;
    catch
        pos(:,1) = pos(:,1) * pixelSize;
        pos(:,2) = pos(:,2) * pixelSize;
    end   
        
    nucleus{i} = [ tmp(:,1), pos(:,1), pos(:,2) ];
end


%% Get mean number of nuclei

cnt = 0;
for i = 2:nrNuclei
    cnt = cnt + length(nucleus{i});
end
avg_nrNuclei = cnt / nFrames;

%% Calculation of growth speed

growth_speed = sqrt(sum((nucleus{1}(end,2:3)-nucleus{1}(1,2:3)).^2))/totTime;

%% Compute forward, backward and tumbling events

speeds_events = [];
forward_events = [];
backward_events = [];
tumbling_events = [];

thr = growth_speed;

% attention: index 1 is the tip of the hyphae
for i = 1:nrNuclei

    speed{i} = sqrt(sum(diff(nucleus{i}(:,2:3)).^2, 2)) / dt;
    directionX{i} = sign(diff(nucleus{i}(:,2)));

    speeds_events = [ speeds_events; speed{i} ];
    %a_speed(i) = mean(speed{i});
    
    %exclude the hyphae tip which is at index 1
    if i==2
        speeds_events = [];
        forward_events = [];
        backward_events = [];
        tumbling_events = [];
    end
    
    % categorize motion:
    forwards{i}  = directionX{i}.*speed{i}  >= thr;
    [ swi, ~, val ] = find( diff(forwards{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'forward %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_forwards(i) = length(mov);
    forward_events = [ forward_events; mov ];
   
    tumblings{i} = (directionX{i}.*speed{i} < thr) & ( directionX{i} >= 0 );
    [ swi, ~, val ] = find( diff(tumblings{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'tumbling %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_tumblings(i) = length(mov);
    tumbling_events = [ tumbling_events; mov ];
    
    backwards{i} = directionX{i} < 0;
    [ swi, ~, val ] = find( diff(backwards{i}) );
    dur = diff(swi);
    mov = dur( logical(val(2:end)<0) ) * dt;
    %fprintf(1, 'backward %9.2f %9.2f %9.2f\n', length(mov), mean(mov), max(mov));
    nb_backwards(i) = length(mov);
    backward_events = [ backward_events; mov ];
    
end

%% Compute movements maximum speed

maxspeed = max(speeds_events);
maxspeed_min = maxspeed*60;
avgspeed_min = mean(speeds_events)*60;

%% Forward, backward and tumbling total number, average duration and maximum duration of events

totNb_forwards = numel(forward_events)/(avg_nrNuclei*totTime_min);
totAvgDur_forwards_min = mean(forward_events)/60;
maxDur_forwards_min = max(forward_events)/60;

totNb_tumblings = numel(tumbling_events)/(avg_nrNuclei*totTime_min);
totAvgDur_tumblings_min = mean(tumbling_events)/60;
maxDur_tumblings_min = max(tumbling_events)/60;

totNb_backwards = numel(backward_events)/(avg_nrNuclei*totTime_min);
totAvgDur_backwards_min = mean(backward_events)/60;
maxDur_backwards_min = max(backward_events)/60;

mvt_ratio = totNb_forwards/totNb_backwards;

%% Compute bypassing events

% create asymmetric matrix to count the events
mat_bypass = zeros(nrNuclei, nrNuclei);

for i = 1:nrNuclei
    for j = i+1:nrNuclei
        %find frame where two nuclei are present:
        common = intersect( nucleus{i}(:,1), nucleus{j}(:,1) );
        ix = logical(ismember(nucleus{i}(:,1), common));
        jx = logical(ismember(nucleus{j}(:,1), common));
        dx = nucleus{j}(jx,2) - nucleus{i}(ix,2);
        events = diff(sign(dx))/2;
        for e = 1:length(events)
            if events(e) 
                fprintf(1, 'nuclei %i %i cross at time %.2f\n', i, j, e*dt/60);
            end
        end
        mat_bypass(i,j) = mat_bypass(i,j) + sum(abs(events));
    end
    clear('ix', 'jx', 'dx');
end
totNb_bypass = sum(sum(mat_bypass)) / ( avg_nrNuclei * totTime_min );
nb_bypass = sum(mat_bypass + mat_bypass', 2);



%% Plot
if(params.showplot)

    color = [ 0 0 0; 0.9 0 0; 0 0.9 0; 0.7 0.7 0; 0 0 1;...
        1 0 1; 0.5 0.5 0.5; 1 0 0.5; 0.1 0.7 0.5; 0.5 0 0.7;...
        0.5 0.7 0; 0 0.5 0.7; 0.7 0.5 0.7; 0.9 0.5 0 ];
    
    toplot = 1:nrNuclei;
    figsize1 = [0 0 400 400];
    figsize2 = [0 0 400 80*nrNuclei];
    figsize3 = [0 0 160*nrNuclei 80*nrNuclei];
    figsize4 = [0 0 800 400];
    
    % Nuclei overall movements
    h1 =setupFigure(1,figsize1);
    
    for i = 1:nrNuclei
        plot(nucleus{i}(:,2), nucleus{i}(:,1)*dt/60,'color',color(i,:));
        text(nucleus{i}(end,2), nucleus{i}(end,1)*dt/60, num2str(i));
        set(gca,'YDir','rev');
    end
    title ('Nuclei overall movements', 'FontWeight', 'bold');
    xlabel('Nucleus position (\mu m)');
    ylabel('Time (min)');
    
    % Patterns of forward, backward and tumbling events
    h2 =setupFigure(2,figsize2);
    for i = toplot
        subplot(nrNuclei,1,i);
        plot(gca, nucleus{i}(1:end-1,1)*dt/60, forwards{i}-backwards{i},'color',color(i,:));
        set(gca, 'YLim', [-1.5 1.5], 'YTick', [-1, 0, 1], 'YTickLabel', {'-', '0', '+'});
        title(['Nucleus ' num2str(i)], 'FontWeight', 'bold');
    end
    xlabel('Time (min)', 'FontWeight', 'bold');
    
    % Speed of movements
    h3 =setupFigure(3,figsize2);
    for i = toplot
        subplot(nrNuclei,1,i);
        plot(nucleus{i}(1:end-1,1)*dt/60, speed{i}(1:end),'color',color(i,:));
        title(['Nucleus ' num2str(i)], 'FontWeight', 'bold');
    end
    xlabel('Time (min)', 'FontWeight', 'bold');
   
    % Number of forward, backward, tumbling and by passing events
    h4 =setupFigure(4,figsize4);
    for i = toplot
        subplot (1,4,1)
        plot(nb_forwards(i),'s','color',color(i,:));
        ylabel('Number of events (#)');
        xlabel('Forward events');
        hold on
        subplot (1,4,2)
        plot(nb_backwards(i),'s','color',color(i,:));
        xlabel('Backward events');
        hold on
        subplot (1,4,3)
        plot(nb_tumblings(i),'s','color',color(i,:));
        xlabel('Tumbling events');
        hold on
        subplot (1,4,4)
        plot(nb_bypass(i),'s','color',color(i,:));
        xlabel('Bypassing events');
        hold on
    end

end


%% Save

if(params.savefig)
    prefix = 'LM';
    saveas(h1,[prefix '_Figure_1'], 'eps');
    saveas(h1,[prefix '_Figure_1'], 'jpg');
    saveas(h2,[prefix '_Figure_2'], 'eps');
    saveas(h3,[prefix '_Figure_3'], 'eps');
    saveas(h4,[prefix '_Figure_4'], 'eps');
    saveas(h5,[prefix '_Figure_5'], 'eps');
    saveas(h6,[prefix '_Figure_6'], 'eps');
end

%% Export parameters

if params.output
    if ~exist(params.output, 'file')
        fid = fopen(params.output,'w');
        fprintf(fid, '%% avg_nrNuclei totTime_min growth_speed totNb_forwards totAvgDur_forwards_min totNb_backwards ');
        fprintf(fid, 'totAvgDur_backwards_min mvt_ratio avgspeed_min totNb_tumblings totAvgDur_tumblings_min totNb_bypass\n');
    else
        fid = fopen(params.output,'a');
    end
    fprintf(fid, '%.2f %.2f %.4f %.3f %.3f %.3f ', avg_nrNuclei, totTime_min, growth_speed, totNb_forwards, totAvgDur_forwards_min, totNb_backwards);
    fprintf(fid, '%.3f %.3f %.3f %.3f %.3f %.3f\n', totAvgDur_backwards_min, mvt_ratio, avgspeed_min, totNb_tumblings, totAvgDur_tumblings_min, totNb_bypass);
    fclose(fid);
end

