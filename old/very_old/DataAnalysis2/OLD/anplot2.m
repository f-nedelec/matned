function anplot(param, data, linespec, which)

s1 = find( data(:,5) ~= 0 );
s2 = find( data(:,6) ~= 0 );

d1 = ( data(s1,2) - data(s1,5) ) ./ ( data(s1,5) );
d2 = ( data(s2,3) - data(s2,6) ) ./ ( data(s2,6) );

e1 = sum( d1 .* d1 ) ;
if ( size(s1,1) ~= 0 )   e1 = e1 / size(s1,1); end

e2 = sum( d2 .* d2 ) ;
if ( size(s2,1) ~= 0 )   e2 = e2 / size(s2,1); end

disp(['errors = ' num2str(e1) ',  ' num2str(e2)]);

s  = intersect(s1, s2);

d1 = ( data(s,2) - data(s,5) ) ./ ( data(s,5) );
d2 = ( data(s,3) - data(s,6) ) ./ ( data(s,6) );
plot( d1, d2, linespec, 'LineWidth', 2, 'MarkerSize',10 );

%hold on;
%mx=max( [ d1 d2]);

%plot([0 mx], [0 mx],'k');


return;