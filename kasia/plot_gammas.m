function plot_gammas(pth)
%
%
% Francois's function to plot the profile of simulated spindles
% Updated Nice, 18.09.2012; Vienna, 10.04.2014; Strasbourg 4.4.2015

if nargin < 1
    pth = '.';
end

%% only use data after 4000s

[ data, cnt ] = load_after(fullfile(pth, 'gammas.txt'), 4000);

if numel(data) == 0
    %error('Could not load data from <gammas.txt>');
    data = load([pth, '/gammas.txt']);
end


%figure('Name', pth);

plot_this(data, cnt);

title('Gamma Complexes');
xlabel('Position / um');
ylabel('Count per 2 um');

end

%%
function plot_this(f, cnt)

global max_pos;

col = 'gbbk';
bins = -max_pos:2:max_pos;
mids = ( bins(2:end)+bins(1:end-1) ) / 2;

for s = 0:3
    sel = logical( f(:,1) == s );
    cnts = histcounts( f(sel, 2), bins ) ./ cnt;
    plot(mids, cnts, col(s+1), 'LineWidth', 3);
    hold on;
end
xlim([-max_pos, max_pos]);

end
