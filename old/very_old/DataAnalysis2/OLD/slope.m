function [s, a] = slope( x, y, debugflag )

p = polyfit(x,y,1);
s = p(1);
a = p(0);


if (exist('debugflag','var') == 1)
   figure('Name','debug logslope');
   plot(x,y);
   hold on;
   plot(x, polyval(p,x) );
end