function [ im, nb ] = dataarray( param, data, verbose )

if ( exist('verbose','var') == 0 ) verbose = 0; end

if ( size(param,2) ~= 2 ) 
   disp('param has more than two values -> ignored last');
end

uniq1 = sort( unique( param(:,1) ) )';
uniq2 = sort( unique( param(:,2) ) )';

val='top->bottom: ';
for i=1:length(uniq1)
   val = [val sprintf('%7.3f  ', uniq1(i))];
end
if ( verbose ) disp(val); end

val ='left->right: ';
for i=1:length(uniq2)
   val = [val sprintf('%7.3f  ', uniq2(i))];
end
if ( verbose ) disp(val); end

im = zeros( length(uniq1) , length(uniq2) );
nb = zeros( length(uniq1) , length(uniq2) );

for u1=1:length(uniq1)
for u2=1:length(uniq2)
   
   match = ( param(:,1) == uniq1(u1) ) .* ( param(:,2) == uniq2(u2) );
   indx = find( match );
   if ( length( indx ) )
      im(u1,u2) = mean( data( indx ) );
      nb(u1,u2) = length(indx);
   end
   
end
end