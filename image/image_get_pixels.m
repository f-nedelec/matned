function pixels = image_get_pixels(im, indx)
% Extract the pixel from the structs returned by tiffread.m
%
% F. Nedelec, 25.04.2019

%% if index is not specified, return sum of all images

if isfield(im, 'data')
    if nargin < 2 || isempty(indx)
        pixels = im(1).data;
        for i = 2:length(im)
            pixels = pixels + im(i).data;
        end
    else
        pixels = im(indx).data;
    end
else
    pixels = im;
end

%% extract tiffread color image

if iscell(pixels)
    res = zeros([size(pixels{1}), 3]);
    try
        for c = 1:length(pixels)
            res(:,:,c) = pixels{c};
        end
    catch
        disp('get_pixel failed to assemble a RGB image');
    end
    pixels = res;
end

end