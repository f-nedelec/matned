function testmovietrack2

nbframes = 10;
sizef = [100, 100];
step = 20;


mask = maskgrad1( 10 );
high = max(max( mask .* maskcircle1( 21 ) ));
pic  = ( high - mask );
pic  = pic .* ( pic > 0 );
pics = ([1 1] + size(pic))/2;

noise = 0.2 * max(max( pic ));

c1 = cumsum( step*( rand(nbframes,2) - 1/2 ) ) ; 
c2 = cumsum( step*( rand(nbframes,2) - 1/2 ) ) ; 

for i=1:nbframes
   c1(i,:) = c1(i,:) + sizef/3;
   %c1(i,:) = max( [1 1], c1(i,:) );
   %c1(i,:) = min( sizef, c1(i,:) );
   
   c2(i,:) = c2(i,:) + sizef/3;
   %c2(i,:) = max( [1 1], c2(i,:) );
   %c2(i,:) = min( sizef, c2(i,:) );
end

[c1, c2] = reorderpoints( c1, c2 );

for i=1:nbframes
   
   im = noise * rand( sizef );
   im = rand * immax( im, pic, c1(i,:)-pics, 2 );
   im = rand * immax( im, pic, c2(i,:)-pics, 2 );
   mov( i ).data = im;
   
end


[ cmov, ct1, ct2] = movietrack1( mov );

show1(255*ones(sizef),'raw');
for indx=1:nbframes
   plot(c1(indx,2), c1(indx,1),'bo', 'LineWidth', 2);
   plot(c2(indx,2), c2(indx,1),'ro', 'LineWidth', 2);
   plot(ct1(indx,2), ct1(indx,1),'bx', 'MarkerSize',10);
   plot(ct2(indx,2), ct2(indx,1),'rx', 'MarkerSize',10);
end

error1 = max( dist( ct1, c1 ) );
error2 = max( dist( ct2, c1 ) );

min(error1, error2)

return;

showmovie( cmov );

function x = dist(c,d)
e = ( c - d ) .^2;
x = sqrt( sum( e, 2 ) );
return
