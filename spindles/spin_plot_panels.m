function spin_plot_panels(opt)

% plot the mass, on each image

if nargin < 1
    opt = [];
end


try
    data = load('results_mass.txt');
catch
    error('File "results_mass.txt" not found');
end



% Produce a figure for each time-point, using colored regions to
% highlight the dectected symmetry of the structure,

TUB = spin_load_images('tub', [], opt);

for n = 1:length(TUB)
    
    show_image(TUB(n));
    
    for p = 1:size(data,1)

        region = data(p, 1:5);
        mass = data(p, 7+n-1);

        plot_rect(region(2:5), mass, region(1));
        %image_drawrect(region(2:5), 'g:', num2str(region(1)));
    end
    
end

    function h = plot_rect(rec, value, id)
        lx = rec(1);
        ly = rec(2);
        ux = rec(3);
        uy = rec(4);
        h(1) = plot([ ly, ly, uy, uy, ly ], [ lx, ux, ux, lx, lx ], 'b:');
        h(2) = text(ly, lx, num2str(value), 'Color', [1 1 0], 'VerticalAlignment', 'Bottom');
        h(3) = text(ly, ux, num2str(id), 'Color', [0 0 1], 'VerticalAlignment', 'Bottom');
    end

end
