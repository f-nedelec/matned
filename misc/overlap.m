function [para, anti] = overlap(P, Q)
% 

pts = cat(1, P, Q);
kind = cat(1, zeros(length(P), 1), ones(length(Q), 1));


figure;
voronoi(pts(:,1), pts(:,2));
hold on;
plot(P(:,1), P(:,2), 'kx', 'MarkerSize',10);
plot(Q(:,1), Q(:,2), 'ko', 'MarkerSize',10);

TRI = delaunay(pts);
triplot(TRI, pts(:,1), pts(:,2), 'k:');

%[V,C] = voronoin(pts);

para = 0;
anti = 0;
for t = 1:size(TRI,1)
    anti = anti + check_pair(TRI(t,1), TRI(t,2), pts, kind);
    anti = anti + check_pair(TRI(t,2), TRI(t,3), pts, kind);
    anti = anti + check_pair(TRI(t,1), TRI(t,3), pts, kind);
    para = para + 3;
end

para = para / 2
anti = anti / 2

end


function res = check_pair(a, b, pts, kind)
    res = ( kind(a) ~= kind(b) );
    if ( 0 )
        plot([pts(a,1), pts(b,1)], [pts(a,2), pts(b,2)], 'r', 'LineWidth', 4);
    end
end