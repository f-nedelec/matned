function flat = filterflatten( im )

m = 2000;

imb = filtergaussian( imb, m );

imb = imb + (imb==0);

%the 'sharp' image:
ima = filtergaussian( im, 1.5);


%divide one by the other:

flat =  ima ./ imb ;

