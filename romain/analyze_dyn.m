function res = analyze_dyn(arg, varargin)
%
% call another program (eg. analyzeSIM) in multiple directories
% 
% Examples:
% analyze_dirs('run*');
% analyze_dirs('run0013');
%
% F.Nedelec 13.Nov.2014 updated 4.4.2014

if nargin < 1
    arg = 'run*';
end

paths = {};

if isdir(arg)
    paths{1} = fullfile(pwd, arg);
else
    p = fileparts(arg);
    d = dir(arg);
    for i = 1:size(d,1)
        paths{i} = fullfile(pwd, p, d(i).name);
    end
end

res = [];
hdir = pwd;

for i = 1:length(paths)
    
    dst = paths{i};
    
    if isdir(dst)
        cd(dst)
        fprintf(1, '================== Visiting %s\n', dst);
        dat = analyzeSIMdynein(varargin{:});
        res = cat(1, res, dat);
        cd(hdir);
    end    
end

save('res.mat', 'res');
figure;
plot(cat(1,res(:).numofdyn), cat(1,res(:).totNb_forwards), 'r.');
hold on;
plot(cat(1,res(:).numofdyn), cat(1,res(:).totNb_backward), 'b.');
ylim([0 0.5]);
savepng('dynein.png');

end

