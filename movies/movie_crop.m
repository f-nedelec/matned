function cmov = cropmovie(movie)

show_image( movie(1) );
poly = ROImousepoly;
[mask, roi] = maskpolygon( poly );
close;

for pic = 1:length( movie )
   
   cpic = mask .* image_crop( movie(pic).data, roi );
   
   cmov(pic).data = cpic;
   
end
    