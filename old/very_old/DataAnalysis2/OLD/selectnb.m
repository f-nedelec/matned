function [ps, ds, nb] = selectnb( param, data, indx, nbmust )

% [ps, ds] = selectnb( param, data, indx, nbmust )
%
% select data which have nbmust values in param(indx)

others = [1:indx-1 indx+1:size(param,2)];
tr     = param(:, others);
ps     = unique( tr, 'rows' );
nb     = zeros( size( param, 1 ), 1 );

for u=1:length(ps)
   
   equal = zeros( size(tr,1),1 );
   
   for i=1:size(tr, 1)
      equal(i) = all( tr(i,:) == ps(u, :) );
   end
   sel = find( equal );
   
   nb(sel) = length(sel) * ones( size( sel, 1 ), 1 );
   
end

g  = find( ismember(nb, nbmust) );
ds = data( g, : );
ps = param( g, : );
nb = nb( g );

return