function [d,xi,yi] = roipoly(x,y,a,xi,yi)
%ROIPOLY Define polygonal region of interest (ROI).
%	[BW,Xi,Yi] = ROIPOLY prompts you to define a region of interest 
%	using the mouse.  The region of interest is returned in the 
%	binary image BW which has the same size as the current image.  
%	BW contains 0's outside the region of interest and 1's inside.  
%	Also returned are the vertices of the polygon in Xi and Yi.
%	The ROI is defined by clicking in the image to define a closed
%	polygon. Press RETURN or right mouse button (shift-click on
%	the Macintosh) when done.
%
%	BW = ROIPOLY(A,Xi,Yi) returns a binary image BW that contains 1's 
%	inside the polygon defined by the vertices (x,y) and 0's
%	outside.
%
%	BW = ROIPOLY(X,Y,A,Xi,Yi) accounts for non-default axis limits.
%
%	Note:  the algorithm used by this function uses region
%	boundaries that lie entirely along edges between pixels.
%	For example, to select the rectangular region containing the
%	pixels A(a:b, c:d), set XI = [a-.5 a-.5 c+.5 c+.5] and
%	YI = [b-.5 d+.5 d+.5 b-.5] (assuming default axis limits).
%
%	See also ROICOLOR.

%	Reference:  For a discussion on region filling using boundaries
%	that lie entirely between pixels, see the technical report
%	"A Region Fill Algorithm Based on Crack Code Boundaries," by
%	Robert Grzeszczuk, Steven L. Eddins, and Thomas DeFanti,
%	Department of Electrical Engineering and Computer Science,
%	University of Illinois at Chicago, Chicago, Illinois 60680-4348,
%	UIC-EECS-92-6, 1992.

%	Clay M. Thompson 1-28-93
%	Revised Steven L. Eddins 14 March 1994
%	Copyright (c) 1993 by The MathWorks, Inc.
%	$Revision: 1.24 $  $Date: 1995/02/17 21:38:16 $


if nargin==0, % Get information from the current figure
  [x,y,a,hasimage] = getimage;
  if ~hasimage,
    error('The current figure must contain an image to use ROIPOLY.');
  end
  [xi,yi] = getline(gcf); % Get rect info from the user.

elseif nargin==1 | nargin==2 | nargin==4,
  error('Wrong number of input arguments.');

elseif nargin==3,
  yi = a(:); a = x; xi = y(:); 
  [num_rows,num_cols] = size(a);
  x = [1 num_cols]; y = [1 num_rows];
else
  xi = xi(:); yi = yi(:);
  x = [x(1) x(prod(size(x)))];
  y = [y(1) y(prod(size(y)))];
end

[num_rows, num_cols] = size(a);

if length(xi)~=length(yi)
  error('XI and YI must be the same length.'); 
end
if length(xi)==0
  error('XI and YI can''t be empty.'); 
end

% Make sure polygon is closed.
xi = [xi;xi(1)]; yi = [yi;yi(1)];
% Transform xi,yi into pixel coordinates.
roix = axes2pix(num_cols, x, xi);
roiy = axes2pix(num_rows, y, yi);

% Round input vertices to the nearest 0.5 and then add 0.5.
roix = floor(roix + 1);
roiy = floor(roiy + 1);

% Test for special case:  rectangular ROI.
%if (isrect(roix, roiy))
%  xmin = min(roix);
%  xmax = max(roix);
%  ymin = min(roiy);
%  ymax = max(roiy);
%  d = zeros(num_rows, num_cols);
%  d(ymin:(ymax-1), xmin:(xmax-1)) = ones(ymax-ymin, xmax-xmin);
%  return;
%end

% Initialize output matrix.  We need one extra row to begin with.
d = zeros(num_rows+1,num_cols);

num_segments = max(size(roix)) - 1;

% Process each segment.
for counter = 1:num_segments
  x1 = roix(counter);
  x2 = roix(counter+1);
  y1 = roiy(counter);
  y2 = roiy(counter+1);

  % We only have to do something with this segment if it is not vertical
  % or a single point.
  if (x1 ~= x2)

    % Compute an approximation to the segment drawn on an integer
    % grid.  Mark appropriate changes in the x direction in the
    % output image.
    [x,y] = intline(x1,x2,y1,y2);
    if ((x1 < 1) | (x1 > num_cols) | (x2 < 1) | (x2 > num_cols) | ...
	(y1 < 1) | (y1 > (num_rows+1)) | (y2 < 1) | (y2 > (num_rows+1)))
      xLowIdx = find(x < 1);
      if (length(xLowIdx))
	x(xLowIdx) = ones(size(xLowIdx));
      end
      xHighIdx = find(x > num_cols);
      if (length(xHighIdx))
	x(xHighIdx) = num_cols * ones(size(xHighIdx));
      end
      yLowIdx = find(y < 1);
      if (length(yLowIdx))
	y(yLowIdx) = ones(size(yLowIdx));
      end
      yHighIdx = find(y > (num_rows+1));
      if (length(yHighIdx))
	y(yHighIdx) = (num_rows+1) * ones(size(yHighIdx));
      end
    end
    diffx = diff(x);
    dx_indices = find(diffx);
    if (x2 > x1)
      mark_val = 1;
    else
      mark_val = -1;
      dx_indices = dx_indices + 1;
    end
    d_indices = [y(dx_indices) (x(dx_indices)-1)] * [1; (num_rows+1)];
    d(d_indices) = d(d_indices) + mark_val(ones(size(d_indices)),1);
  end
    
end

% Now a cumulative sum down the columns will fill the region with 
% either 1's or -1's.
d = abs(cumsum(d));

% Get rid of that extra row and we're done!
d = d(1:num_rows,:);
