function maiwenfun( filename, im )

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.*', 'select image file');
    cd(pathname);
end

if ( nargin <= 1 )
    if ischar( filename )
        im        = iplread( filename );
    else
        im        = filename;
        filename  = 'file not specified';
    end
end

if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

mask = maskipl( size(im,1), size(im,2) );
im = im .* mask;
fig = show( im );

%============= propose a possible center:

%center = findcenterwithcircles( im, [64, 128], 1);
%plot( center(2), center(1), 'y+');
figure(fig);

%============= user click on the center:

square_radius = 20;
center = dotclick;

%==================   compute the maximum radius allowed by ROI:

radius = floor( max( [ center, size(im) - center ] ) );
%disp( sprintf('radius = %i ', round(radius) ) );

%============= compute profile:
bin = 4;

[ raw_profile, var, area ] = distprofile( im, center, bin, radius, mask );

dist = bin *( [ 1:length( raw_profile ) ] - 0.5 );

fig1 = figure('Name', 'raw profile');
plot(dist, raw_profile, '.');
fit_coef = polyfit( dist, raw_profile, 2 );
fit = ( dist .^ 2 ) * fit_coef(1) + dist * fit_coef(2) + fit_coef(3);
hold on;
plot( dist, fit, 'k');
