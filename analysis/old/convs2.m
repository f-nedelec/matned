
function hc = convs2 ( h, c, mode )

% hc = conv2( h, c, mode ) 
%
% compute the convolution of h by c
% defined by f = sum( c(i) * h(x+dx) ) / sum( c );
% mode defines the clipping:
%     0: keep only the 'inside' pixels
%     1: keep the size of h ( default )


if ( nargin < 3 ) mode = 1; end

if ( rem( size(c,1) + 1, 2 ) ) | ( rem( size(c,2) + 1, 2 ) ) 
   disp( 'error in conv1: function c must have an odd size' );
   return;
end

dx = [1:size(c,1)] - ( size(c,1) + 1 )/2;
dy = [1:size(c,2)] - ( size(c,2) + 1 )/2;

h2 = image_crop( h, [ 1+min(dx), 1+min(dy), size(h,1)+max(dx), size(h,2)+max(dy) ], 0 );
%where h lies in h2:
lx = 1 - min(dx);
ly = 1 - min(dy);
ux = size(h,1) - min(dx);
uy = size(h,2) - min(dy);

%[lx ly ux uy]

hc = zeros( size(h) );

for i = 1:size(c,1);
   for j = 1:size(c,2);
      if ( c(i,j) )      
         hc = hc + c(i,j) * h2( lx+dx(i):ux+dx(i), ly+dy(j):uy+dy(j) );
      end
   end
end

%crop the picture according to specified mode:
if ( mode == 0)
   hc = image_crop( hc, [ lx, ly, size(h,1) - max(dx), size(h,2)-max(dy) ] );
end