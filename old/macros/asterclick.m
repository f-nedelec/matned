function [ points, isrect ] = asterclick( im )

% click first on the center, then on the microtubule's ends
% the macro displays the lengths.

if ( nargin > 0 )
    showim( log( double( im.data )));
end

savedpointer = get(gcf, 'pointer');
set(gcf, 'pointer', 'fullcrosshair');
set(gcf, 'units', 'pixels')

nb = 0;
while ( 1 )
    
    drawnow;
    k = waitforbuttonpress;
   
    %stop if key pressed or right mouse button:
    if k | strcmp( get( gcf, 'SelectionType' ) ,'alt' )
        break;
    end 
    
    p = get(gca,'CurrentPoint');        % button down detected
    p = p(1,2:-1:1);                    % extract x and y
    
    if ( nb == 0 )
        center = p;
        plot( center(2), center(1), 'r+', 'linewidth', 4);
    else
        plot( [center(2), p(2)], [center(1), p(1)], ':' );
        text( p(2), p(1), num2str( nb ), 'Color', 'blue' );
        dist(nb) = sqrt( sum( ( p - center ) .^ 2 ) );
        disp(sprintf('dist(%i) = %.1f pixels', nb, dist(nb) ));
   end
   nb = nb + 1;
   
end

disp(sprintf(' average is %.1f pixels', mean(dist) ));

set( gcf, 'pointer', savedpointer );
