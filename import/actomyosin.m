%% =========================================================================
%
% MICROSCOPIC MODEL OF A ONE-DIMENSIONAL ACTO-MYOSIN BUNDLE
%
% 
% Model Ingredients: 
% - deterministic treadmilling of actin filaments
% - plus-end-tracking crosslinking of actin
% - bipolar myosin filaments
% - motion determined by force balance
%
% Should run in recent Matlab suites.
%
% --- Disclaimer and Copyright statement -----------------------------------
%
% (c) B.M. Friedrich, 11/2010 - 10/2011
% email: ben@pks.mpg.de
%
% This program is provided as is and comes without any guarantee at all.
% You are free to use, modify and redistribute this file as long as this copyright statement is retained as whole and unchanged.
% If you use this code, consider citing our corresponding paper
%
% Title: Sarcomeric Pattern Formation by Actin Cluster Coalescence
% Authors: Benjamin M. Friedrich, Elisabeth Fischer-Friedrich, Nir S. Gov, Samuel A. Safran
% Journal: PLoS Computational Biology 8(6):2012, e1002544
%
%
% --- Features --------------------------------------------------------------
%
% - probabilistic rigid crosslinking of filaments with aligned plus ends
% - fixed random filament length drawn from user-specified distribution
% - myosin as crosslinkers and force generators
% - inter-filament friction: hydrodynamic friction, dynamic crosslinking, pushing forces -> increases run time by 50%
% - periodic boundary conditions
%
%
% --- Data structures (pre-allocation is key for performance):----------------
% ... for clusters
% - x, npos, nneg ... large lists for filament cluster information
% - is_clus ... boolean list indicating which entries in use
% ... for individual filaments
% - Lfil ... length of individual filaments
% - is_fil ... boolean list indicating which entries in use
% - clusfil ... cluster of individual filaments
% - posfil ... orientation of individual filaments (positive=treadmilling to the right)
% ... for myosin filaments
% - xmyo ... position data myosin filaments
% - mpos, mneg ... binding sites of myosin filaments for plus and minus filaments : filament index
% speed bottle necks
% - myosin attachment, now fast version is used for case of uniform filament length
% - inter-filament friction
%
% =========================================================================


%% paramaters =============================================================
% system size --------------------------------------------
Lsys=40; % system size (periodic b.c.)
% filament numbers ---------------------------------------
nfil0=50*Lsys; % number of filamants at t=0
nmyo =nfil0/2; % number of myosin filaments
nomyo=(nmyo==0);
% filament lengths ---------------------------------------
Lfil0=1; % mean filament length
Lmyo=0.5; % myosin filament length
% actin dynamics -----------------------------------------
kturnover=0; % turnover rate of single actin filament
v0=1; % treadmilling speed of actin filaments
% actin filament plus-end crosslinking -------------------
delta=0.05*Lfil0; % range of rigid plus end crosslinking
rate0=1; % base rate for cluster crosslinking
% actin-myosin interaction -------------------------------
kon=1; % rate constants for myosin - actin filament binding
koff=1;
% friction coefficients ----------------------------------
gamma_ma=10; % myosin-actin filament friction coefficient; equivalent to linear force-velocity relation
gamma_m=0.5; % myosin-cytosol friction coefficient 
fmyo=0; % myosin stall force
gamma=1; % filament-cytosol friction coefficient
zeta0=0; % inter-filament friction coefficient
sigma=0; % filament length variability
Lfil_func=@(n) ones(n,1); % uniform filament length
% Lfil_func=@(n) Lfil0*lognrnd(0,sigma,n,1); % random filament lengths for n filaments
% Lfil_func=@(n) exprnd(Lfil0,n,1); % random filament lengths for n filaments

save_results=false; 
        
nsim=1; % number of simulations

tmax=10; % simulation time

treport=.10; % time interval at which simulation results are saved
% tplot=2*tmax; % never
tplot=1; % time interval at which simulation results are plotted
% tplot=tmax;
handle_plot=figure(1);
% time interval at which data structure is cleaned up
if kturnover>0    
    tcleanup=.2/kturnover; 
else
    tcleanup=1;
end;
dt=0.01; % time step
tlist=dt:dt:tmax;

% initialize random number generator
RandStream.setGlobalStream(RandStream('mt19937ar','seed',1)); % for debugging only
% RandStream.setDefaultStream(RandStream('mt19937ar','seed',sum(100*clock)));

mod=@(x,y) x-y.*floor(x./y); % inline mod for speed

tstart=tic;

maxdata=2*nfil0; % size of data structure to hold filaments and clusters            
        
% output file
fname=sprintf('./sim_Lsys_%i_nfil0_%i_nmyo_%i_zeta0_%.3f_kturnover_%.3f_fmyo_%.2f_sigma_%.2f.dat',Lsys,nfil0,nmyo,zeta0,kturnover,fmyo,sigma);
if exist(fname,'file') && save_results
    delete(fname);    
end;

% loop over simulations
for isim=1:nsim
    
    % initialize global vars =======================================
    % allocate mem -------------------------------------------------
    Lfil=nan(maxdata,1);    % length of individual filaments    
    clusfil=nan(maxdata,1); % cluster index for individual filaments
    posfil=nan(maxdata,1);  % plus filament ?    
    x   =nan(maxdata,1);    % cluster position
    yfil=nan(maxdata,1);    % only for visualization
    npos=nan(maxdata,1);    % number of plus filaments in cluster - redundancy npos(idata)=sum(posfil(clusfil==idata))
    nneg=nan(maxdata,1);
    is_fil=false(maxdata,1); % which entries in use ?
    is_clus=false(maxdata,1);        
    % assign random initial values ---------------------------------
    nfil=nfil0; % number of filaments
    Lfil(1:nfil)=Lfil_func(nfil); % user-specified length distribution
    % force filament lengths within admissable range [2*delta,Lsys/2]
    while (min(Lfil(1:nfil))<2*delta) || (max(Lfil(1:nfil))>Lsys/2)
        ind=(Lfil(1:nfil)<2*delta) | (Lfil(1:nfil)>Lsys/2);
        Lfil(ind)=Lfil_func(sum(ind));
    end;    
    yfil(1:nfil)=rand(nfil,1);    
    clusfil(1:nfil)=1:nfil; % at the beginning, each single filament counts as one cluster
    posfil(1:nfil)=randi(2,nfil,1)-1; % plus filament, i.e. treadmilling to the left ?        
    x(1:nfil)=sort( Lsys*rand(nfil,1) ); % positions of filaments/clusters (plus ends)    
    npos=posfil;
    nneg=1-posfil;    
    is_fil(1:nfil)=true(nfil,1);
    is_clus(1:nfil)=true(nfil,1);
    last_fil=nfil; % index points to last valid filament in Lfil
    last_clus=nfil; % index points to last valid cluster in x
    % myosin filaments ------------------------------------------
    xmyo=Lsys*rand(nmyo,1); % center position of myosin filaments
    ymyo=rand(nmyo,1);
    mpos=nan(nmyo,1); % binding site for plus filaments
    mneg=nan(nmyo,1); % binding site for minus filaments
    
    % simple Euler scheme =========================================    
    for t=tlist                             
                
        % cluster formation ---------------------------------------                
        ind_clus=find(is_clus);                
        ind_clus_inv=cumsum(is_clus);
        nclus=length(ind_clus);        
        dist=repmat(x(is_clus),1,nclus)-repmat(x(is_clus)',nclus,1); % pairwise distances
        dist=mod(dist+Lsys/2,Lsys)-Lsys/2; % periodic b.c.
        % upper triangular matrix of crosslinking rates
        rate=rate0*exp(-abs(dist)/delta)/delta.*(...
            npos(is_clus)*npos(is_clus)'+nneg(is_clus)*nneg(is_clus)'+...
            npos(is_clus)*nneg(is_clus)'.*reshape(dist>0,nclus,nclus)+...
            nneg(is_clus)*npos(is_clus)'.*reshape(dist<0,nclus,nclus) );        
        rate=rate.*triu(ones(nclus),1);
        % ignore crosslinking rates that are too low
        ind=rate>.1;
        % matrix of random numbers        
        randmat=zeros(nclus); randmat(ind)=rand(sum(sum(ind)),1);
        ind_coal=find(randmat>0 & randmat<rate*dt)';
        ind_fused=1:nclus; % modified entry=i1 at position i2 indicates that cluster i2 fused with cluter i1
        for icoal=ind_coal
           [i1,i2]=ind2sub([nclus nclus],icoal);
           i1=ind_fused(i1);
           i2=ind_fused(i2);           
           % special case: both clusters of icoal coalesced with third cluster earlier
           if i1==i2
               continue
           end;
           j1=ind_clus(i1);
           j2=ind_clus(i2);
           % combine both filaments/clusters
           n1=npos(j1)+nneg(j1); n2=npos(j2)+nneg(j2); % respective filament numbers
           % match positions
           x(j2)=x(j1)+mod(x(j2)-x(j1)+Lsys/2,Lsys)-Lsys/2; % take care of periodic b.c.
           x(j1)=( x(j1)*n1+x(j2)*n2 ) / (n1+n2); % weighted average
           x(j1)=mod(x(j1),Lsys);
           npos(j1)=npos(j1)+npos(j2);
           nneg(j1)=nneg(j1)+nneg(j2);                                            
           % delete entry for second filament/cluster
           x(j2)=nan; npos(j2)=nan; nneg(j2)=nan;
           is_clus(j2)=false;              
           % any subsequent cluster fusion involving i2 should target i1 instead                      
           ind_fused(ind_fused==i2)=i1;           
           % myosin binding sites point to single filaments, not clusters: no need to update them
        end; % icoal            
        % update filament cluster membership       
        clusfil(is_fil)=ind_clus(ind_fused(ind_clus_inv(clusfil(is_fil))));                      
        
        % filament turnover ===================================================
        % remove filaments ----------------------------------------------------
        ndecay=poissrnd(kturnover*nfil*dt);                  
        ind_fil=find(is_fil);            
        while ndecay>0
            ifil=ind_fil(randi(length(ind_fil))); % select random filament
            if is_fil(ifil) % take care that no filament gets deleted twice
                is_fil(ifil)=false;                
                iclus=clusfil(ifil); % cluster corresponding to this filament                
                % detach any myosin filaments attached to this plus/minus filament & update cluster
                if posfil(ifil)                        
                    mpos(mpos==ifil)=nan;
                    npos(iclus)=npos(iclus)-1;
                else
                    mneg(mneg==ifil)=nan;
                    nneg(iclus)=nneg(iclus)-1;
                end;
                % cluster now empty?                
                if npos(iclus)+nneg(iclus)==0
                    is_clus(iclus)=false;
                    x(iclus)=nan; npos(iclus)=nan; nneg(iclus)=nan;                                            
                end;
                Lfil(ifil)=nan; yfil(ifil)=nan; clusfil(ifil)=nan; posfil(ifil)=nan; % delete all information associated with this filament
                nfil=nfil-1; % update number of filaments            
                ndecay=ndecay-1;
            end;
        end; % while ndecay>0         
        % create single filaments ---------------------------------------------
        nbirth=poissrnd(kturnover*nfil0*dt); 
        if nbirth>0            
            ind_fil=last_fil+(1:nbirth); % indices of new filaments
            ind_clus=last_clus+(1:nbirth); % indices of new single-filament cluster
            % random filaments length and orientation            
            Lfil(ind_fil)=Lfil_func(nbirth); % user-specified length distribution
            % force filament lengths within admissable range [2*delta,Lsys]
            while (min(Lfil(ind_fil))<2*delta) || (max(Lfil(ind_fil))>Lsys/2)
                ind=(Lfil(ind_fil)<2*delta) | (Lfil(ind_fil)>Lsys/2);
                Lfil(ind_fil(ind))=Lfil_func(sum(ind));
            end;                
            clusfil(ind_fil)=ind_clus; % map new filaments to new clusters
            yfil(ind_fil)=rand(nbirth,1); % for visualisation only
            is_fil(ind_fil)=true;
            posfil(ind_fil)=randi(2,nbirth,1)-1;
            % random position
            x(ind_clus)=Lsys*rand(nbirth,1);
            npos(ind_clus)=posfil(ind_fil);
            nneg(ind_clus)=1-npos(ind_clus);
            is_clus(ind_clus)=true;
            % update data pointers
            last_fil=last_fil+nbirth; 
            last_clus=last_clus+nbirth;
            nfil=nfil+nbirth; % update number of filaments
        end; % nbirth>0 ?                
        
        % myosin attachment and detachment ================================                
        if ~nomyo        
        % detachment of myosin filament -----------------------------------
        % ... plus filaments
        ibound=find(isfinite(mpos)); % select myosin filaments with occupied plus binding site        
        % spontaneous detachment
        ind=rand(length(ibound),1)<koff*dt; 
        % forced detachment once actin filament end is reached
        dist=mod(xmyo(ibound)+Lmyo/2-x(clusfil(mpos(ibound)))+Lsys/2,Lsys)-Lsys/2;
        ind=ind | (dist>0) | (dist<- Lfil(mpos(ibound)) );            
        mpos(ibound(ind))=nan; % effect detachment
        % ... negative filaments    
        ibound=find(isfinite(mneg));
        ind=rand(length(ibound),1)<koff*dt;
        dist=mod(xmyo(ibound)-Lmyo/2-x(clusfil(mneg(ibound)))+Lsys/2,Lsys)-Lsys/2;
        ind=ind | (dist<0) | (dist> Lfil(mneg(ibound)) );            
        mneg(ibound(ind))=nan; % effect detachment                        
                
        % attachment of myosin filament -----------------------------------------        
        ind_clus_inv=cumsum(is_clus);
        % ... plus binding site        
        ind_free=find(isnan(mpos)); % myosin with free plus binding site
        nfree=length(ind_free);        
        if sigma>0 % slow
            ind_pos=find(posfil==1); % indices of plus filaments
            nposfil=length(ind_pos);
            distplus =repmat(xmyo(ind_free),1,nposfil)+Lmyo/2-repmat(x(clusfil(ind_pos))',nfree,1); % distance binding site - filament plus end        
            distminus=distplus+repmat(Lfil(ind_pos)',nfree,1); % distance binding site - filament minus end        
            % incidence matrix of binding possibilities (periodic b.c.)    
            bindmat=( (distplus<0) & (distminus>0) ) | ( (distplus-Lsys<0) & (distminus-Lsys>0) ); % corrected 02.06.2011        
            ind_myo=find( rand(nfree,1) < kon*dt*sum(bindmat,2) ); % indices of myosins that are going to bind to a plus filament
            for imyo = ind_myo'
                ind_bind=find(bindmat(imyo,:)); % indices of plus filaments in binding range of myosin 'imyo'
                mpos(ind_free(imyo))=ind_pos(ind_bind(randi(length(ind_bind)))); % select ONE random filament for binding
            end;
        else % fast code for case of uniform filament length
            % incidence matrix sorting plus filaments to their clusters
            ind_posfil=find(posfil==1); nposfil=length(ind_posfil);
            filclus=sparse(1:nposfil,ind_clus_inv(clusfil(ind_posfil)),1,nposfil,nclus);             
            for iclus=find(npos>0)' % loop over clusters with plus filaments
                dist=xmyo(ind_free)+Lmyo/2-x(iclus); % distances of myosin plus binding site to cluster position
                % incidence vector of binding possibilities (periodic b.c.)    
                ind_bind=( (dist<0) & (dist+Lfil0>0) ) | ( (dist-Lsys<0) & (dist+Lfil0-Lsys>0) );            
                ind_bind(ind_bind)=rand(sum(ind_bind),1)<kon*dt*npos(iclus); % select myosins that bind to this cluster
                % for each myosin that binds to this cluster: select ONE plus filament from this cluster
                if sum(ind_bind)>0                 
                    ind_fil=ind_posfil( logical(filclus(:,ind_clus_inv(iclus))) );
                    mpos(ind_free(ind_bind))=ind_fil(randi(npos(iclus),sum(ind_bind),1));
                end;
            end; % iclus     
        end; % sigma>0?
        % ... minus binding site        
        ind_free=find(isnan(mneg)); % myosin with free minus binding site
        nfree=length(ind_free);
        if sigma>0 % slow
            ind_neg=find(posfil==0); % indices of plus filaments
            nnegfil=length(ind_neg);        
            distplus =repmat(xmyo(ind_free),1,nnegfil)-Lmyo/2-repmat(x(clusfil(ind_neg))',nfree,1); % distance binding site - filament plus end
            distminus=distplus-repmat(Lfil(ind_neg)',nfree,1); % distance binding site - filament minus end        
            % incidence matrix of binding possibilities (periodic b.c.)        
            bindmat=( (distplus>0) & (distminus<0) ) | ( (distplus+Lsys<0) & (distminus+Lsys>0) ); % corrected 02.06.2011        
            ind_myo=find( rand(nfree,1) < kon*dt*sum(bindmat,2) ); % indices of myosins that are going to bind to a minus filament
            for imyo = ind_myo'
                ind_bind=find(bindmat(imyo,:)); % indices of plus filaments in binding range of myosin 'imyo'
                mneg(ind_free(imyo))=ind_neg(ind_bind(randi(length(ind_bind)))); % select ONE random filament for binding
            end;        
        else % fast code for case of uniform filament length
            % incidence matrix sorting minus filaments to their clusters
            ind_negfil=find(posfil==0); nnegfil=length(ind_negfil);
            filclus=sparse(1:nnegfil,ind_clus_inv(clusfil(ind_negfil)),1,nnegfil,nclus); 
            for iclus=find(nneg>0)' % loop over clusters with minus filaments
                dist=xmyo(ind_free)-Lmyo/2-x(iclus); % distances of myosin minus binding site to cluster position
                % incidence vector of binding possibilities (periodic b.c.)    
                ind_bind=( (dist>0) & (dist-Lfil0<0) ) | ( (dist+Lsys>0) & (dist-Lfil0+Lsys<0) );            
                ind_bind(ind_bind)=rand(sum(ind_bind),1)<kon*dt*nneg(iclus); % select myosins that bind to this cluster
                % for each myosin that binds to this cluster: select ONE minus filament from this cluster                
                if sum(ind_bind)>0                
                    ind_fil=ind_negfil( logical(filclus(:,ind_clus_inv(iclus))) );
                    mneg(ind_free(ind_bind))=ind_fil(randi(nneg(iclus),sum(ind_bind),1));
                end;
            end; % iclus
        end; % sigma>0 ?
        end;
        
        % update positions =======================================  
        % force-balance yields linear equation system for cluster velocities: A.v+b=0
        % slaved equations for coupling cluster velocities -> myosin velocities: vm=C.v+d
        nclus=sum(is_clus);        
        C=zeros(nmyo,nclus); d=zeros(nmyo,1); % mem allocation        
        % intended use for ind_v : x(is_clus)(ind_v(k))=x(clusfil(k))
        dummy=cumsum(is_clus); 
        ind_v=nan(maxdata,1);
        ind_v(is_fil)=dummy(clusfil(is_fil));
        % filament-cytosol friction ------------------------------
        ind_posfil=find(posfil==1); nposfil=length(ind_posfil); % select plus filaments 
        ind_negfil=find(posfil==0); nnegfil=length(ind_negfil); % select minus filaments 
        Lpos=sparse(clusfil(ind_posfil),1:nposfil,ones(nposfil,1),maxdata,nposfil)*Lfil(ind_posfil);
        Lneg=sparse(clusfil(ind_negfil),1:nnegfil,ones(nnegfil,1),maxdata,nnegfil)*Lfil(ind_negfil);
        b=gamma*v0*(-Lpos(is_clus)+Lneg(is_clus)); % left-hand-side (times -1)        
        A=gamma*diag(Lpos(is_clus)+Lneg(is_clus)); % friction matrix        
        % inter-filament friction --------------------------------        
        if zeta0>0 % inter-filament friction ?
            % filament plus and minus end positions                        
            % ... plus filaments
            ind_posfil=find(posfil==1); nposfil=length(ind_posfil); % select plus filaments 
            posxplus=x(clusfil(ind_posfil)); % plus end positions
            posxminus=posxplus-Lfil(ind_posfil); % minus end positions; do NOT adjust for periodic b.c. here
            is_crossing=posxminus<0; % filaments extending across the branch-cut need special treatment
            posxplus =[posxplus;posxplus(is_crossing)+Lsys]; % add these special filaments a second time
            posxminus=[posxminus;posxminus(is_crossing)+Lsys]; 
            nposfil2=length(posxplus); % accounts for double counting of special filaments; nposfil2=nposfil+sum(is_crossing)
            % (sparse) incidence matrix sorting filaments to their respective cluster velocity
            ivposfilmat=sparse(1:nposfil2,...
                [ind_v(ind_posfil);ind_v(ind_posfil(is_crossing))],ones(nposfil2,1),nposfil2,nclus);
            % ... minus filaments
            ind_negfil=find(posfil==0); nnegfil=length(ind_negfil); % select minus filaments 
            negxplus=x(clusfil(ind_negfil)); % plus end positions
            negxminus=negxplus+Lfil(ind_negfil); % minus end positions; do NOT adjust for periodic b.c. here
            is_crossing=negxminus>Lsys; % filaments extending across the branch-cut need special treatment
            negxplus =[negxplus;negxplus(is_crossing)-Lsys]; % add these special filaments a second time
            negxminus=[negxminus;negxminus(is_crossing)-Lsys]; 
            nnegfil2=length(negxplus); % accounts for double counting of special filaments
            % (sparse) incidence matrix sorting filaments to their respective cluster velocity
            ivnegfilmat=sparse(1:nnegfil2,...
                [ind_v(ind_negfil);ind_v(ind_negfil(is_crossing))],ones(nnegfil2,1),nnegfil2,nclus);            
            % compute filament overlap
            % ... of plus and plus filaments
            olap=min(repmat(posxplus ,1,nposfil2),repmat(posxplus' ,nposfil2,1))-...
                 max(repmat(posxminus,1,nposfil2),repmat(posxminus',nposfil2,1));            
            olap(nposfil+1:end,nposfil+1:end)=0; % ignore overlap of special filaments with themselves
            App=zeta0*ivposfilmat'*max(0,olap)*ivposfilmat;            
            % ... of minus and minus filaments
            olap=min(repmat(negxminus,1,nnegfil2),repmat(negxminus',nnegfil2,1))-...
                 max(repmat(negxplus, 1,nnegfil2),repmat(negxplus' ,nnegfil2,1));            
            olap(nnegfil+1:end,nnegfil+1:end)=0;
            Amm=zeta0*ivnegfilmat'*max(0,olap)*ivnegfilmat;            
            % ... of plus and minus filaments
            olap=min(repmat(posxplus ,1,nnegfil2),repmat(negxminus',nposfil2,1))-...
                 max(repmat(posxminus,1,nnegfil2),repmat(negxplus' ,nposfil2,1));                   
            olap(nposfil+1:end,nnegfil+1:end)=0; % there should be no overlap between these special filaments anyway
            Apm=zeta0*ivposfilmat'*max(0,olap)*ivnegfilmat;            
            % update friction matrix
            A=A+diag(sum(App,2))-App+diag(sum(Amm,2))-Amm; % no contribution to b, no need to worry about diagonal entries of App            
            Amp=Apm'; % use symmetry
            A=A+diag(sum(Apm,2))-Apm+diag(sum(Amp,2))-Amp;
            b=b-2*v0*sum(Apm-Amp,2);
        end;        
        % myosin as active crosslinker ------------------------------------
        % ... doubly bound myosin
        indmyo=find( isfinite(mpos) & isfinite(mneg) );      
        C( [ sub2ind([nmyo nclus],indmyo,ind_v(mpos(indmyo))) ...
             sub2ind([nmyo nclus],indmyo,ind_v(mneg(indmyo))) ] )=gamma_ma/(2*gamma_ma+gamma_m); % vm~v1+v2; no contribution to d                         
        for imyo=indmyo' % loop over myosin filaments
            ipos=ind_v(mpos(imyo));
            ineg=ind_v(mneg(imyo));
            A(ipos,ipos)=A(ipos,ipos)+gamma_ma*(1-C(imyo,ipos));            
            A(ineg,ineg)=A(ineg,ineg)+gamma_ma*(1-C(imyo,ineg));            
            A(ineg,ipos)=A(ineg,ipos)+gamma_ma*( -C(imyo,ipos));            
            A(ipos,ineg)=A(ipos,ineg)+gamma_ma*( -C(imyo,ineg));            
            b(ipos)=b(ipos)-gamma_ma*v0+fmyo;
            b(ineg)=b(ineg)+gamma_ma*v0-fmyo;
        end; % imyo        
        % ... myosin bound to plus filament, but not to minus filament
        indmyo=find( isfinite(mpos) & isnan(mneg) );       
        C( sub2ind([nmyo nclus],indmyo,ind_v(mpos(indmyo))) )=gamma_ma/(gamma_ma+gamma_m);
        d(indmyo)=(-gamma_ma*v0+fmyo)/(gamma_ma+gamma_m);
        for imyo=indmyo' % loop over myosin filaments
            ipos=ind_v(mpos(imyo));
            A(ipos,ipos)=A(ipos,ipos)+gamma_ma*(1-C(imyo,ipos));            
            b(ipos)=b(ipos)+gamma_ma*(-v0-d(imyo))+fmyo;
        end; % imyo        
        % ... myosin bound to minus filament, but not to plus filament
        indmyo=find( isnan(mpos) & isfinite(mneg) );       
        C( sub2ind([nmyo nclus],indmyo,ind_v(mneg(indmyo))) )=gamma_ma/(gamma_ma+gamma_m);
        d(indmyo)=(gamma_ma*v0-fmyo)/(gamma_ma+gamma_m);
        for imyo=indmyo' % loop over myosin filaments
            ineg=ind_v(mneg(imyo));
            A(ineg,ineg)=A(ineg,ineg)+gamma_ma*(1-C(imyo,ineg));            
            b(ineg)=b(ineg)+gamma_ma*(+v0-d(imyo))-fmyo;
        end; % imyo                   
        % solve for velocities --------------------------------------------        
        v=-A\b; % solve for velocity vector           
        vm=C*v+d; % velocities of myosin filaments
        % update positions ------------------------------------------------        
        x(is_clus)=x(is_clus)+v*dt;
        xmyo=xmyo+vm*dt;
        % periodic b.c.
        x(is_clus)=mod(x(is_clus),Lsys);                    
        xmyo=mod(xmyo,Lsys);   
                
        % clean-up data-structure ===================================        
        if abs(mod(t,tcleanup))<dt/2            
            % indices to sort and compactify cluster list -----------
            ind_clus=find(is_clus);            
            [xsorted,ind_sort]=sort( x(ind_clus) );
            ind=ind_clus(ind_sort);
            % and now the inverted indices --------------------------                        
            ind_sort_inv=zeros(nclus,1);
            ind_sort_inv(ind_sort)=1:nclus; % dirty trick to invert permutation vector
            ind_inv=nan(maxdata,1); % corrected 25.04.2011
            ind_inv(is_clus)=ind_sort_inv; % intended use: xold(k)=xnew(ind_inv(k))
            % write used part of data list --------------------------
            last_clus=sum(is_clus);
            is_clus(1:last_clus)=true;
            x(1:last_clus)=x(ind); 
            npos(1:last_clus)=npos(ind);
            nneg(1:last_clus)=nneg(ind);
            % erase unused part of data list ------------------------
            is_clus(last_clus+1:end)=false;
            x(last_clus+1:end)=nan; npos(last_clus+1:end)=nan; nneg(last_clus+1:end)=nan;                        
            % update cluster membership of filaments ----------------
            clusfil(isfinite(clusfil))=ind_inv(clusfil(isfinite(clusfil)));            
            % sort and compactify filament list ---------------------
            ind_fil=find(is_fil);            
            [clusfil_sorted,ind_sort]=sort( clusfil(ind_fil) );
            ind=ind_fil(ind_sort);
            % and now the inverted indices --------------------------                        
            ind_sort_inv=zeros(nfil,1);
            ind_sort_inv(ind_sort)=1:nfil; % dirty trick to invert permutation vector
            ind_inv=nan(maxdata,1); % corrected 25.04.2011 
            ind_inv(is_fil)=ind_sort_inv; % intended use: Lfilold(k)=Lfilnew(ind_inv(k))
            % write used part of data list --------------------------
            last_fil=sum(is_fil); % = nfil
            is_fil(1:last_fil)=true;
            clusfil(1:last_fil)=clusfil(ind); % =clusfil_sorted
            Lfil(1:last_fil)=Lfil(ind);
            posfil(1:last_fil)=posfil(ind);            
            yfil(1:last_fil)=yfil(ind);
            % erase unused part of data list ------------------------
            is_fil(last_fil+1:end)=false;
            clusfil(last_fil+1:end)=nan; Lfil(last_fil+1:end)=nan; posfil(last_fil+1:end)=nan; yfil(last_fil+1:end)=nan;                        
            % update myosin binding sites ---------------------------            
            mpos(isfinite(mpos))=ind_inv(mpos(isfinite(mpos)));
            mneg(isfinite(mneg))=ind_inv(mneg(isfinite(mneg)));
        end;

        % make report ===============================================
        if abs(mod(t+dt/2,treport)-dt/2)<dt/2
            % compute structure factor
            q0=2*pi/Lsys; % fundamental wave vector
            nmax=2*Lsys/Lfil0; % maximal mode number       
            q=(0:.1:nmax)'*q0;
            s=( npos(is_clus)+nneg(is_clus) )/nfil; % normalized cluster sizes
            I0=sum(s.^2); % base level
            U=exp(1i*q*x(is_clus)')*s; % 1i=imaginary unit
            I=abs(U).^2; % structure factor
            results=[isim,t,I0,I'];
            if save_results
                save(fname,'results','-ASCII','-APPEND');                
            end;                    
        end; % make report ?

        % visualize actin bundle ====================================
        if abs(mod(t+dt/2,tplot)-dt/2)<dt/2            
            set(0,'CurrentFigure',handle_plot); % flag figure for future drawing WITHOUT raising the window
            clf
            subplot(2,1,1)
            hold on
            xlim([0 Lsys])
            ylim([0 1])
            axis off
            text(37,1.15,sprintf('t=%.1f',t),'FontSize',12)
            % plot filaments
            linewidth=2;
            for ifil=find(is_fil)'
                iclus=clusfil(ifil);
                if posfil(ifil)                                    
                    plot(x(iclus)+[-Lfil(ifil) 0]     ,yfil(ifil)+[0 0],'LineWidth',linewidth)
                    plot(x(iclus)+[-Lfil(ifil) 0]+Lsys,yfil(ifil)+[0 0],'LineWidth',linewidth) % periodic b.c.
                else
                    plot(x(iclus)+[ Lfil(ifil) 0]     ,yfil(ifil)+[0 0],'r','LineWidth',linewidth)
                    plot(x(iclus)+[ Lfil(ifil) 0]-Lsys,yfil(ifil)+[0 0],'r','LineWidth',linewidth) % periodic b.c.
                end;
            end;
            % plot plus end crosslinkers
            for iclus=find(is_clus)'
                y=yfil(clusfil==iclus);
                % linewidth=max(1,round( 5*(npos(iclus)+nneg(iclus))*Lsys/(2*Lfil0+Lmyo)/nfil0 ));                
                plot(x(iclus)+[0 0],[min(y) max(y)],'color',[0 0.75 0],'LineWidth',linewidth)
            end;            
            subplot(2,1,2)
            hold on
            plot(q,I)            
            offset=200;
            [maxI,maxq]=max(I(offset+2:offset+100));
            plot((maxq+offset)*0.1*q0,maxI,'r.','MarkerSize',20)
            set(gca,'FontName','Symbol')
            set(gca,'XTick',pi*(1:4))
            text(0,-0.1,'0'); set(gca,'XTickLabel',{'p','2p','3p','4p'})
            set(gca,'YTick',[])
            text(-0.2,0,'0'); text(-0.2,1,'1')            
            xlim([0 3.5*pi])
            ylim([0 1])
            text(0.1,0.9,'structure factor','FontSize',12)
            text(3.55*pi,0,'q L','FontSize',12)
             saveas(gcf,sprintf('./movies/frame%.1f.png',t),'png')
        end; % plot ?
                                    
    end; % t
end; % isim

toc(tstart)