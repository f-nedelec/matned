function mark2(t, x, indx, val, indx2, val2, which)

figname=['only t(:,' num2str(indx) ') == ' num2str(val)];
figure('Name',figname);
hold on;


sel = find( t(:,indx) == val );
selsel = find( t(sel,indx2) == val2 );
sel2=sel(selsel);

if ( length(sel2) == 0 ) 
   disp('empty , choose below:');
   disp([num2str(unique(t(:,indx2)))]);
   return
end

anplot( t(sel,:), x(sel,:) , 'k.', which );
anplot( t(sel2,:), x(sel2,:) , 'bx', which );


return