function im = mask_appy( image, mask, d, mode )

% im = mask_appy( im1, mask, d, mode )
%
% apply 'mask' translated by 'd' to 'image'
% 'mode' defines operation as follows:
%     0  : keep only the pixels in the mask (default)
%     1  : (invert mask) keep the pixels not falling in the mask

if  nargin < 4 ; mode = 0; end
if  nargin < 3 ; d = [ 0 0 ]; end

l  = max( [1 1], [1 1] + d );
u  = min( size(image), d + size(mask) );

if  mode == 0 
    
   im = zeros( size(image) );
   if  any( d + [ 1 1 ] > size(image) );  return; end
   if  any( d + size(mask) < [ 1 1 ] );   return; end
   im( l(1):u(1), l(2):u(2) ) = image( l(1):u(1), l(2):u(2) ) .* image_crop( mask, [ l-d,  u-d ], 1 );

else

    im = image;
    if  any( d + [ 1 1 ] > size(image) );  return; end
    if  any( d + size(mask) < [ 1 1 ] );   return; end
    im( l(1):u(1), l(2):u(2) ) = im( l(1):u(1), l(2):u(2) ) .* ( 1 - image_crop( mask, [ l-d,  u-d ], 1 ));

end

