
function res = ring_length(arg, make_plot)

% Francois Nedelec, 16.09.2016
%
% to analyze the length of ring simulated in Cytosim
%
% to generate the input files, run: reportF fiber:points 
%

if nargin < 2
    make_plot = 0;
end

if nargin > 1
    d = dir([arg, '/report*']);
else
    d = dir('report*');
end

%%

n_data = size(d,1);
res = zeros(n_data,1);

for i = 1:n_data
    file = d(i).name;
    data = load(file);
    [cff, len] = fourier_coefficients(data(:,3:4), 16, make_plot);
    res(i) = len;
end

%% make plot:

figure
plot(res);
xlabel('Frame (time)', 'FontSize', 14);
ylabel('Length', 'FontSize', 14);

end