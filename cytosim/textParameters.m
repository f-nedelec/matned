function result=textParameters( P, list )
%builds a string to represent a subset of the parameters P


if ( nargin < 2 )
    list = [ cellstr('magic'), cellstr('asinit'), cellstr('haspeed'), ...
             cellstr('asmtmax'), cellstr('hadetachrate'), cellstr('haenddetachrate'), ...
             cellstr('cxmax') ];
end

for f = 1 : size(list,2)

    name = char( list(f) );
    
    if isfield( P, name )
        
        field = getfield( P, name );
        
        line  = [sprintf('%15s  : ',name), sprintf(' %9.2f', field )]; 
        result(f, 1:size(line,2)) = line;
        
    end
end

return;
