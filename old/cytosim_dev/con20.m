function con8
%simulate two string of points, of fixed length and given rigidity
%implicit integration

global L M;
M        = 30;           %for the first mt;
L        = 1;           %length of the bonds
rigid    = 10;           %rigidity of the polymer

rkm      = rigid / ( L ^ 3 );

dt       = 1e-2;
nbsteps  = 20;
rec      = 20;

fb = 0.4;
%mobility of the rods, of length L
mu    = 1/L;
LN    = M * L;
mudt  = mu * dt;

figure('Position',[ 200, 200, 800, 800], 'MenuBar','None', 'Name','Implicit');
set(gca,'Position',[0 0 1 1]);

%vector x contains [x,y] of all successive points

x  = zeros(M*2, 1);          %positions of points

%initial position is an circle:
alp = 2 * pi / M;
r   = L / alp;
for i=1:M
   x(2*i-1) = cos( i * alp ) * r;
   x(2*i)   = sin( i * alp ) * r;
end


%building the matrix for rigidity forces:
R1 = rkm * RigidityMatrix(M);
R = zeros(2*M, 2*M);
R(1:2:2*M, 1:2:2*M) = R1;
R(2:2:2*M, 2:2:2*M) = R1;

substep = round( nbsteps / rec );
sav=0;

%start the timer:
tic

%==================================================================================

%integrating the motion:
for t=0:nbsteps
   
   %display the position
   if ( rem( t, substep ) == 0 )
      sav = sav + 1;
      plot( x(1:2:2*M-1), x(2:2:2*M), '-ko', 'LineWidth', 2, 'MarkerEdgeColor','b');
      hold on;
      plot( x([1, 2*M-1]), x([2, 2*M]), '-ko', 'LineWidth', 2, 'MarkerEdgeColor','b');
      axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);
      hold off;
      pause(0.1);
      %hold off;
      %saving position:
      sol(sav, 1:2*M) = x';
   end
      
   %building the jacobian of length constraints:
   J = zeros(M, 2*M);
   c = 1;
   for r = [ 1:M-1 ]
      J(c, 2*r + [-1, 0, 1, 2] ) = x( 2*r + [-1, 0, 1, 2] )' - x( 2*r + [ 1, 2, -1, 0] )';
      c = c + 1;
   end   
   J(c, [1, 2, 2*M-1, 2*M] ) = x( [1, 2, 2*M-1, 2*M] )' - x( [ 2*M-1, 2*M, 1, 2] )';
   c = c + 1;
      
   %J is rectangular, and J*J' is symetric definite positive
   
   P = ( eye( 2*M ) - J' * inv( J * J' ) * J );
   
   %implicit method:
   PL = inv( eye(2*M) - mudt * P * R );
   
   A = 0.01*( rand(2*M, 1) - 0.5 );
   x = PL \ ( x +  P * ( mudt * A ) ) ;
   
end
toc



%==================================================================================

%plot trajectory:
hold on;
for i=1:sav-1
   for j=1:M
      plot( [ sol(i,j*2-1), sol(i+1,j*2-1)], [sol(i,j*2), sol(i+1,j*2)], '-b');
   end
end
axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);

%plot some distances:
dis1 = sqrt( ( sol(:,4) - sol(:,2) ) .^ 2 + ( sol(:,3) - sol(:,1) ) .^ 2 );
dis2 = sqrt( ( sol(:,6) - sol(:,4) ) .^ 2 + ( sol(:,5) - sol(:,3) ) .^ 2 );
dis3 = sqrt( ( sol(:,8) - sol(:,6) ) .^ 2 + ( sol(:,7) - sol(:,5) ) .^ 2 );

figure('Name','Implicit-distances');
plot(dis1);
hold on;
plot(dis2);
plot(dis3);
hold off;


return;


function R = RigidityMatrix( N )

R = zeros(N,N);

for i=1:N;   R(i, i) = -6;  end
for i=1:N-1;   R(i, i+1) =  4; R(i+1, i) =  4;  end
R(N, 1) = 4; R(1, N) = 4;
for i=1:N-2;   R(i+2, i) = -1; R(i, i+2) = -1;  end
R(N-1,1) = -1; R(N,2) = -1;
R(1,N-1) = -1; R(2,N) = -1;

R
return;


      