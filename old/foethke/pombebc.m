function pombebc(indx)
%nedelec, March 2007. To compare location of minus-end in pombe simulation
%this produces a distribution of ase1-fluorescence, which can be compared
%to Carazo-salas et al, NCB 2006

figure;

color=['r', 'g', 'b', 'y', 'm'];

for ii = 1 : length(indx)
    
    i = indx(ii);
    e=load(sprintf('run%i/e',i));

    
    xl  = e(:,1);
    xr  = e(:,2);
    x   = ( xl + xr ) / 2;
    dx  = xr - xl;
    
    sm  = sum( dx .* e(:,3) );
    pd  = e(:,3) ./ sm;
    
    if exist('pds', 'var')
        pds = pds + pd;
    else
        pds = pd;
    end
        

    plot(x, pd, color(ii));
    hold on;
    
    %estimate a gaussian fit, of mean m and standard deviation s
    m = sum( x.*pd.*dx );
    s = sqrt( sum( x.*x.*pd.*dx ) - m*m );
    fprintf( '%i : %c : mean = %f, sigma = %f\n', i, color(ii), m, s);
    %check the fit:
    fit = 1/(s*sqrt(2*pi)) * exp( -(x-m).^2 / (2*s*s) );
    plot(x, fit, 'k:');
    
    %calculate the 75% threshold:
    %fprintf( '75%% at %f\n', 1.15*s);
    
end

pd = pds/length(indx);
m = sum( x.*pd.*dx );
s = sqrt( sum( x.*x.*pd.*dx ) - m*m );
fprintf( 'avg : mean = %f, sigma = %f\n', m, s);
%fprintf( '75%% at %f\n', 1.15*s);

plot(x, pd, 'k');
axis([-6 6 0 0.6]);
