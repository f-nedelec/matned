function res = load_spindle(filename, first_id)

% Copyright Laure Maillant and F. Nedelec, EMBL Heidelberg, 2014--2015
% Created June 2014 - Modified Jan 2015, July 2015

% mts = load_spindle(filename)
% 
% load_spindle loads microtubules coordinates from
% file whose name is filename (.mv3d format)
% 
% load_spindle returns a structure whose length is
% the number of different microtubules in the input file.
% Each cell of the structure contains an id and a matrix.
% Each row of the matrix corresponds to the coordinates (x,y,z) of one
% point on the considered microtubule. x y and z are converted to micrometers
%
%

if nargin < 2
    first_id = 1;
end

if nargin < 1
    fprintf('error');
end

file_handle = fopen(filename);

if file_handle == -1
    error('Could not find file "%s"\n', filename);
end


%% Takes each line and save the data

% Initialisation of results
pts = zeros(64, 3);
curid = [];
inx = 0;

chunk_inx = 0;
chunk_size = 1024;
chunk = cell(chunk_size, 1);

res = [];

while ~feof(file_handle)
    
    tline = fgets(file_handle);
    
    % Skip comments
    if tline(1) == '%'
        continue;
    end
    
    data = sscanf(tline, '%f %f %f %f %f');
    
    %Convert from Angstroms to micrometers:
    if numel(data) > 3
        id = data(1);
        pt = [ data(2)/10000 data(3)/10000 data(4)/10000 ];
    else
        continue
    end
    
    if id == curid
        % extend coordinates
        inx = inx + 1;
        pts(inx, :) = pt;
    else
        if inx > 0
            store_record(curid, pts(1:inx, :));
        end
        % initiate new fiber
        curid = id;
        pts(1,:) = pt;
        inx = 1;
    end

end

%record last entry:
store_record(curid, pts(1:inx, :));

if chunk_inx > 0
    res = cat(1, res, cell2mat(chunk(1:chunk_inx)));
end

fclose(file_handle);

%%
    function store_record(id, pts)
        %fprintf(2, 'store_record %i\n', id);
        mt.id  = id + first_id;
        mt.pts = pts;
        % record in chunk
        chunk_inx = chunk_inx + 1;
        chunk{chunk_inx} = mt;
        % transfer 'chunk' into 'res' if full
        if chunk_inx >= chunk_size
            if isempty(res)
                res = cell2mat(chunk);
            else
                res = cat(1, res, cell2mat(chunk));
            end
            chunk_inx = 0;
        end
    end

end


