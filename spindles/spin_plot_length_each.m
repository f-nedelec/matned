function polpol = spin_plot_length_each

% function spin_plot_length_each
%  plot the length of the objects which have been defined by clicks
%
% F. Nedelec, Feb. 2009
%
%

%% Find images
files = dir('*.pts');

if size(files,1) == 0
    error('No data found');
end

%% Collect data

for ii = 1:size(files,1)
    
    filename = files(ii).name;
    [pathstr, name] = fileparts(filename);

    try
        im = tiffread([name, '.lsm']);
    catch
        try
            im = tiffread(['../', name, '.lsm']);
        catch
            fprintf('No image found for "%s"\n', filename);
            continue;
        end
    end
    try
        lsm = im.lsm;
        pixel_size = 10^6 * lsm.VoxelSizeX;
    catch
        fprintf('No LSM-info found for "%s"\n', filename);
        continue;
    end

    pts = load(filename);
    dis = pixel_size * sqrt((pts(:,2)-pts(:,4)).^2 + (pts(:,3)-pts(:,5)).^2);

    %% Plot data
    polpol = dis;
    cnt = length(polpol);
    avg = mean(polpol);
    dev = std(polpol);
    msg = sprintf('%s : Length = %.2f +/- %.2f (N=%i)', name, avg, dev, cnt);
    fprintf('%s\n', msg);


    figure('Toolbar', 'none');
    plot(polpol, 'kx', 'MarkerSize', 14, 'LineWidth', 2);
    xlabel('Object', 'FontSize', 16, 'FontWeight', 'bold');
    ylabel('Length / um', 'FontSize', 16, 'FontWeight', 'bold');
    title(msg, 'FontSize', 20);

    ylim([0 45]);

end
