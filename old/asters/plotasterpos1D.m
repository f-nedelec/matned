function plotasterpos1D( name );

figure( 2 );
clf;

filename = [ name, '/aspos.out' ];
if exist(filename) == 0
    return; 
end
aspos = load(filename);


middle_x = mean( aspos(:,2) ) / 2 + mean( aspos(:,5) ) / 2;


plot( ( aspos(:,2) - middle_x ) / 15, 'b.', 'MarkerSize', 1);

hold on;

plot( ( aspos(:,5) - middle_x ) / 15, 'k.', 'MarkerSize', 1);

axis([0 size( aspos, 1 ) -1.5 1.5]);

drawnow;
%wait = input( [ name ' ( press return )'], 's' );
