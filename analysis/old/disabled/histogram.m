function h = histogram ( im );

% h = histogram( im );
% it calls imhist( im )
   
h = imhist( im );
