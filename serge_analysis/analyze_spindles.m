function [analysis,profiles] = analyze_spindles(image,opt)

% spindles = save_regions(spindles, filename)
% Save the spindles to file filename or 'spindles.txt'(default)
% S. Dmitrieff, Nov 2012

filename='spindles.txt';
regfilename='regions.txt';
pointfilename='points.txt';
contourfilename='contour.txt';
if nargin < 2
    opt=spin_default_options();
end
if isfield(opt,'seg_number')
    nc=opt.seg_number;
else
    defopt=spin_default_options;
    nc=defopt.seg_number;
end
%if isfield(opt,'clicks_filename')
%    clickfile=opt.clicks_filename;
%else
%    defopt=spin_default_options;
%    clickfile=defopt.clicks_filename;
%end
if isfield(opt,'max_polarity')
	npmax=opt.max_polarity;
else
	defopt=spin_default_options();
	npmax=defopt.max_polarity;
end
if  isfield(image, 'data') 
    if ( length(image) > 1 ) 
        disp('show_image displaying picture 1 only');
    end
    image = image(1).data;
end
% compatibility with tiffread color image
if  iscell(image)    
    tmp = image;
    image = zeros([size(tmp{1}), 3]);
    try
        for c = 1:numel(tmp)
            image(:,:,c) = tmp{c};
        end
    catch
        disp('show_image failed to assemble RGB image');
    end
    clear tmp;
end
if isfield(opt,'radius')
    rad=opt.radius;
else
    defopt=spin_default_options();
    rad=defopt.radius;
end

spindles=load_objects(filename);
clickets=load_objects(pointfilename);
regions=load_regions(regfilename);
% Analysis of spindles
num_spindles=numel(spindles);
n_reg=numel(regions);
n_poles=numel(clickets);
xmax=size(image,1);
ymax=size(image,2);

% Variables
n_spinds=zeros(1,npmax+1);
seg_intens=cell(1,npmax+1); %intensity of segments per unit lengths
seg_width=cell(1,npmax+1); % width of segments
seg_dens=cell(1,npmax+1); % density (intens / width) of segments
tot_intens=cell(1,npmax+1); % total intensity per spindle (NOT per unit length)
tot_lengs=cell(1,npmax+1); % length of spindles along the segments =/= center-pole dist
tot_aspect=cell(1,npmax+1); %Aspect ration of the spindle (slope of width(l))
np=0;
contour=load_objects(contourfilename);
for n = 1:n_poles
    pl=clickets{n};
    clicks=pl.points;
    state=str2num(pl.info);
    if state>0
        center=clicks(1,:);
        points=clicks;
        coords=[center center]-[rad rad -rad -rad];
        xp_min=min(points(:,1));
        xp_max=max(points(:,1));
        yp_min=min(points(:,2));
        yp_max=max(points(:,2));
        % Making sure clicks are in the image, and image is well defined
        coords(1)=max( min(coords(1),xp_min) , 1);
        coords(2)=max( min(coords(2),yp_min) , 1);
        coords(3)=min( max(coords(3),xp_max) , xmax);
        coords(4)=min( max(coords(4),yp_max) , ymax);
        % Starting the analysis
        points=points-ones(nc+1,1)*coords(1:2);
        st=state+1;
        n_spinds(st)=n_spinds(st)+1/state;
        im=image(coords(1):coords(3),coords(2):coords(4));
        im=im-image_background(im);
        [profiles]=analyze_intens_onespind( points , im );
        lengths=segments_lengths(points);
        ints=zeros(1,nc);
        rms=zeros(1,nc);
        mea=zeros(1,nc);
        [right,projright]=dist_points_segments(contour{2*n-1}.points,clicks);
        [left,projleft]=dist_points_segments(contour{2*n}.points,clicks);
        for k=1:nc
            pro=profiles{k};
            lp=length(pro);
            i=right(k)+lp/2;
            j= left(k)+lp/2;
            ints(1,k)=sum(pro(i:j));
        end
        rms(1,:) = abs(left(:)'-right(:)');
        mea(1,:) = (left(:)'+right(:)' )/2;
        p=-(polyfit(projright,right,1)-polyfit(projleft,left,1));
        seg_intens{st}=[seg_intens{st} ; ints./lengths];
        tot_intens{st}=[tot_intens{st} ; sum(ints) ];
        seg_width{st}=[seg_width{st} ; rms];
        tot_lengs{st}=[tot_lengs{st} ; sum(lengths)];
        seg_dens{st}=[seg_dens{st} ; ints./(lengths.*rms)];
        tot_aspect{st}=[tot_aspect{st} ; p(1)];
    end
end
n1=n_spinds(2);
n2=n_spinds(3);
nm=sum(n_spinds(4:npmax));
ntot_analyzed=sum(n_spinds(1:npmax));
analysis.numbers=n_spinds;
analysis.fraction_mon = n1/ntot_analyzed;
analysis.fraction_bi   = n2/ntot_analyzed;
analysis.fraction_multi= nm/ntot_analyzed;
%-   Tubulin analysis
analysis.seg_intens_mean=zeros(npmax+1,nc);
analysis.seg_intens_err=zeros(npmax+1,nc);
analysis.seg_width_mean=zeros(npmax+1,nc);
analysis.seg_width_err=zeros(npmax+1,nc);
analysis.seg_dens_mean=zeros(npmax+1,nc);
analysis.seg_dens_err=zeros(npmax+1,nc);
analysis.tot_intens_mean=zeros(npmax+1,1);
analysis.tot_intens_err=zeros(npmax+1,1);
analysis.tot_lengs_mean=zeros(npmax+1,1);
analysis.tot_lengs_err=zeros(npmax+1,1);
analysis.tot_aspect_mean=zeros(npmax+1,1);
analysis.tot_aspect_err=zeros(npmax+1,1);
for i=1:npmax+1
    if n_spinds(i)>0
        analysis.seg_intens_mean(i,:)=mean(seg_intens{i},1);
        analysis.seg_intens_err(i,:)=sqrt(mean(seg_intens{i}.^2,1)-mean(seg_intens{i},1).^2);
        analysis.seg_width_mean(i,:)=mean(seg_width{i},1);
        analysis.seg_width_err(i,:)=sqrt(mean(seg_width{i}.^2,1)-mean(seg_width{i},1).^2);
        analysis.seg_dens_mean(i,:)=mean(seg_dens{i},1);
        analysis.seg_dens_err(i,:)=sqrt(mean(seg_dens{i}.^2,1)-mean(seg_dens{i},1).^2);
        analysis.tot_intens_mean(i)=mean(tot_intens{i});
        analysis.tot_intens_err(i)=sqrt(mean(tot_intens{i}.^2)-mean(tot_intens{i})^2);
        analysis.tot_lengs_mean(i)=mean(tot_lengs{i});
        analysis.tot_lengs_err(i)=sqrt(mean(tot_lengs{i}.^2)-mean(tot_lengs{i})^2);
        analysis.tot_aspect_mean(i)=mean(tot_aspect{i} );
        analysis.tot_aspect_err(i)=sqrt(mean(tot_aspect{i}.^2)-mean(tot_aspect{i})^2);
    end
end
% Saving 
save_analyze(analysis,opt);

end
