function plotCatastrophesFig(data)

% plot the locations of catastrophes in 'mtcontacts.sta' for figure 3
% F. nedelec, Feb 2008

if ~exist('mtcontacts.cut')
    system('cut -c2- mtcontacts.sta | tail -n +2 > mtcontacts.cut');
end
if nargin < 1
    data = load('mtcontacts.cut');
end

%dimensionality of simulation
dim = ( size(data,2) - 4 ) / 2;
%extract position of direction information:
indxP = size(data,2) - 2*dim + 1;
indxD = size(data,2) - dim + 1;

%select only bundle catastrophes
bundles = logical( data(:,2)==1 );
singles = logical( data(:,2)==0 );

fprintf('%i singles, %i bundle catastrophes\n', sum(singles), sum(bundles));

XYZ = data(:, indxP + [0,1,2] );
X = abs( XYZ(:,1) );
R = sqrt( XYZ(:,2).^2 + XYZ(:,3).^2 );


%% plot single events in X-R:
figure('name', pwd, 'Position', [150 150 900 400]);
hold on;

plot( X(bundles,1), R(bundles), 'ko', 'MarkerFaceColor', 'k');
plot( X(singles,1), R(singles), 'k^', 'MarkerFaceColor', 'w');


xlabel('|x| (\mu m)', 'FontSize', 20);
ylabel('distance to cell-axis (\mu m)', 'FontSize', 20);
set(gca, 'FontSize', 20);
axis equal;
axis([0 +6 0 +2.5]);
box on;


%% Plot density with colors
%plot X-R:

XR = cat(2,X,R);
xe = 0.1:0.2:6.1;
ye = 0.1:0.2:2.0;

cB = histc2d(XR(bundles,:), xe, ye);
cS = histc2d(XR(singles,:), xe, ye);

bits=2^8;
fact=10;

imB = uint8( fact * cB * ( bits / max(max(cB)) ));
imS = uint8( fact * cS * ( bits / max(max(cS)) ));



figure('Position', [200 400 650 480], 'Name', 'Density of catastrophes');

ahB = axes('Position', [0.14 0.53 0.8 0.4]); 
imhB = image(xe, ye, imB);
set(gca, 'YDir', 'normal');
colormap( gray(bits) );
ylabel('R  / \mum', 'FontSize', 20);
set(ahB, 'XTickLabel', []);

ahS = axes('Position', [0.14 0.10 0.8 0.4]); 
imhS = image(xe, ye, imS);
colormap( gray(bits) );

caxis(ahS, [0 bits]);
caxis(ahB, [0 bits]);

h = [ ahS, ahB ];
axis(h, 'equal');
axis(h, [0 +6 0 +2]);
set(h, 'FontSize', 18);

%add a colorbar:
%colorbar();
    
xlabel('|x|  / \mum', 'FontSize', 20);
ylabel('R  / \mum', 'FontSize', 20);

end
