function [ps, ds, nb] = average( param, data, indx, nbmust )

% [ps, ds] = average( param, data, indx, nbmust )
%
% average all data which only differ by the value of param(indx)
% indx may be an set of indices
% if nb is given, reject data bins that do not contain nbmust values

others = setdiff( [1:size(param,2)], indx );
tr     = param(:, others);
ps     = unique( tr, 'rows' );
ds     = zeros( size( ps,1 ), 1 );
nb     = zeros( size( ps,1 ), 1 );

for u=1:length(ps)
   
   equal = zeros(size(tr,1),1);
   
   for i=1:size(tr, 1)
      equal(i) = all( tr(i,:) == ps(u,:) );
   end
   sel = find( equal == 1 );
   
   ds(u) = mean( data(sel,:) , 1 );
   nb(u) = size( sel, 1 );
end

if ( exist('nbmust','var') )
   
   g = find( ismember(nb, nbmust) );
   ds = ds( g );
   ps = ps( g, : );
   nb = nb( g );
   
end

return