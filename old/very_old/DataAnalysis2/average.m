function da = average(data, indx)
% average and supress values of indx;

ds = size(data, 2);
d  = zeros(size(data));
others = setdiff( [1:ds-1], indx);

uniq = unique( data(:, others), 'rows' );

k=0;
for u=1:size(uniq, 1)
   
   j=0;
   for i=1:size(data, 1)
      if  all( data(i, others) == uniq(u,:) )
         j = j + 1;
         d(j, 1:ds) = data(i, 1:ds);
      end
   end
   
   if ( j )
      k = k + 1;
      da( k, 1:ds-length(indx)-1 ) = d(1, others );
      da( k, ds-length(indx) ) = mean( d(1:j, ds), 1 );
   end
end

