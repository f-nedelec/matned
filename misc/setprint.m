function setprint(scale)

if nargin < 1
    scale = 0.9;
end

%=============prepare for printing on A4:

width  = 21;
height = 29.7;

set(gcf, 'PaperUnits','centimeters');
set(gcf, 'PaperPositionMode','manual');
set(gcf, 'PaperType','A4');
set(gcf, 'PaperSize', [width height]);
get(gcf, 'PaperSize')
set(gcf, 'PaperOrientation','portrait');
%set(gcf, 'PaperOrientation','landscape');


figpos  = get(gcf, 'Position');
scaleX  = width / figpos(3);
scaleY  = height / figpos(4);
scale   = scale * min( scaleX, scaleY );

widthP  = scale * figpos(3);
heightP = scale * figpos(4);
left    = (  width - widthP ) / 2;
bottom  = ( height - heightP ) / 2;

set(gcf,'PaperPosition',[left, bottom, widthP, heightP ]);


return;
