function testlogs(distance, mmo);

figure(1)

for indx=1:5
   dz = 0.001 * indx;
   mmoz=mmo+dz;
   
   %compute the loglog-slope:
   m  = round( interp1(distance, 1:length(distance), 3) );
   MM = 100;
   lx = log( distance(m:MM) );
   lc = log( mmoz(m:MM) );
   thslope = slope(lx, lc)
   plot(lx,lc);
   
   
   hold on; 
   dz
end
