function scanhideki;

files=dir('16um*');

for u=1:size(files, 1)
   
   filename = files(u).name;
   
   disp( filename );
   
   if ( exist( filename, 'file' ) == 2 )
       
       im = tiffread( filename );
       showim( im, 'saturated' );

   end
       
end