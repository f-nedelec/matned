
cnt = 1000000;
x = -log(rand(1, cnt)) - 10;
y = -log(rand(1, cnt));

if 0
    figure
    subplot(2,1,1);
    hist(max(x,y), 100);
    xlim([0,10]);
    %mean(max(x,y))
end
if 0
    subplot(2,1,2);
    hist(x+y, 100);
    xlim([0,10]);
    %mean(x+y)
end

s = 1;
while s
    i = logical( x < 0 );
    s = sum(i);
    x(i) = x(i) - log(rand(1, s));
end

figure
hist(x, 100);
mean(x)