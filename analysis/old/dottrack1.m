function [ cen, i ] = dottrack1( im, cen )

global debug;
global searchR minPixels;

if (debug==4)
   debugfig = show1(im);
   plot(cen(2), cen(1),'bo','MarkerSize',10);
end

searchM    = maskcircle1( 2 * searchR + 1);

if (debug==12) 
   show1(im, 'scale 8bit', cen );
   plot(cen(2), cen(1),'yo','MarkerSize',10,'LineWidth',5);
end

c = round( cen );

for i=1:300
   
   co = c;
   c  = round ( cen );
   %refine the centers:
   
   im2   = image_crop( im, [ c - searchR, c + searchR ], 0 );
   im2   = im2 .* searchM;
   base  = valuetop( im2, minPixels );
   above = im2 - base;
   above = ( above > 0 ) .* above;
   cen   = c + barycenter1( above ) - ( searchR + 1 );
        
   %make sure we stay inside:
   cen = max( [ 1 1 ] , cen );
   cen = min( size(im), cen );
   
   if (debug==12) 
      plot(cen(2), cen(1),'yx','MarkerSize',10,'LineWidth',5);
   end
  
   %check for convergence:
   if ( round(cen(1)) == c(1) ) & ( round(cen(2)) == c(2) )
      break;
   end
   if ( round(cen(1)) == co(1) ) & ( round(cen(2)) == co(2) )
      break;
   end
   
end

if (debug==11) 
   show1(above, 'scale 8bit', cen ); 
   fprintf('conv: %i, dc = %.2f %.2f ', i, dc(1), dc(2));
end

return;