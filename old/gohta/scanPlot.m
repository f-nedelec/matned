function scanPlot(numbers);


if  nargin < 1
    files      = gohta_dir();
else
    for ii=1:length(numbers)
        files(ii).name = sprintf('data%04i', numbers(ii));
        files(ii).isdir = isdir( files(ii).name );
    end
end

nfiles = size(files,1);

u = 0;
while  ( u < nfiles )
    
    u = u + 1;
    
    filename = files(u).name;
    data_nb  = sscanf(filename, 'data%d');
    params   = getParameters( filename );
    
    %if ( params.cxmax(1) > 0 )
    %    fprintf(1, '%s\n', filename);
    %    continue;
    %end

    if ( 1 == files(u).isdir ) & ( filename(1) ~= '.' )
        
        plotFocussing(filename, (u == 1) );
        
        if ( ~skip & ( u < nfiles ))
            
            k = waitforbuttonpress;

            switch( k )
                case 1  %key pressed detected

                    switch( get( gcf, 'CurrentCharacter') )
                        case 'q' %quit
                            break;
                        case 'b' %back to previous plot
                            if ( u > 1 )
                                u = u - 2;
                            end
                        case 'p' %back to previous plot
                            if ( u > 1 )
                                u = u - 2;
                            end
                    end

                case 0       % button down detected

                    p = get(gca,'CurrentPoint');

            end
        end        
        
    end
end
