function result = track( filename, im )

% function result = track( filename, im )
%
% manual tracking of microtubule from an aster.
% keyboard commands:
%   'c'         set the center by mouse click
%   'b'         back one frame
%   'n'         next frame
%   'q'         quit
%   '1' & '2' : change color-scale min level
%   '3' & '4' : change color-scale max level
%
% F. Nedelec, nedelec@embl-heidelberg.de  last modified Sept 7 2002

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select movie file');
    cd(pathname);
end

if ischar( filename )
    im = tiffread( filename );
else
    if ( nargin < 2 )
        im = filename;
        filename = 'file not specified';
    else
        tmp = im;
        im = filename;
        filename = tmp;
    end
end

im_nb  = 1;
im_max = length(im);
redraw = 0;

result  = zeros(im_max+1, 4);

figplot = figure('Position', [730,500,550, 350]);
clf;

cpic = double( im(im_nb).data );
[figID, hIm] = showim( cpic );

cmin = min(min( cpic ));
cmax = max(max( cpic ));

hText  = text( 10, 10, ['1 / ', num2str(im_max)], 'color', 'g' );
hclick = plot( 0, 0 , '.');

center  = findbrightspot( cpic );
hcenter = plot( center(2), center(1), 'bo' );

pointer = get(gcf, 'pointer');
set(gcf, 'pointer', 'fullcrosshair');

while 1

    figure(figID);
    if ( redraw == 1 )
                
        %================ show the picture in gray scale
        cpic = double( im(im_nb).data );
    
        scal = 256 / ( cmax - cmin );
        pix = uint8( scal * ( cpic - cmin ));
    
        set( hIm, 'CData', pix );
        set( hText, 'string', [num2str(im_nb), ' / ', num2str(im_max)] );

        
        %================ move the point at place of last click
        set(hclick, 'xdata', result(im_nb, 3) );
        set(hclick, 'ydata', result(im_nb, 2) );
        
        redraw = 0;
        
    end
    
    k = waitforbuttonpress;
    
    if ( k == 1 )
        
        switch( get( figID, 'CurrentCharacter' ) )
        case 'q'
            break;
            
        case 'c'
            %================================== set the center of aster by a click:
            k = waitforbuttonpress;
            if ( k == 0 )
                point = get(gca, 'CurrentPoint');     % button down detected
                center = point(1, 2:-1:1);                 % extract x and y
                
                set(hcenter, 'xdata', center(2));
                set(hcenter, 'ydata', center(1));
                
                for nb = 1:im_max
                    if ( result(nb, 1) == 1 )
                        result(nb, 4) = sqrt( sum( ( result(nb,2:3) - center ) .^ 2 ) );
                    end
                end
                
                if any( result(:,1) == 1 )
                    figure(figplot);
                    clf;
                    plot(result(:,4), '.');
                    axis([ 1, im_max, 0, max(result(:,4)) ]);
                end
            end
            
        case 'n'
            if ( im_nb < im_max ) 
                im_nb = im_nb + 1;
                redraw = 1;
            end
            
        case 'b'
            if ( im_nb > 1 ) 
                im_nb = im_nb - 1;
                redraw = 1;    
            end
            
        case '1'
            cmin = cmin - 20;
            redraw = 1;
        case '2'
            cmin = cmin + 20;
            redraw = 1;
        case '3'
            cmax = cmax - 20;
            redraw = 1;
        case '4'
            cmax = cmax + 20;
            redraw = 1;
        otherwise
            
        end
        
    else
        
        point = get(gca, 'CurrentPoint');     % button down detected
        P = point(1, 2:-1:1);                 % extract x and y
        
        if strcmp( get( figID, 'SelectionType' ) ,'alt' )
            
            %local autoscale:
            rect = round( [ P - 10, P + 10 ] );
            sample = crop(im(im_nb), rect);
            cmin = min(min( sample ));
            cmax = max(max( sample ));
            redraw = 1;
            
        else
                
            result(im_nb, :) = [1, P, 0];
        
            if exist('center','var')
                result(im_nb, 4) = sqrt( sum( ( P - center ) .^ 2 ) );
                figure(figplot);
                clf;
                plot(result(:,4), '.');
                axis([ 1, im_max, 0, max(result(:,4)) ]);
            end
        
            if ( im_nb < im_max )
                im_nb = im_nb + 1;
                redraw = 1;
            else
                beep;
            end
        end
    end
    

end

close;
set( gcf, 'pointer', pointer );

%=============save:

if ~ strcmp( filename, 'file not specified' )
    
    result(im_max+1, 1:3) = [2, center];
    
    dataname = strrep(filename, '.stk', '.dat');
    save(dataname, 'result', '-ascii');
    disp(['saved in ', dataname] );
    
end

%=============prepare for printing:

setprint;

return;
