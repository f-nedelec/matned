function [im, L] = fakeaster( length, nbmt )

noise = 3;
im = zeros( length );

cx = length * ( 0.5 + 0.3 * ( rand - 0.5 ));
cy = length * ( 0.5 + 0.3 * ( rand - 0.5 ));

for mt = 1:nbmt

    %if rem( i, 3 )
        L( mt ) = 0.1 * length * exp( rand );
        %else
    %    L( mt ) = 0.3  * size;
    %end
    
    angle = rand * 2 * pi;
    ca = cos( angle );
    sa = sin( angle );
    
    ab = 0 : 0.1 : L( mt );
    
    mtx = round( cx + ca * ab + noise * ( rand(size(ab)) - 0.5 ) );
    mty = round( cy + sa * ab + noise * ( rand(size(ab)) - 0.5 ) );
   
    inside = ( mtx > 0 ) & ( mtx < length ) & ( mty > 0 ) & ( mty < length );
        
    for i = 1:size(mtx,2)
        if inside( i )
            im( mtx(i), mty(i) ) = im( mtx(i), mty(i) ) + 1;
        end
    end
end

