%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  plotData.m 
%  Dietrich Foethke 2004
%  foethke@embl.de
%  CVS: $Id: plotData.m,v 1.3 2004/09/28 16:15:47 foethke Exp $
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% The calling function has to provide a global struct called GPD
% (Global Plot Data). We need a global struct to achieve better
% performance. Unfortunately there's no other way, since matlab
% can't handle pointers and addresses.
% GPD has to contain a data field that holds the (x, y) coordinates
% and optionally the color of the data. This field must have
% dimensions [n, 2] for coordinates only, or [n, 3] for
% coordinates + color. For the color just specify any range of
% values. They will be mapped onto the colorbar.
% Optionally GPD might contain a field that holds an information
% string for each data point. This should be a cell array of
% strings of dimension [n, 1];
% Furthermore GPD might contain a field containing a string that's
% executed by a system call if you click on a data point. This
% should be a cell array of strings of dimension [n, 1];
%
%
% The fieldnames for the struct GPD are arbitrary. They have to be
% specified in the struct 'fields' that's passed as the first
% argument to plotData:
%
% The only mandatory field in 'fields' is 'data'. It's a string
% that should contain the name of the data field in GPD:
%
% fields.data = 'nameOfTheDataFieldInGPD'
%
% Optional fields are:
%
% fields.info = 'nameOfTheInfoStringFieldInGPD'
% fields.exec = 'nameOfTheExecStringFieldInGPD'
%
%
% The second argument to plotData is optional. It's again a struct,
% the can be used to define some properties of the plot:
%
% PD.xLabel          - label for the x axis (string)
% PD.yLabel          - label for the y axis (string)
% PD.markerType      - type of marker: 1 for circles, 2 for
%                      diamonds, default: 2
% PD.markerSize      - size of markers in pixels, default: 9
% PD.unicolor        - marker color (used only if no color data is
%                      specified in GPD). Should be an integer
%                      between 1 and the size of the colormap,
%                      which is 129 by default.
%                      default: min(72,cmapSize)
% PD.showColorBar    - 0 to hide the colorbar and 1 to show it
%                      default: 1 if color array is given, 0 if not
% PD.colorbar        - the colormap for the plot. Must be an array
%                      with RGB values of size (n,3) with n >= 2.
%                      The first color is used for the background.
%                      default: [ 1 1 1; hsv(128) ]


% create a figure and draw the data points;
% set all the options for the plot, including callback functions;
% call createSelMat to create the selection matrix
function plotData( fields, PD )
  
  global GPD;

  % check 'fields' fields for reasonable input
  if ~ isfield(fields, 'data')
    error('struct ''fields'' doesn''t contain field ''data''!');
  else
    if ~ ischar(fields.data)
      error('fields.data doesn''t contain a valid string!');
    else
      if ~ isfield(GPD, fields.data);
        error( sprintf( 'GPD.%s is missing - no data supplied for plotting!', fields.data ));
      end
    end
  end
 if (size(GPD.(fields.data),2) < 2) || (size(GPD.(fields.data),2) > 3)
   error( sprintf( 'width of array GPD.%s must be 2: [x y] or 3: [x y color]!', fields.data ));
 end
  
  if isfield(fields, 'info')
    if ~ ischar(fields.info)
      error('fields.info doesn''t contain a valid string!');
    else
      if isfield(GPD, fields.info)
        if size(GPD.(fields.data),1) ~= size(GPD.(fields.info),1)
          error( sprintf( 'size of GPD.%s and GPD.%s does not match!', ...
                          fields.data, fields.info ));
        end
      end
    end
  end
  if isfield(fields, 'exec')
    if ~ ischar(fields.exec)
      error('fields.exec doesn''t contain a valid string!');
    else
      if isfield(GPD, fields.exec)
        if size(GPD.(fields.data),1) ~= size(GPD.(fields.exec),1)
          error( sprintf( 'size of GPD.%s and GPD.%s does not match!', ...
                          fields.data, fields.exec ));
        end
      end
    end
  end

  
  % create unused fieldnames for the global struct GPD and add the
  % names to 'fields'
  for n=1:100
    fieldname = sprintf('selMat%d', n);
    if ~ isfield(GPD, fieldname)
      fields.selMat = fieldname;
      break
    end
  end
  if n >= 100
    error('no names left for ''selMat'' in global struct GPD!');
  end
  for n=1:100
    fieldname = sprintf('selTextHandle%d', n);
    if ~ isfield(GPD, fieldname)
      fields.selTextHandle = fieldname;
      break
    end
  end
  if n >= 100
    error('no names left for ''selTextHandle'' in global struct GPD!');
  end
  for n=1:100
    fieldname = sprintf('lineHandle%d', n);
    if ~ isfield(GPD, fieldname)
      fields.lineHandle = fieldname;
      break
    end
  end
  if n >= 100
    error('no names left for ''lineHandle'' in global struct GPD!');
  end
  for n=1:100
    fieldname = sprintf('axesPos%d', n);
    if ~ isfield(GPD, fieldname)
      fields.axesPos = fieldname;
      break
    end
  end
  if n >= 100
    error('no names left for ''axesPos'' in global struct GPD!');
  end
  
  % set plot parameters in struct PD
  PD.numPts            = size(GPD.(fields.data),1);
  PD.dataMax           = [max(GPD.(fields.data)(:,1)), max(GPD.(fields.data)(:,2))];
  PD.dataMin           = [min(GPD.(fields.data)(:,1)), min(GPD.(fields.data)(:,2))];
  PD.dataSize          = PD.dataMax - PD.dataMin;
  PD.axesPos           = [80, 60, 620, 480];
  if isfield(PD, 'markerType')
    if ((rem(PD.markerType,1) > 0) || ...
        (PD.markerType < 1) || ...
        (PD.markerType > 2))
      error('PD.markerType must be an integer between 1 and 2!');
    end
  else
    % set default
    PD.markerType = 2;
  end
  if isfield(PD, 'markerSize')
    if ((rem(PD.markerSize,1) > 0) || ...
        (PD.markerSize < 1))
      error('PD.markerSize must be an integer > 0!');
    end
  else
    % set default
    PD.markerSize = 9;
  end
  if isfield(PD, 'colormap')
    if size(PD.colormap,1) < 2
      error(['PD.colormap must contain at least two colors: ' ...
             'i.e. background and marker!' ]);
    end
    if (size(PD.colormap,2) ~= 3)
      error(['colors must be given as RGB values!']);
    end
  else
    % set default
    PD.colormap = [ 1 1 1; hsv(128) ];
  end
  PD.cmapSize = size(PD.colormap, 1);
  if isfield(PD, 'unicolor')
    if ((rem(PD.unicolor,1) > 0) || ...
        (PD.unicolor < 1) || ...
        (PD.unicolor > PD.cmapSize))
      error(sprintf('PD.unicolor must be an integer between 1 and %d!', ...
            PD.cmapSize ));
    end
  else
    % set default
    PD.unicolor = min(72,PD.cmapSize);
  end
  if isfield(PD, 'showColorBar')
    if ((rem(PD.showColorBar,1) > 0) || ...
        (~((PD.showColorBar == 0) || ...
           (PD.showColorBar == 1))))
      error('PD.showColorBar must be either 0 or 1!');
    end
  else
    % set default
    if (size(GPD.(fields.data),2) == 3)
      PD.showColorBar = 1;
    else
      PD.showColorBar = 0;    
    end
  end
  
  
  % calculate the color values, if there are any
  if (size(GPD.(fields.data),2) == 3)
    % color given as an arbitrary range of values
    PD.highCol  = max(GPD.(fields.data)(:,3));
    PD.lowCol   = min(GPD.(fields.data)(:,3));
    colRange    = PD.highCol - PD.lowCol;
    if (colRange <= 0)
      PD.color  = PD.unicolor*ones(PD.numPts,1);
    else
      PD.color  = zeros(PD.numPts,1);
      for n=1:PD.numPts
        PD.color(n) = round((GPD.(fields.data)(n,3)-PD.lowCol)/colRange*(PD.cmapSize-2))+2;
      end
    end
  else
    PD.color    = PD.unicolor*ones(PD.numPts,1);
    PD.lowCol   = 1;
    PD.highCol  = PD.cmapSize;
  end

  
  % add a pad of size PD.markerSize on each side
  % to ensure that the markers won't be cut off
  pad(1)      = PD.markerSize/PD.axesPos(3)*PD.dataSize(1);
  pad(2)      = PD.markerSize/PD.axesPos(4)*PD.dataSize(2);
  PD.dataMax  = PD.dataMax + pad;
  PD.dataMin  = PD.dataMin - pad;
  PD.dataSize = PD.dataMax - PD.dataMin;
  
  
  % calculate nice axes
  scale       = 10.^round(log(PD.dataSize)./log(10))./10;
  PD.dataMax  = ceil(PD.dataMax./scale).*scale;
  PD.dataMin  = floor(PD.dataMin./scale).*scale;
  PD.dataSize = PD.dataMax - PD.dataMin;

  
  % create figure and axes
  PD.figHandle  = figure( 'Position', [100,100,800,600] );
  PD.axesHandle = axes('Units', 'Pixels', 'Position', PD.axesPos);  
  set(PD.figHandle, 'Pointer', 'crosshair');
  set(PD.axesHandle, 'Color', get(PD.figHandle, 'Color'));
  axis( [PD.dataMin(1), PD.dataMax(1), PD.dataMin(2), PD.dataMax(2)] );
  axis manual;
  
  
  % create colormap
  colormap(PD.colormap);
  
  if PD.showColorBar
    % create colorbar
    PD.colHandle = colorbar('location', 'EastOutside');
    set(PD.colHandle, 'YLim', [2, PD.cmapSize+1]);
  
    % restore axes position after addition of colorbar
    % and set a nice colorbar length - matlab does strange things here!
    set(PD.axesHandle, 'Units', 'Pixels', 'Position', PD.axesPos);
    set(PD.axesHandle, 'Units', 'normalized');
    axesPos  = get(PD.axesHandle, 'Position');
    colorPos = get(PD.colHandle, 'Position');
    %set(PD.colHandle, 'Position', [colorPos(1), axesPos(2), colorPos(3), axesPos(4)]);

  
    % set tick strings for the colorbar
    ticks = get(PD.colHandle, 'YTickLabel');
    if PD.cmapSize == 2
      newticks = PD.highCol;
    else
      newticks = (str2num(ticks) - 2.5) ./ (PD.cmapSize-2) *(PD.highCol - PD.lowCol) + PD.lowCol;
    end
    ticksstr = num2str(newticks);
    set(PD.colHandle, 'YTickLabel', ticksstr);
  end
  
  
  % calculate positions of markers in pixels
  PD.dataPos = zeros(PD.numPts, 2);
  for k=1:PD.numPts
%    PD.dataPos(k,[1 2]) = round(PD.axesPos([3, 4]).*(GPD.(fields.data)(k,[1 2]) - PD.dataMin)./PD.dataSize);
    PD.dataPos(k,[1 2]) = ceil(PD.axesPos([3, 4]).*(GPD.(fields.data)(k,[1 2]) - PD.dataMin)./PD.dataSize);
%    PD.dataPos(k,[1 2]) = floor(PD.axesPos([3, 4]).*(GPD.(fields.data)(k,[1 2]) - PD.dataMin)./PD.dataSize);
  end
  
  
  % create the Image and show it
  hold on
  imMat = createImage( PD );
  image( 'CData', imMat, 'Clipping', 'off', 'XData', [PD.dataMin(1) PD.dataMax(1)], 'YData', [PD.dataMin(2), PD.dataMax(2)] );

  
  % This code is stolen from the matlab 'axis tight' command
  % It's used to finally resize the axes, such that the image AND
  % the axes, tickmarks etc. fit in, and the image does'nt become distorted
  realLimits = objbounds(findall(PD.axesHandle));
  set(PD.axesHandle, 'XLim', [realLimits(1), realLimits(2)]);
  set(PD.axesHandle, 'YLim', [realLimits(3), realLimits(4)]);
  set(PD.axesHandle, 'Layer', 'Top');
  
  
  % set axes label
  if ( isfield(PD, 'xLabel') && ischar(PD.xLabel) )
    set(get(PD.axesHandle, 'XLabel'), 'String', PD.xLabel);
  end
  if ( isfield(PD, 'yLabel') && ischar(PD.yLabel) )
    set(get(PD.axesHandle, 'YLabel'), 'String', PD.yLabel);
  end

  
  % define a marker and text for a selected data point
  GPD.(fields.lineHandle)    = line( 'XData', 0, 'YData', 0, 'LineStyle', 'none', ...
                                     'Marker', 'x', 'MarkerSize', 11, 'MarkerFaceColor', [0 0 0], ...
                                     'Visible', 'Off' );
  GPD.(fields.selTextHandle) = text( 0, -50, '', 'Units', 'Pixels', ...
                                     'Visible', 'Off' );
  
  % initialise some values in GPD
  GPD.(fields.axesPos)       = PD.axesPos;
  GPD.(fields.selMat)        = createSelMat( PD );

  set(PD.figHandle, 'userdata', fields);
  set(PD.figHandle, 'Pointer', 'crosshair')
  set(PD.figHandle, 'WindowButtonDownFcn', @buttonDownCallback);
  set(PD.figHandle, 'WindowButtonMotionFcn', @buttonMotionCallback);
  set(PD.figHandle, 'KeyPressFcn', @keyPressCallback);
  
  return;



% this function defines the string that's printed when the mouse is
% moved over an object; 'sel' holds the index of the selected
% object
function prtStr=printSelection( sel )
  fields   = get(gcbf, 'userdata');
  global GPD;
  if ( isfield(fields, 'info') && ...
       isfield(GPD, fields.info) && ...
       (size(GPD.(fields.info),1) >= sel) && ...
       (length(GPD.(fields.info){sel}) > 0) )
    prtStr = GPD.(fields.info){sel};
  else
    prtStr = '';
  end
  return;
  
  
  
% this function can be used to define the action that's taken, when
% the user clicks on an object; 'sel' holds the index of the
% selected object
function clickAction( sel )
  fields   = get(gcbf, 'userdata');
  global GPD;
  if ( isfield(fields, 'exec') && ...
       isfield(GPD, fields.exec) && ...
       (size(GPD.(fields.exec),1) >= sel) && ...
       (length(GPD.(fields.exec){sel}) > 0) )       
    system(GPD.(fields.exec){sel});
  end
  return;
  
  
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                    Nothing to change beyond this point
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function imMat = createImage( PD )  
  % create the image matrix
  imMat = insertMat( PD.axesPos([4, 3]), PD.markerSize, PD.markerType, ...
                     PD.dataPos, PD.color );
  
  return

  
function selMat = createSelMat( PD )
  % create the selection matrix
  selMat = insertMat( PD.axesPos([4, 3]), PD.markerSize, PD.markerType, PD.dataPos );
  
  return;

  
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                             Callback Functions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function buttonDownCallback(obj, eventdata)
  fields   = get(gcbf, 'userdata');
  %fields   = get(gcf, 'userdata');
  global GPD;

  % get pointer position  
  position = get(obj, 'CurrentPoint');
  position(1) = position(1) - GPD.(fields.axesPos)(1);
  position(2) = position(2) - GPD.(fields.axesPos)(2);

  % if there's anything at the selected point, do something
  if (position(1) > 0) && (position(2) > 0) ...
    && (position(1) <= GPD.(fields.axesPos)(3)) ...
    && (position(2) <= GPD.(fields.axesPos)(4))
    sel = GPD.(fields.selMat)(position(2), position(1));
    if sel ~= 0
      clickAction(sel);
    end
  end
  
  return;


function buttonMotionCallback(obj, eventdata)
  fields   = get(gcbf, 'userdata');
  %fields   = get(gcf, 'userdata');
  global GPD;

  % get pointer position
  position = get(obj, 'CurrentPoint');
  position(1) = position(1) - GPD.(fields.axesPos)(1);
  position(2) = position(2) - GPD.(fields.axesPos)(2);
  
  % check if there's anything to select under the pointer
  if (position(1) > 0) && (position(2) > 0) ...
    && (position(1) <= GPD.(fields.axesPos)(3)) ...
    && (position(2) <= GPD.(fields.axesPos)(4))
    sel = GPD.(fields.selMat)(position(2), position(1));
    if sel ~= 0
      set(GPD.(fields.lineHandle), 'XData', GPD.(fields.data)(sel,1), ...
                                   'YData', GPD.(fields.data)(sel,2), ...
                                   'Visible', 'On');
      set(GPD.(fields.selTextHandle), 'String', printSelection(sel), ...
                                      'Visible', 'On');
    else
      set(GPD.(fields.lineHandle), 'Visible', 'Off');
      set(GPD.(fields.selTextHandle), 'Visible', 'Off');
    end
  else
    set(GPD.(fields.lineHandle), 'Visible', 'Off');
    set(GPD.(fields.selTextHandle), 'Visible', 'Off');
  end
  
  %set(gcbf, 'Pointer', 'crosshair');
  return;


function keyPressCallback(obj, eventdata)
  if eventdata.Character == 'q'
    close;
  end
  return;
