function neatFigure

% remove edges one a figure containing one axes
% and remove the menu. Ready to print

hAxes = gca;
h = findobj(get(hAxes, 'Children'), 'Type', 'image');

if ~isempty(h)
    
    sz = size(get(h(1), 'CData'));

    s = 2;
    w = s*sz(2);
    h = s*sz(1);
    set(gcf, 'Units', 'Pixels', 'Position', [200 200 w-80 h-160]);
    set(gcf, 'Menu', 'none', 'PaperPositionMode', 'auto');
    set(hAxes, 'Units', 'Pixels', 'Position', [-20 -80 w h]);

end


end


