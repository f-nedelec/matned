function im = quadratic_value(rec, quad, min_value)

% im = quadratic_value( rec, quad, min_value )
%
% Calculates im(X,Y) = q(1)*X^2 + q(2)*X*Y + q(3)*Y^2 + q(4)*X + q(5)*Y + q(6)
% over the rectangular space specified as:
%         rec = [x_inf, y_inf, x_sup, y_sup]
% or      rec = [ height, width ]
%
% If the optional argument min_value is provided (for example 0),
% values below min_value are set to min_value
%
% F. Nedelec 

if numel(rec) == 4
    vx  = (rec(1):rec(3))';
    hy  =  rec(2):rec(4);
elseif numel(rec) == 2
    vx  = (1:rec(1))';
    hy  =  1:rec(2);
else
    error('First argument should specify a rectangle');
end

if numel(quad) ~= 6
    error('Second argument should specify the 6 terms of a quadratic');
end

v1  = ones( size(vx) );
h1  = ones( size(hy) );

im = vx.^2 * (quad(1)*h1) + (quad(2)*vx) * hy + (quad(3)*v1) * hy.^2 + ...
     (quad(4)*vx) * h1 + (quad(5)*v1) * hy + (quad(6)*v1) * h1;

if nargin > 2
   im = ( im > min_value ) .* ( im - min_value ) + min_value;
end

end

