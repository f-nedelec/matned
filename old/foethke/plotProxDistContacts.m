function plotProximalDistalContacts()

if 1 %~ exist('mtcontacts.cut', 'file')
    system('cut -c2- mtcontacts.sta | tail -n +2 > mtcontacts.cut');
end

conA   = load('mtcontacts.cut');

start  = 0;
sel    = find( conA(:,2)==1 & conA(:,3)>start); % & conA(:,4)<600 );
con    = conA(sel,:);

dis    = find( con(:,5)<0 );
pro    = find( con(:,5)>0 );

disT   = mean( con(dis, 4) - con(dis, 3) );
proT   = mean( con(pro, 4) - con(pro, 3) );

disV   = std( con(dis, 4) - con(dis, 3) );
proV   = std( con(pro, 4) - con(pro, 3) );

figure;
plot( con(dis,5), con(dis,7), 'bx');
hold on;
plot( con(pro,5), con(pro,7), 'rx');
axis([-7 7 -3 3]);
hold off;
fprintf( '%4i  bundle-catastrophes in poles:\n', size(sel,1));
fprintf( '%4i  proximal: %.2f +/- %.2f sec\n', size(pro,1), proT, proV);
fprintf( '%4i  distal:   %.2f +/- %.2f sec\n', size(dis,1), disT, disV);

end