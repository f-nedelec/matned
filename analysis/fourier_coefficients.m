function [cff, len] = fourier_coefficients( data, nFou, make_plot )

% function cff = fourier_coefficients( angle, weight, nFou )
%
% Return the coefficients of the Fourier series calculated from the data 
% described in the two arrays [ angle, radius ]
% cff(n+1, 1)  corresponds to cos(n*angle)
% cff(n+1, 2)  corresponds to sin(n*angle)
%
% F. Nedelec, Feb. 2008, updated 17.02.2016

if ( nargin < 2 )
    error('fourier_coefficients takes at least 2 arguments');
end

if ( nargin < 3 )
    make_plot = 0;
end

if size(data, 2) < 2
    error('the first argument data should be a Nx2 or Nx3 matrix');
end

%% calculate angles:

angle  = atan2(data(:,2), data(:,1));
radius = sqrt( data(:,1).^2 + data(:,2).^2 );

[ angle, permut ] = sort(angle);


if size(data,2) == 2 

    radius = radius(permut);
    sca = 1 / pi;

else
    
    radius = radius(permut) .* data(permut, 3);
    sca = 1 / ( pi * mean(data(:, 3)) );

end

d_angle = diff(angle);
d_angle(end+1) = angle(1) + 2*pi - angle(end);

%% calculate coefficients of the fourier series:

cff = zeros(nFou, 2);

cff(1,1) = sca * sum( radius .* d_angle );
cff(1,2) = 0;

for n = 1:nFou-1
    cff(n+1,1) = sca * sum( radius .* cos( n * angle ) .* d_angle );
    cff(n+1,2) = sca * sum( radius .* sin( n * angle ) .* d_angle );
end
    

if nargout > 1
    [ ~, ~, ~, len ] = fourier_curve( cff, nFou );
    %fprintf(1, 'curve_perimeter = %f', len);
end

%% plot

if make_plot
    
    n = 100;
    dan = pi/n;
    ang = (1-n:n) * dan;
    fit = cff(1,1) / 2;
    for n = 1:size(cff,1)-1
        fit = fit + cff(n+1,1) * cos(n*ang) + cff(n+1,2) * sin(n*ang);
    end
    
    figure('Name', 'Fourier Series', 'Position', [100 100 900 400]);
    subplot(1,2,1);
    
    fit_max = max(fit);
    
    plot(data(:,1), data(:,2), 'go');
    hold on;
    plot(fit.*cos(ang), fit.*sin(ang), '-');
    xlabel('X', 'FontSize', 16);
    ylabel('Y', 'FontSize', 16);
    axis equal;
    xlim([-fit_max, fit_max]);
    ylim([-fit_max, fit_max]);

    subplot(1,2,2);
    plot(angle, radius, 'go');
    hold on;
    plot(ang, fit, '-');

    xlabel('angle', 'FontSize', 16);
    ylabel('radius', 'FontSize', 16);
    xlim([-pi, pi]);
    ylim([0, min(radius)+max(radius)]);

end

end