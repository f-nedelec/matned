function ismax = filterlocmax( im, n )

% ismax = filterlocmax( im, n )
%
% keep local maximums of the picture
% returns the number of neighboor that it is strictly greater to

if ( isfield(im,'data') ) im = im.data; end
if ( nargin < 2 ) n = 1; end

mask = maskcircle1( 2*n + 1 );
mask( n+1, n+1 ) = 0;
[ mdx, mdy ] = find( mask );

mdx = mdx - n - 1;
mdy = mdy - n - 1;
ismax = zeros( size(im) );

for indx = 1:length(mdx)
   
   dx = mdx(indx);
   dy = mdy(indx);
   
   lx = max(1, 1 - dx );
   ux = min(size(im,1), size(im,1) - dx);
   ly = max(1, 1 - dy);
   uy = min(size(im,2), size(im,2) - dy);
      
   ismax(lx:ux, ly:uy) = ismax(lx:ux, ly:uy) +...
      ( im(lx:ux, ly:uy) > im( lx+dx:ux+dx, ly+dy:uy+dy ) );
   
end

return;