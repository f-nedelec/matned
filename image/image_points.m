function pts = image_points(im, mask)
% return pts = ( x, y, w ) for the pixels indicated in mask
%
% F. Nedelec, April 2014

if any( size(im) ~= size(mask) )
    error('dimension missmatch');
end

ii = logical(mask);

vx = (1:size(im,1))';
hy = 1:size(im,2);

ix = vx * ones(size(hy));
iy = ones(size(vx)) * hy;

pts = zeros(nnz(ii), 3);
pts(:, 1) = ix(ii);
pts(:, 2) = iy(ii);
pts(:, 3) = im(ii);

end