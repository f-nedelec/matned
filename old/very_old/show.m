function show(im, im2, textcode)

if ( nargin < 2 ) 
   textcode='scale 8bit'; 
   im2=''; 
else
   if ( ischar( im2 ) ) textcode = im2; end;
end

if ( nargin < 3 ) textcode='scale 8bit'; end



if ( isnumeric( im2 ) ) 
   show2(im, im2, textcode);
else
   show1(im, textcode);
end



return;