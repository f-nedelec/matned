function params = getdata( sel );

cnt = 0;
dir = 0;

for u=1:size(sel, 1)
          
    if ( size(sel, 2 ) == 2 )
        folder = sel{ u, 1 };
        if 0 == findfolder( folder )
            return;
        end
        nbr = sel{ u, 2 };
    else
        nbr = sel( u, 1 );
    end
    
    p = load('tel');
    pline = find( p(:,1) == nbr );
      
    if ( size( pline ) ~= [ 1, 1 ] )
        disp(['cannot find ' name ' in <tel>' ]);
    else
        
        cnt = cnt + 1;
        params( cnt, 1:size(p,2) )   = p( pline, : );
        params( cnt, size(p,2) + 1 ) = u;
        
    end

end
         

return;