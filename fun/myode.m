
% ode file for a simple chaotic system
function dy = myode(t, y, b)

if nargin > 1 

    dy = - b * y + sin( y( [2,3,1] ) );
    
end

return;
    