function sel = selectdynamic;

selcnt = 0;

%load the files:
screen = load('screen');
params = load('tel');

%open output file:
fid   = fopen( 'sel', 'w' );
fprintf( fid, '%%%s\n', date );

for u = 1:size(screen,1)

   nbr        = screen( u, 1 );
   name       = sprintf('data%04i', nbr);
   
   distmin    = screen( u, 2 );
   distmean   = screen( u, 3 );
   distmax    = screen( u, 4 );
   minlinks   = screen( u, 5 );
   meanlinks  = screen( u, 6 );
   intralinks = screen( u, 7 );
   paralinks  = screen( u, 8 );
   antilinks  = screen( u, 9 );
   
   
   overlap = 7 * 2 - distmean;
 
   test    = zeros( 1, 5 );
   test(1) = ( distmin > 2 );
   test(2) = ( antilinks + paralinks > 10 );
   test(3) = ( antilinks > paralinks );
   test(4) = ( minlinks > 5 );
 
   alltest = test(1) & test(2) & test(3) & test(4);
   
   %save the selection:
   s = sprintf( '%04i %i %i %i %i %i',...
      nbr, alltest, test(1), test(2), test(3), test(4));

   disp(s);
   fprintf( fid, '%s\n', s );
   
   if ( alltest )
      
      %==================find the corresponding parameters:
      selcnt = selcnt + 1;
     
      pline = find( params(:,1) == nbr );
      if ( size( pline ) == [ 1, 1 ] )
         sel( selcnt, 1:size(params, 2) ) = params( pline, : );
      else
         disp(['cannot find ' name ' in <tel>' ]);
      end
      
      sel( selcnt, size(params,2)+1 ) = overlap;        %the overlap
      
   end
   
end

fclose(fid);
         
return;