function h = setupFigure(ifig,figsize)
%%
% h = setupFigure(ifig,figsize) creates a figure and set some standard properties
% figsize: vector [ posx posy width height]
% ifig: number of vector
% h: handle of figure
%

h = figure(ifig);
clf
hold on
set(h,'OuterPosition',figsize);
%set(h,'ActivePositionProperty','Position')
set(h, 'units', 'pixel', 'pos', figsize)
set(h, 'Color', [1 1 1]); % Sets figure background
set(gca, 'Color', [1 1 1]); % Sets axes background
set(h, 'Renderer', 'painters');
set(h, 'PaperPositionMode', 'auto');
set(gca,'Box','off','FontSize',12);

end
