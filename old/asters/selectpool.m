function allsel = selectpool( crit, fold )

allsel = [];

folders = [];
root = 'std';

switch fold
    
case '1dhetero'
    numbers = [ 1, 2, 4, 5, 14:19, 24:29 ];
    root='CENTL';
case '1dhomo'
    numbers = [ 8, 9, 11:13, 20:23 ];
    root='CENTL';
case '1dmixed';
    numbers = [ 40 ];
    root = 'CENTL';
    
case 'screen1'
    numbers = [ 60:64 ];
case 'screen2'
    numbers = [ 43:45, 50:52, 71:73 ];
case 'screen3'
    numbers = [ 65, 66, 79, 80 ];
case 'screen4'
    numbers = [ 40:42, 74:77 ];
case 'screen5'
    numbers = [ 30:36 ];
case 'screen6'
    numbers = [ 87:89 ];
end


for u = 1:size( numbers, 2 )
    
    folder = sprintf('%s%02i', root, numbers(u) );
    
    if findfolder( folder ) == 0
        continue;
    end
    
    sel = selectfig( crit );
    
    disp( [ folder, ' : ', num2str( size(sel, 1 ) ) ] )
   
    if ( size( sel, 1 ) > 0 )
        
        asel = cell( size(sel) + [ 0 1 ] );
        for s = 1:size( asel, 1 )
            asel(s, 1 ) = { folder };
            asel(s, 2 ) = { sel( s, 1 ) };
        end
        
        if ( 0 == exist( 'allsel', 'var' ) )
            allsel = asel;
        else
            allsel = cat( 1, allsel, asel );
        end
        
    end
    
    cd ..;
    
end
