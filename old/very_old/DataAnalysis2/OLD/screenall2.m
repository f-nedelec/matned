function seld = screenall2( numbers )

if nargin < 1 
   numbers = [ 101:106 ];
end

seld = zeros( 0, 17 );

for i = 1:length( numbers )
   
   nb = numbers( i );
   
   %load and screen the file
   sname = sprintf('c%03i', nb );
   disp( [ 'loading   ' sname ] );
   as = load(sname);
   sname = sprintf('l%03i', nb );
   lsd = load(sname);
   s = screen3( as, [ 2 3 1 ], lsd, nb );
      
   sname = sprintf('t%02i', nb );
   disp( [ 'loading   ' sname ] );
   ap = load(sname);
   nbpam = size( ap, 2 );
   
   %find the corresponding parameters
   p = zeros( size( s, 1 ), 16 );
   for j = 1 : size( s, 1 )
      
      %screenShowLast( nb, s(j,1) );
      
      pline = find( ap(:,1) == s( j, 1 ) );
      if ( size( pline ) == [ 1, 1 ] )
         p( j, 1:nbpam ) = ap( pline, : );
         p( j, 16 ) = nb;
         p( j, 17 ) = s( j, 2 );
      else
         disp(['cannot find ' num2str(s(j,1)) ' in ' num2str(nb)]);
      end
   end
   
   %concatenate to make the big list:
   seld = cat( 1, seld, p );
   
end
