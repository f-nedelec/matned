function testmask( im )

pts = mousepolyroi( im );
[mask,roi] = polytomask( pts );
im2 = crop( im, roi );
im2 = im2 .* mask;

show(im2);

