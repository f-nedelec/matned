function focus(param, data, indx)

Colors='rgbkm';
Shapes='o+*xsd^v<>';

figure('Name',['focus ' num2str(indx)]);

uniq = unique( param(:,indx) );

for u=1:length(uniq)
   
   sel = find( param(:,indx) == uniq(u) );
   
   ci = 1 + mod(u-1, length(Colors));
   if ( length(uniq) < length(Colors) )
      cs = 0;
   else
      cs = 1 + mod(u-1, length(Shapes));
   end
   
   if ( cs )
      linespec = [Colors(ci) Shapes(cs)];   
   else
      linespec = [Colors(ci) '-' ];
   end
   
   val = sprintf('%8.2f  ', uniq(u));
   disp([val ' : ' num2str(linespec)]);
   
   anplot( param(sel,:), data(sel,:) , linespec );
   hold on;
   
end

return