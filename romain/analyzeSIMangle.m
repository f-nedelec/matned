function [nrNuclei] = analyzeSIMangle(varargin)
% Analyze Ashbya simulation's by Romain Gibeaux
%
% Modified 7 Nov 2014, F.Nedelec, 6th July 2015

params = struct('showplot',0,'savefig',0,'output','');
params = inputArgsToStruct(params, varargin);

filename = 'angle3.txt';

if ~ exist(filename, 'file')
    system(['~/bin/reportR ashbya:angle frame=450 output=', filename]);
end

%% Load nuclei data + flow data
data = load(filename);

% Get number of nuclei
nrNuclei = max(data(:,2));

% Order and Simplify
for i = 1:nrNuclei
    angles{i} = data(find(data(:,2)==i),9);
end

%% Export parameters
if params.output
    if ~exist(params.output, 'file')
        fid = fopen(params.output,'w');
        fprintf(fid, '%%%s\n','angles');
        fclose(fid);
    end
    fid = fopen(params.output,'a');
    for i = 1:nrNuclei
        fprintf(fid, '%.4f %% %s\n',angles{i}, pwd);
    end
    fclose(fid);
end


end
