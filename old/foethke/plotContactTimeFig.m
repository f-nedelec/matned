function plotContactTimeFig

% plot histogram of contact-times from S.pombe simulations and experiments
% Figure 3A of Foethke et al. 2008
% F. nedelec, 2006 - 2008


[ bins, h_sim ] = contactTimeHistogram(0);

% bars for simulation data:
figure;
bar( bins, h_sim, 'g' );

y_max = 1.2*max(h_sim);


if exist('contactsExp.txt', 'file')
    
    [ bins, h_exp ] = contactTimeHistogram(1);
    
    % Redo the figure: bars for experimental data:
    clf;
    hb = bar(bins, h_exp, 'b');
    set(hb, 'FaceColor', [0.5 0.5 0.5], 'LineWidth', 1);

    %lines for simulation data:
    hold on;
    plot(bins, h_sim, 'k-', 'LineWidth', 3);
    
    y_max = 1.2 * max([h_exp; h_sim]);
    %text(100, 0.01,  'mean measured  = %.1f\n', mean(evt_exp));
    %text(100, 0.008, 'mean simulated = %.1f\n', mean(evt_sim));
    
end

axis([0 max(bins) 0 y_max ]);
xlabel('Contact time (s)', 'FontSize', 20);
ylabel('Normalized density / 10^{-3}', 'FontSize', 20);
set(gca, 'FontSize', 20);
set(gca, 'TickDir', 'out');

end
