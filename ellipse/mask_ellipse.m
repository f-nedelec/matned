function mask = mask_ellipse( ell, solid )

% mask = mask_ellipse(ell);
%
% Draw a solid ellipse of ones centered in a square of zero-valued pixels.
% ell(1) is the angle in radian between the i-axis and the first axis
%  Note: The i-axis corresponds to the first matrix index and is
%        vertical for displayed images
% ell(2) = half-length of the first axis
% ell(3) = half-length of the second axis.
% The size of the mask produced depends on the ellipse specified
%
% Example: y = mask_ellipse( [pi/4, 10, 5] );
%
% F. Nedelec, may 2000, revised April 2008; Email: nedelec@embl.de

if nargin < 2
    solid = 1;
end

if length(ell) ~= 3
    error('first argument should have a length of 3');
end

mask = [];

an = ell(1);
d0 = ell(2);
d1 = ell(3);

if d0 <= 0 || d1 <= 0
   return;
end

cxx = ( cos(an)/d0 ).^2 + ( sin(an)/d1 ).^2;
cyy = ( sin(an)/d0 ).^2 + ( cos(an)/d1 ).^2;
cxy = cos(an)*sin(an)*( d0^(-2) - d1^(-2) );

xmax = ceil( sqrt( cyy / ( cxx * cyy - cxy^2 ) ) );
xmin = - xmax;

ymax = ceil( sqrt( cxx / ( cxx * cyy - cxy^2 ) ) );
ymin = - ymax;

%[xmin ymin xmax ymax ]


if xmax == 0 || ymax == 0

    mask = 1;

else
    
    mask = zeros( 2*xmax+1, 2*ymax+1 );
    
    if solid
        for y = ymin:ymax
            del = ( y*cxy )^2 - cxx * ( y^2 * cyy - 1 );
            if ( del >= 0 )
                del = sqrt( del );
                x1  =  ceil( ( - y*cxy - del ) / cxx );
                x2  = floor( ( - y*cxy + del ) / cxx );
                mask( 1+x1-xmin:1+x2-xmin, 1+y-ymin ) = ones( x2-x1+1, 1);
             end
        end
    else
        for x = xmin:xmax
            del = ( x*cxy )^2 - cyy * ( x^2 * cxx - 1 );
            if ( del >= 0 )
                del = sqrt( del );
                y1  =  ceil( ( - x*cxy - del ) / cyy );
                y2  = floor( ( - x*cxy + del ) / cyy );
                mask( 1+x-xmin, 1+y1-ymin ) = 1;
                mask( 1+x-xmin, 1+y2-ymin ) = 1;
            end
        end
        for y = ymin:ymax
            del = ( y*cxy )^2 - cxx * ( y^2 * cyy - 1 );
            if ( del >= 0 )
                del = sqrt( del );
                x1  =  ceil( ( - y*cxy - del ) / cxx );
                x2  = floor( ( - y*cxy + del ) / cxx );
                mask( 1+x1-xmin, 1+y-ymin ) = 1;
                mask( 1+x2-xmin, 1+y-ymin ) = 1;
            end
        end
    end
    
end