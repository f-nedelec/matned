function pts = setclockwise( points );

%reorder the points in a clockwise maner

cen = mean( points, 1 );
ang = atan2( points( :, 2) - cen(2) , points(:,1) - cen(1) );
[a, ind] = sort( ang );
pts = points( ind, : );

return