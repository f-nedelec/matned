function im2 = bwmeanfilter( im )
% supress extremum values of the image
% in a 0/1 picture

if ( isfield(im,'data') ) im = im.data; end

dv =[ -1 -1; 0 -1; 1 -1; -1 0; 1 0; -1 1; 0 1; 1 1];
%dv =[ 0 -1; -1 0; 1 0; 0 1 ];

ismax = ones( size(im) );
ismin = ones( size(im) );

for indx = 1:length(dv)
   
   dx = dv(indx,1);
   dy = dv(indx,2);
   
   lx = max(1, 1 - dx );
   ux = min(size(im,1), size(im,1) - dx);
   ly = max(1, 1 - dy);
   uy = min(size(im,2), size(im,2) - dy);
   
   ismax(lx:ux, ly:uy) = ismax(lx:ux, ly:uy) .*...
      ( im(lx:ux, ly:uy) > im( lx+dx:ux+dx, ly+dy:uy+dy ) );
   
   ismin(lx:ux, ly:uy) = ismin(lx:ux, ly:uy) .*...
      ( im(lx:ux, ly:uy) < im( lx+dx:ux+dx, ly+dy:uy+dy ) );
   
end

im2 = im - ismax + ismin;

return;