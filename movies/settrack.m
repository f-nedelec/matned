%settrack
%global variables for asters tracking

global debug minDistance minPixels maxPixels searchR;

%hiding radius, when searching the other aster
minPixels = 20;
maxPixels = 10000;

%searching radius, should be comparable to the size of the
%features beeing tracked

searchR = 5;

minDistance = sqrt( minPixels );


%region of interest:

