function true_speed = motor_step(nb_of_motors)
%usage motor_step( nb_of_motors )


%unit of space: 8 nm = repeat of tubulin on a Microtubule
%unit of time : the second
% a walk = a 8 nm step on the Microtubule lattice
         
%average numbers of 8nm walks per second
speed      = 90; 

%additional delay per step due to putting back the motor in a favorable 
% configuration. This makes the motion less stochastic.
step_delay = 0.001;

%time step of simulation: if small enough, that should have no influence
%on the final curve
dt         = 0.001;

%probability of taking a 8nm-walk in the time interval dt
step_prob  = speed * dt;

%probability of unbinding each time a walk is taken:
%other basal detachment (when the motor is not moving) is neglected
off_prob   = 0.01;

%optical resolution: motion is detected only if it is above 10x8nm steps:
%real resolution is probably higher. This get rids of short runs, for which
%the errors in measurement are the largest.
resolution = 10;   %(has units of 8nm)



%initialize the variables:
attached = 1;
position = 0;
time     = 0;

%monte-carlo simulation:
while attached

    time = time + 1;   %unit of dt
    if ( rand < step_prob )
        position = position + 1;
        if ( rand < off_prob )
            attached = 0;
        end
    end

end


%get the real time:
time = time * dt;

%add time delay due to stepping ( 1 milli-seconds / step ):
%this makes the stepping a little less stochastic
time = time + step_delay * position;


if ( position < resolution ) 
    %motion was not detected:
    true_speed = 0;
else
    %the speed that will be measured experimentally:
    true_speed = position/time;
end

%multi-motor display:
if ( nargin > 0 )
    if ( nb_of_motors == 1 )
        fprintf( 'position %5i time %7.2f speed %9.2f\n', position, time, true_speed);
    else
        for ii=1:nb_of_motors; s(ii)=motor_step; end
        h = histc(s,[0:5:120]);
        %we should convolve by some optical resolution: convolve with a
        %gaussian to represent the imaging process
        bar(h);
    end
end
