% create datafile 'complete.dat' from the simulation raw data
% usage: createDataFile('/path/to/dataxxx-files')
% CVS: $Id: createDataFile.m,v 1.2 2004/09/20 15:18:29 foethke Exp $

function createDataFile( basePath )

  % set paths
  if ~ exist('basePath', 'var')
    basePath  = '/disks/nedelec1/foethke/SCREEN_5';
  end
  if ~ exist(basePath, 'dir')
    error(sprintf('directory %s does not exist!', basePath));
  end
  savePath    = fullfile(basePath, 'SAVE');
  analysePath = fullfile(basePath, 'ANALYSE');
  
  
  % create a list of data directories
  % check if the name starts with 'data' and if it is a directory
  counter = 1;
  dirList  = dir(savePath);
  for n=1:length(dirList)
    if (~ isempty(regexp(dirList(n).name, '^data', 'start'))) && dirList(n).isdir
      dataDirs(counter).name = dirList(n).name;
      counter = counter+1;
    end
  end
  % put the directory names in a cell array and sort it
  dirsCell   = {dataDirs.name};
  sortInd    = sortcellchar(dirsCell);
  sortedDirs = dirsCell(sortInd);
  numDirs    = length(sortedDirs);
  

  % create empty arrays to hold the data
  gr     = zeros(numDirs,1);
  shr    = zeros(numDirs,1);
  cat    = zeros(numDirs,1);
  res    = zeros(numDirs,1);
  means  = zeros(numDirs,1);
  spread = zeros(numDirs,1);

  
  % read data from disk
  for n=1:numDirs
    workingDir = fullfile(savePath, sortedDirs{n});
    nucposFile = fullfile(workingDir, 'nucpos.dat');

    % check if nucpos.dat exists in workingDir
    % if not, try to create it from result.out by running analyse_nucpos
    if ~ exist(nucposFile, 'file')
      resFile = fullfile(workingDir, 'result.out');
      if ~ exist(resFile, 'file')
        zipFile = fullfile(workingDir, 'result.out.gz');  
        if exist(zipFile, 'file')
          disp(sprintf('unzipping %s', zipFile));
          [status, result] = system(sprintf('gunzip %s', zipFile));
          if ( status ~= 0 )
            warning('gunzip went wrong once, trying again...');
            [status2, result2] = system(sprintf('gunzip %s', zipFile));
            if ( status2 ~= 0 )
              error(sprintf('error in gunzip: %s', result2));
            end
          end
        else
          error(sprintf('no file result.out or result.out.gz in directory %s', workingDir));
        end
      end
      currDir = pwd;
      cd(workingDir);
      disp(sprintf('analysing %s', sortedDirs{n}));
      [status, result] = system(sprintf('analyse_nucpos nucleus > %s', nucposFile));
      if ( status ~= 0 )
        error(sprintf('error in analyse_nucpos: %s', result));
      end
      disp(sprintf('zipping %s', resFile));
      system(sprintf('gzip %s', resFile));
      cd(currDir);
    end

    % read nucpos.dat and data.out
    disp(sprintf('processing %s', sortedDirs{n}));
    [dummy, xPos] = textread(nucposFile);
    P = getParameters(fullfile(workingDir, 'data.out'));
    
    gr(n)     = P.mtdynspeed(3);
    shr(n)    = P.mtdynspeed(4);
    cat(n)    = P.mtdyntrans(3);
    res(n)    = P.mtdyntrans(4);
    force(n)  = P.mtdynforce;
    means(n)  = mean(xPos);
    spread(n) = std(xPos,1);    
  end
  

  % write output file
  fid = fopen(fullfile(analysePath, 'complete.dat'), 'w');
  for n=1:numDirs
    fprintf(fid, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t', ...
            [gr(n); shr(n); cat(n); res(n); force(n); means(n); spread(n);]);
    fprintf(fid, '%s\n', sortedDirs{n});
  end
  fclose(fid);
  return;
