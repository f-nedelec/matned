function a = quadratic_fit2(x,y)

% a = quadratic_fit2(x,y)
%
% x,y are lists of coordinates
% provides the coefficients (a,b,c,d,e,f)
% the fitting ellipse is the zeros of :
% F(a,x) = a*x^2 + b*y^2 + c*x*y + d*x + e*y + f;


% Build design matrix 
D = [ x.*x x.*y y.*y x y ones(size(x)) ];

% Build scatter matrix
S = D'*D;

% Build 6x6 constraint matrix 
C =  zeros(6);
C(1,3) = -2;
C(2,2) =  1;
C(3,1) = -2;

% Solve eigensystem 
[gevec, geval] = eig(inv(S)*C);

% Find the negative eigenvalue
[NegR, NegC] = find(geval < 0 & ~isinf(geval));

% Extract eigenvector corresponding to positive eigenvalue
a = gevec(:,NegC);