function [ his, polpol ] = spin_plot_length

% function spin_plot_length
%
% F. Nedelec, Feb. 2009
%
%


system('cat *.pts > all_clicks.txt');

try
    pts = load('all_clicks.txt');
catch
    disp('File "all_clicks.txt" not found');
    return;
end

%% Collect data

polpol = [];
hBar = waitbar(0,'Reading images...','Name','Reading data...');
cnt = 0;
while cnt < 100
    cnt = cnt + 1;
    waitbar(cnt/100, hBar);
    try
        im = spin_load_images('tub', cnt);
    catch
        im = spin_load_image_old('tub', cnt);
        break;
    end
    [pathstr, name, ext] = fileparts(im.file_name);
    try
        pts = load([name , '.pts']);
        dis = im.pixel_size * sqrt((pts(:,2)-pts(:,4)).^2 + (pts(:,3)-pts(:,5)).^2);
        polpol = cat(1, polpol, dis);
    catch
        fprintf('No click file found for "%s"\n', im.file_name);
    end
end
close(hBar);

%% Plot data

cnt = length(polpol);
avg = mean(polpol);
dev = std(polpol);
msg = sprintf('Length = %.2f +/- %.2f (N=%i)', avg, dev, cnt);
fprintf('%s\n', msg);


figure('Toolbar', 'none');
plot(polpol, 'kx', 'MarkerSize', 14, 'LineWidth', 2);
xlabel('Bipolar Structures', 'FontSize', 16, 'FontWeight', 'bold');
ylabel('Length / um', 'FontSize', 16, 'FontWeight', 'bold');
title(msg, 'FontSize', 20);
ylim([0 45]);


X = 20:1:46;
figure('Toolbar', 'none');
his = histc(polpol, X);
hist(polpol, X);
xlabel('Length / um', 'FontSize', 16, 'FontWeight', 'bold');
ylabel('Count', 'FontSize', 16, 'FontWeight', 'bold');
title(msg, 'FontSize', 20);
xlim([min(X), max(X)]);

%%
disp('Histogram data:');
%print on terminal for copy-paste
fprintf('[');
for i = 1:length(his)
    fprintf(' %i', his(i));
end
fprintf(' ]\n');

end
