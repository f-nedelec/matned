function data = analysekf2;
%this is for vale25

ankf = load('an');
params = load('params');

cnt = 0;

for ii = 1:size(ankf,1)-3
    
    sim    = ankf(ii,1);
    ncd    = params(ii, 2);
    ncd2   = params(ii+1, 2); 
    ncd3   = params(ii+3, 2); 
    cov    = ( sim+1 == ankf(ii+1, 1) ) & ( sim+3 == ankf(ii+3, 1) );
    
    if ( params(ii, 1) ~= sim )
        fprintf(1, 'warning: mismatch %i and %i\n', sim, ii);
    end
        
    if ( cov & ( ncd > 0 ) & ( ncd2 == 0 ))
        
        %check the parameters for both cases

        cnt = cnt + 1;
        data(cnt, 1) = sim;
        data(cnt, 2) = ankf(ii,   2);   %focussing for dynein+ncd
        data(cnt, 3) = ankf(ii+1, 2);   %focussing for dynein alone
        data(cnt, 4) = params(ii, 6);   %end-off rate for dynein 
        data(cnt, 5) = ankf(ii+3, 2);   %focussing lower dynein alone
    end
    
end

%fitcoef = polyfit( data(:,2), data(:,3), 1 );
%fitx = [ 0 9 ];
%fity = polyval( fitcoef, fitx );


figure('Position', [10 10 600 600]);
cnt=3;
for ii = [ 2, 4, 8, 16, 32, 64, 128 ]
    
    f=find( data(:,4) == ii );
    plot( data(f,3), data(f,2), 's', 'MarkerSize', cnt );
    hold on;
    cnt = cnt + 1;

end

%plot( fitx, fity, '-' );
plot([0; 9], [0; 9], 'k:');
title( 'Plot 1: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'dynein alone', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'dynein +ncd', 'FontSize', 14, 'FontWeight', 'bold' );




figure('Position', [610 10 600 600]);
cnt=3;
for ii = [ 2, 4, 8, 16, 32, 64, 128 ]
    
    f=find( data(:,4) == ii );
    plot( data(f,3), data(f,2)-data(f,3), 's', 'MarkerSize', cnt);
    hold on;
    cnt = cnt + 1;

end
hold on;
plot([0; 9], [0; 0], 'k:');
title( 'Plot 2: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'quality of focussing with dynein only (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'improvement brought by ncd (negative=improvement)', 'FontSize', 14, 'FontWeight', 'bold' );




figure('Position', [610 410 600 600]);
plot( data(:,5), data(:,3), 's', 'MarkerSize', 4);
hold on;
plot([0; 9], [0; 9], 'k:');
title( 'Plot 2: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'focussing low dynein (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'focussing more dynein', 'FontSize', 14, 'FontWeight', 'bold' );
