function [im1, im2, im3] = makeimages( param , data )

% we first average the motor number:

[ ps, ds, nb ] = average( param, data, 4, [4 6] );


% we select according to speed:

[ p1, d1 ] = select( ps, ds, 1, 1   );
[ p2, d2 ] = select( ps, ds, 1, 0.2 );
[ p3, d3 ] = select( ps, ds, 1, 0.1 );

%two parameters are left...

% we make the pictures:

im1 = dataarray( p1, d1);
im2 = dataarray( p2, d2);
im3 = dataarray( p3, d3);
