function subim = crop( im, brect, mode, paddingvalue )

% subim = crop( im, brect , mode, paddingvalue )
% crop image <im> in rectange <brect> to form <subim>
% <brect>=[lower_x, lower_y, upper_x, upper_y] can be any size, with negative value.
% mode defines the clipping:
%    1: padd <subim> to force a size (upper_x - lower_x + 1, upper_y - lower_y + 1)
%    0: clipping occurs, no padding (default)

if ( nargin < 3 ) mode = 0; end
if any( rem(brect,1) > 0 )
   brect = round( brect );
end

lx = brect(1);
ly = brect(2);
ux = brect(3);
uy = brect(4);

if ( mode == 1 )
   
   if ( exist( 'paddingvalue', 'var') )
      subim = paddingvalue * ones( ux - lx + 1 , uy - ly + 1 );
   else
      subim = zeros( ux - lx + 1 , uy - ly + 1 );
   end
   
   if ( ux < 1 ) | ( uy < 1 ) | ( lx > size(im,1) ) | ( ly > size(im,2) )
      return;
   end
   
   brect = max( [ 1 1 1 1 ], brect );
   brect = min( [ size(im) size(im) ], brect );
   
   clx = brect(1);
   cly = brect(2);
   cux = brect(3);
   cuy = brect(4);
   
   if ( cux < clx ) | ( cuy < cly ) return; end
   
   subim( clx-lx+1:cux-lx+1, cly-ly+1:cuy-ly+1 ) = im(clx:cux, cly:cuy);
   
else  %not padding
      
   if ( ux < 1 ) | ( uy < 1 ) | ( lx > size(im,1) ) | ( ly > size(im,2) )
      subim = [ ];
      return;
   end
   
   brect = max( [ 1 1 1 1 ], brect );
   brect = min( [ size(im) size(im) ], brect );
   
   clx = brect(1);
   cly = brect(2);
   cux = brect(3);
   cuy = brect(4);
   
   if ( cux < clx ) | ( cuy < cly ) return; end
   
   subim = im(clx:cux, cly:cuy);
   
end
