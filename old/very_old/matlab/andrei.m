function andrei( im )

filename = '';
if ( ischar( im ) )
    filename = im;
    im       = tiffread( im );
end

if ( isfield(im,'data') ) 
    im = double( im.data ); 
end

%==================find the background level:

base1      = min(min( im ));
base2      = imbase( im );

%==================   select a rectangular ROI:

showim( im );
drawnow;

polygon = 0;
if ( polygon )
    
    poly = ROImousepoly;
    [mask, roi] = maskpolygon( poly );
    %showim( mask );
    subim  = ( crop( im, roi ) - base2 ) .* mask;

else
    
    roi  = ROImouserect;
    subim = crop( im, roi ) - base2;
    
end
close;

%==================   keep only positive pixels:

subim  = subim .* ( subim > 0 );

%==================   find the center of the aster, as the brightest spot:

immin = min( min( subim ));
immax = max( max( subim ));

%==================   make a summary figure:

figid=figure('Name', ['from file ', filename], 'Position', [ 100, 100, 800, 400 ] );
set(gca, 'Position', [0.05 0.05 0.45 0.95]);
showim( subim, 'nofigure', 'axisticks', 'trueratio' );

%==================   fit an ellipse to the sub-image
[cen, mom, rot, sv] = barycenter2( subim, 10 );


%==================   find the mean pixel value inside

ellmask  = maskellipse( [ rot(1), 2*rot(2:3) ] );
ellsize  = size( ellmask );
botleft  = round( cen - ellsize / 2 );
topright = botleft + ellsize - [1 1];

subsubim = ellmask .* crop( subim, [ botleft, topright ] );
%showim( subsubim );
total_intensity = sum( sum( subsubim ) );
total_surface   = sum( sum( ellmask ) );
mean_intensity  = total_intensity / total_surface;
total_intensity = 10^-5 * total_intensity;

figure(figid);
axes( 'Position', [ 0.51 0.13 0.4 0.7 ] );
title( filename, 'FontSize', 14, 'Interpreter', 'none' );

mess1 = sprintf('mean  intensity %5.0f a.u.\n',  mean_intensity );
mess2 = sprintf('total intensity %5.0f 10^5 a.u.\n', total_intensity);
mess3 = sprintf('length %7.0f pixels\n', 4 * rot( 2 ) );
mess4 = sprintf('width  %7.0f pixels\n', 4 * rot( 3 ) );
mess5 = sprintf('surface %6.0f pixels^2\n', total_surface);

text( 0 , 0.5, [mess1, mess2, mess3, mess4], 'FontSize', 14, 'FontName', 'fixedwidth');
axis off;

%=============prepare for printing:

set(gcf,'PaperType','A4');
set(gcf,'PaperOrientation','portrait');
set(gcf,'PaperPositionMode','manual');
set(gcf,'PaperUnits','centimeters');
set(gcf,'PaperPosition',[2 12 17 8.5]);
