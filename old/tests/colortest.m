function img = colortest()

img=zeros(8,1,3);

n=3;

for i=1:2^(3*n)+1
      d = i-1;
      for c=1:3
         img(i,1,c)=mod(d,2^n)/2^n;
         d = fix(d / 2^n); 
      end
end