function saveeps(filename, fig, dpi)

% function saveeps(filename, fig, dpi)
%
% export figure 'fig' to file as a PNG image, preserving its aspect ratio.
% By default, dpi=150 and fig='current figure'
%
% F. Nedelec, April, June 2015

if nargin < 3
    %resolution (dpi) of final graphic
    dpi = 150;
end

if nargin < 2 || isempty(fig)
    fig = gcf;
end

%% check filename

if nargin > 0 && ~ isempty(filename)
    [~,~,ext] = fileparts(filename);
    if isempty(ext)
        filename = [ filename, '.eps' ];
    elseif ~strcmpi(ext, '.eps')
        error('incorrect file name extension');
    end
else
    filename = sprintf('figure%i.eps', get(gcf, 'Number'));
end

%% Get the same size as currently displayed on the screen

pos_pixels = getpixelposition(fig);
scr = get(0, 'ScreenPixelsPerInch');
pos_inches = pos_pixels(3:4)/scr;

fig.PaperUnits = 'inches';
fig.PaperSize = pos_inches;
fig.PaperPosition = [0 0 pos_inches];
% This will preserve the background color:
fig.InvertHardcopy = 'off';


if dpi == 150
    saveas(fig, filename, 'epsc');
else
    print(fig, filename, '-depsc', ['-r',num2str(dpi)])
end

end
