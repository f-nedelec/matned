function scan_register_spindle_centered;

fichier = dir('*.tif');

for i = 1 : size( fichier, 1 )
    
    inname = fichier(i).name;

    %if ( isempty( findstr(inname, 'REG' )))
    if ( findstr(inname, 'REG' ))
        disp( inname );
        %register = register_spindle_click( inname );
        register = register_spindle_centered( inname );
        showim( register );
    
        outname = sprintf('Ctr%s', inname );
        imwrite( register, outname, 'tif', 'Compression', 'none' );
    
    end
end