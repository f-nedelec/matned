function [ cff, phase ] = fourier_image_coefficients( im, nFou, verbose )

% function cff = fourier_image_coefficients( im, nFou )
%
% Return the fourier coefficients corresponding to a centered image
% cff(n+1, 1)  corresponds to cos(n*theta)
% cff(n+1, 2)  corresponds to sin(n*theta)
%
% F. Nedelec, Feb. 2008

s = size(im);
if s(1) ~= s(2)
    error('the image should be square');
end
s = s(1);

ang  = mask_angles(s);
mask = mask_circle(s);
for d = 1:size(im,3)
    im(:,:,d)  = double(im(:,:,d)) .* mask;
end

%normalize such that fourier_image() and fourier_image_coefficients() are inverse

norm = sum(sum(mask)) / 2;
cff = zeros(nFou, 2);

for n = 0:nFou
    for d = 1:size(im,3)
        cff(n+1,1,d) = sum(sum( double(im(:,:,d)) .* cos( n * ang ) )) / norm;
        cff(n+1,2,d) = sum(sum( double(im(:,:,d)) .* sin( n * ang ) )) / norm;
    end
end

%provide the phase
if nargout > 1
    phase = atan2(cfou(2:nFou,2), cfou(2:nmax,1)) ./ (1:nFou);
end


if nargin > 2

    %display image and coefficients:
    show_image(im);
    fPos = get(gcf, 'Position');
    set(gcf, 'Position', [fPos(1) fPos(2) 2*fPos(3) fPos(4)]);
    set(gca, 'Units', 'pixels', 'Position', [1 1 fPos(3) fPos(4)] );
    B = fix( max(fPos(3), fPos(4)) * 0.1 );
    axes('Units', 'pixels', 'Position', [fPos(3)+B 1+B fPos(3)-2*B fPos(4)-2*B] );
    bar(0:nFou, cff, 2);
    xlim([-1 nFou+1]);
    legend('cosinus', 'sinus');
    xlabel('Order');
    
end


end