function data = planar_section(data, plane)

% data = planar_section(mts, plane)
%
% returns coordinates of the intersection point(s) between
% the plane defined by four scalars in 'plane' and the filaments in `data`
%
% The equation of the plane is: ax + by + cz + d = 0
% if plane = [a b c d]
% the normal to the plane is [a b c]
% 
% On input 'data' is a structure containing a field 'pts' describing the 
% backbone of the filaments.
% On output 'data' has a field 'intersect' with the coordinates of the
% intersection between the backbone and the plane.
%
% F. Nedelec, 31.03.2018

if nargin < 2
    error('Arguments missing')
end

data = convert_fibers(data);

nor = plane(1:3);

% Tests every filament one by one
for ii = 1:length(data)
    
    pts = data(ii).pts;

    %calculate distance to plane:
    dis = plane(4) * ones(size(pts,1), 1);
    for d = 1:3
        dis = dis + nor(d) * pts(:,d);
    end
    
    %find point closest to plane:
    [m, pp] = min(abs(dis));
    pos = pts(pp,:);
    res = [];
    
    % check both sides around this point:
    if pp < size(pts, 1)
        dif = pos - pts(pp+1,:);
        a = ( dot(nor,pos) + plane(4) ) / dot(nor, dif);
        if 0 <= a && a <= 1
            res = pos - a * dif;
        end
    end
    
    % check both sides around this point:
    if 1 < pp
        dif = pos - pts(pp-1,:);
        a = ( dot(nor,pos) + plane(4) ) / dot(nor, dif);
        if 0 <= a && a <= 1
            res = pos - a * dif;
        end
    end

    data(ii).intersect = res;
    %a = dot(nor, res) + plane(4);
    %fprintf('intersection %.2f %.2f %.2f : %.2f\n', res(1), res(2), res(3), a);
end

end
