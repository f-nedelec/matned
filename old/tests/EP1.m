function avg = EP

dt = 0.1;
L = 50;
a = dt * 0.05;
d = dt * 0.1;
h = dt * 1;
sigma = 0;

de = d + h * (1-sigma);
r  = a / ( a + d );

s = zeros(L+1, 1);
avg = zeros(L+1, 1);


steps = 1000000;

for i = 1 : steps 

    site = 1 + floor( L * rand );
            
    if s( site ) == 0
        
        if rand < a
            s( site ) = 1;
        end
        
    else
        
        if ( site == L )
            if ( rand < de )
                s( site ) = 0;
            end
        else
            if rand < d
                s( site ) = 0;
            else
                if ( s( site + 1 ) == 0 ) & ( rand < h )
                    s( site ) = 0;
                    s( site + 1 ) = 1;
                end
            end
        end
        
    end
    
    avg = avg + s;
    
end

avg = avg / steps;

figure;
plot( avg );
hold on;
plot( [ 1 L ], [ r r ], 'k-' );
axis( [1 L 0 2 * r ] );