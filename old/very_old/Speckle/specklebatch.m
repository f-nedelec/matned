
function specklebatch(Basename, BadPix, FrameMin, FrameMax, 
MedianBoxSize, ThreshMin, DilateSize, PhiRunning, RowMax, ColMax)

dlmwrite([Basename 'Args'],[Basename, BadPix, FrameMin, 
num2str(FrameMax), num2str(MedianBoxSize), num2str(ThreshMin), 
num2str(DilateSize), num2str(PhiRunning), num2str(RowMax), 
num2str(ColMax)],';');

[FlagWind,WindImgH]=figflag('Results',1);
if ~FlagWind
	WindImgH=figure('Name','Results','Visible','off');
end

[FlagWind,WindCalcsH]=figflag('Thinking',1);
if ~FlagWind
	WindCalcsH=figure('Name','Thinking','Visible','off');
end

FileNoCode(2)=length(FrameMin);
FileNoFormat=['%' num2str(FileNoCode(1:end),'%1d') 'd'];
FrameMin=str2num(FrameMin);

FrameCount=1 + FrameMax - FrameMin;

% Structural element for later dilation of convex hull
StructElement=ones(DilateSize);

tic
for i=1:FrameCount
	imgTmp=double(imread([Basename 
num2str(FrameMin+i-1,FileNoFormat) '.tif']));

	if BadPix
		imgTmp=medfilt2(imgTmp,[2 2]);
		imgTmp=imgTmp(1:end-25,20:end-1);
	end

	tmpMax=max(imgTmp(:));
	tmpMin=min(imgTmp(:));
	imgRaw=uint8((256*(imgTmp-tmpMin))/(tmpMax-tmpMin));

	IntensityDensity(i)=ceil((256/(tmpMax-tmpMin))/2);

	imgMedian=medfilt2(imgRaw,[MedianBoxSize MedianBoxSize]);
	disp(['Frame ' num2str(i+FrameMin-1) '... Median Filtered, 
time ' num2str(toc)])


	% median filter width 5 histogram
	histWorking=imhist(imgMedian);

	HistMode=find(histWorking==max(histWorking(20:end)));

	for j=(1+IntensityDensity(i)):(256-IntensityDensity(i))
 
	histWorkingMax(j)=max(histWorking(j-IntensityDensity(i):j+IntensityDe 
nsity(i)));
	end

	for j=(1+IntensityDensity(i)):(256-2*IntensityDensity(i))
 
	histWorkingSmooth(j)=mean(histWorkingMax(j-IntensityDensity(i):j+Inte 
nsityDensity(i)));
	end

	for j=(1+IntensityDensity(i)):(256-2*IntensityDensity(i)-1)
 
	histWorkingDeriv(j)=histWorkingSmooth(j+1)-histWorkingSmooth(j-1);
 
end

	for j=HistMode+10:250
		if median(histWorkingDeriv(j-5:j+5))>ThreshMin
			ThreshVal=j;
			break
		end
	end

	% thresholds, labelling connected regions
	rgnTemp=bwlabel(imgMedian>=ThreshVal);
	% count area of each region
	RgnHist=sort(rgnTemp(find(rgnTemp~=0)));
	RgnHistDiffs=diff([RgnHist;max(RgnHist)+1]);
	RgnHistCounts=diff(find([1;RgnHistDiffs]));
	% choose region of maximal area, find its convex hull and diameter
	RgnHistMode=find(RgnHistCounts==max(RgnHistCounts));
 
	RgnFeatures(i)=imfeature((rgnTemp==RgnHistMode),'ConvexHull','MajorAx 
isLength');
	if DilateSize>0
 
	rgnThresh=dilate(roipoly(imgMedian,RgnFeatures(i).ConvexHull(:,1),Rgn 
Features(i).ConvexHull(:,2)),StructElement);
	else
 
	rgnThresh=roipoly(imgMedian,RgnFeatures(i).ConvexHull(:,1),RgnFeature 
s(i).ConvexHull(:,2));
	end

	imwrite(imgRaw,['tmpRaw' num2str(i+FrameMin-1,FileNoFormat) 
'.tif'], 'tif');
	imwrite(imgMedian,['tmpMedian' 
num2str(i+FrameMin-1,FileNoFormat) '.tif'], 'tif');
	imwrite(rgnThresh,['tmpThresh' 
num2str(i+FrameMin-1,FileNoFormat) '.tif'], 'tif');

	disp(['... Thresholded, time ' num2str(toc)])
end

% determine minimal n^2 x n^2 box
CropDim=2^nextpow2(max([RgnFeatures.MajorAxisLength]));

set(WindImgH,'Visible','on');
set(WindCalcsH,'Visible','on');

CoordsBest=[];

for i=1:FrameCount
	imgRaw=imread(['tmpRaw' num2str(i+FrameMin-1,FileNoFormat) '.tif']);
	imgMedian=imread(['tmpMedian' 
num2str(i+FrameMin-1,FileNoFormat) '.tif']);
	rgnThresh=imread(['tmpThresh' 
num2str(i+FrameMin-1,FileNoFormat) '.tif']);

	% region restricted spindle image
	RawMean(i,1)=i+FrameMin-1;
 
	[imgRawTmp,rgnThreshRestricted,RawMean(i,2)]=imagerestrict(imgRaw,rgn 
Thresh,CropDim);
	imgRawRestricted=uint8(imgRawTmp);

	% region-restricted median
	MedianMean(i,1)=i+FrameMin-1;
	[imgMedianTmp,rgnThreshRestricted,MedianMean(i,2)]=imagerestrict(imgMedian,rgnThresh,CropDim);
	imgMedianRestricted=uint8(imgMedianTmp);

	disp(['Frame ' num2str(i+FrameMin-1) '... Resized, time ' 
num2str(toc)])

	if i==1
		if PhiRunning==0
 
	Orientation=imfeature(rgnThreshRestricted,'Orientation');
			PhiRunning=90-Orientation.Orientation;
			RowMax=0;
			ColMax=0;
		end
		PhiMax=0;
	else
		imgI=double(imgRawRotTrans)-MedianMean(i-1,2);
		imgI(~logical(rgnThreshRotTrans))=0;
		imgFFTi=fft2(imgI);
		imgI1=double(imgRawRestricted)-MedianMean(i,2);
		imgI1(~logical(rgnThreshRestricted))=0;

 
	ImgConv=real(ifft2(imgFFTi.*fft2(imrotate(imgI1,PhiRunning+180,'Bicub 
ic','Crop'))));

		CoordsConv=[0, max(ImgConv(:))];

		PhiMax=0;

		for PowerCount=3:-1:-2
			for phi=[PhiMax-1.25*2^PowerCount 
PhiMax+1.25*2^PowerCount]
 
	ImgConv=real(ifft2(imgFFTi.*fft2(imrotate(imgI1,PhiRunning+180+phi,'B 
icubic','Crop'))));
				CoordsConv=[CoordsConv; phi, max(ImgConv(:))];
			end

 
	PhiMax=CoordsConv(find(CoordsConv(:,2)==max(CoordsConv(:,2))),1);
 
end

		% Having determined best phi with raw image, find 
best overlap from blurred
		imgI=double(imgMedianRotTrans)-MedianMean(i-1,2);
		imgI(~logical(rgnThreshRotTrans))=0;
		imgI1=double(imgMedianRestricted)-MedianMean(i,2);
		imgI1(~logical(rgnThreshRestricted))=0;
 
	ImgConv=real(ifft2(imgFFTi.*fft2(imrotate(imgI1,PhiRunning+180+PhiMax 
,'Bicubic','Crop'))));
		[RowMax,ColMax]=find(ImgConv==max(ImgConv(:)));

		% Subtract 1 from translation coordinates since DC at 1,1
		if RowMax>CropDim/2
			RowMax=RowMax-CropDim;
		end
		if ColMax>CropDim/2
			ColMax=ColMax-CropDim;
		end
	end

 
	imgRawRot=imrotate(imgRawRestricted,PhiRunning+PhiMax,'Bicubic','Crop 
');
	imgRawRotTrans=imtranslate(imgRawRot,RowMax,ColMax);
	imwrite(imgRawRotTrans,[Basename 'Aligned' 
num2str(i+FrameMin-1,FileNoFormat) '.tif'], 'tif', 'Compression', 
'none');

 
	imgMedianRot=imrotate(imgMedianRestricted,PhiRunning+PhiMax,'Bicubic' 
,'Crop');
	imgMedianRotTrans=imtranslate(imgMedianRot,RowMax,ColMax);
	imwrite(imgMedianRotTrans,[Basename 'MedAligned' 
num2str(i+FrameMin-1,FileNoFormat) '.tif'], 'tif', 'Compression', 
'none');

 
	rgnThreshRot=imrotate(rgnThreshRestricted,PhiRunning+PhiMax,'Bicubic' 
,'Crop');
	rgnThreshRotTrans=imtranslate(rgnThreshRot,RowMax,ColMax);
	imwrite(rgnThreshRotTrans,[Basename 'RgnAligned' 
num2str(i+FrameMin-1,FileNoFormat) '.tif'], 'tif', 'Compression', 
'none');

	PhiRunning=PhiRunning+PhiMax;

	CoordsBest=[CoordsBest; i+FrameMin-1 PhiMax PhiRunning RowMax ColMax];

	figure(WindCalcsH);
 
	plot(FrameMin:i+FrameMin-1,CoordsBest(:,2),'r-*',FrameMin:i+FrameMin- 
1,CoordsBest(:,4),'g-+',FrameMin:i+FrameMin-1,CoordsBest(:,5),'b-x');
	set(gca,'XTick',FrameMin:i+FrameMin-1);
	legend('Phi','Row','Col');
	figure(WindImgH);
	imshow(imgRawRot);
	drawnow;

	disp(['... Oriented, time ' num2str(toc)])
end

dlmwrite([Basename 'TransformCoord'],CoordsBest,';');
dlmwrite([Basename 'MedianMean'],MedianMean,';');
dlmwrite([Basename 'RawMean'],RawMean,';');


%$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$%
function [imgOut, rgnOut, rgnMean]=imagerestrict(imgIn, rgnIn, CropDim)
% imagerestrict	Given an input image and logical roi, returns image
%						zero outside region 
centered in CropDim x CropDim square

rgnMean=mean(imgIn(logical(rgnIn)));
RgnCentroid=imfeature(rgnIn,'Centroid');

imgTemp=imgIn;

imgTemp(CropDim,CropDim)=0;
imgTemp=imtranslate(imgTemp,CropDim/2-round(RgnCentroid.Centroid(2)), 
CropDim/2-round(RgnCentroid.Centroid(1)));

rgnTemp=rgnIn;
rgnTemp(CropDim,CropDim)=0;
rgnTemp=imtranslate(rgnTemp,CropDim/2-round(RgnCentroid.Centroid(2)), 
CropDim/2-round(RgnCentroid.Centroid(1)));

imgOut=imgTemp(1:CropDim,1:CropDim);
rgnOut=rgnTemp(1:CropDim,1:CropDim);

%$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$%
function imgOut=imtranslate(imgIn, TransRows, TransCols)
% imtranslate(img, row, col) translates image, returning zero-padded 
image of same size

ImgRows=size(imgIn,1);
ImgCols=size(imgIn,2);
imgTmp=imgIn;

if TransRows>0
	imgTmp=[zeros(TransRows,ImgCols); imgTmp];
	imgTmp(end-TransRows+1:end,:)=[];
elseif TransRows<0
	imgTmp(1:-TransRows,:)=[];
	imgTmp(ImgRows,:)=0;
end

if TransCols>0
	imgTmp=[zeros(ImgRows,TransCols) imgTmp];
	imgTmp(:,end-TransCols+1:end)=[];
elseif TransCols<0
	imgTmp(:,1:-TransCols)=[];
	imgTmp(:,ImgCols)=0;
end

imgOut=imgTmp;
