function [ sTUB, sDNA ] = spin_plot_ellipse(opt)

% Use the ellipse fits to align the spindles and calculate an average image

if nargin < 1
    opt.visual = 0;
end

try
    data = load('results_ellipse.txt');
catch
    disp('File "results_ellipse.txt" not found');
    return;
end

sel = spin_select_bipolar;




%% Summing all structures, after bringing them in register

imsize = data(1,4:5)-data(1,2:3)+[1,1];
DNA = spin_load_images('dna', 1);
TUB = spin_load_images('tub');

%magnification = 4:
D = 4 * imsize(1);

if opt.visual > 2
    hFig = figure('Name', 'Spots', 'MenuBar','None', 'Position', [300 300 D D]);
    hAxes = axes('Units', 'pixels', 'Position', [1 1 D D] );
else
    hFig = [];
end

sDNA = zeros(imsize);
sTUB = zeros(imsize);

for inx = 1:length(TUB)
    for p = 1:size(data,1)
        if sel(p)
            %angle of the structure in degrees:
            angle = -data(p, 8+5*inx) * (180/pi);
            
            %center of the structure:
            cen = data(p,(6:7)+5*inx) + data(p,2:3) - [1,1];
            rec = make_square(cen, imsize(1));
            
            %extract data, wiht global background subtraction:
            dna = imrotate(image_crop(DNA.data, rec, DNA.back), angle, 'bilinear', 'crop');
            tub = imrotate(image_crop(TUB(inx).data, rec, TUB(inx).back), angle, 'bilinear', 'crop');

            %sum up:
            sDNA = sDNA + dna;
            sTUB = sTUB + tub;

            if opt.visual > 2
                show_overlay(tub, tub, dna, hAxes);
                drawnow;
            end
        end
    end
end

show_image(sTUB, 'Name', 'averaged TUB');
show_image(sDNA, 'Name', 'averaged DNA');
show_overlay(sTUB, sTUB, sDNA); set(gcf, 'Name', 'averaged bipolar structure');

if ~isempty(hFig)
    delete(hFig);
end

%% plot the angles
angle_dna = data(sel, 8);
angle_tub = data(sel, 8+5*inx);

rotation = angle_dna;
angle_tub = angle_tub + rotation;

figure, plot(angle_tub(:,1), 'bo');
hold on, plot(pi + angle_tub(:,1), 'ko');

figure, plot(cos(angle_tub(:,1)), sin(angle_tub(:,1)), 'bo');
hold on, plot(cos(pi+angle_tub(:,1)), sin(pi+angle_tub(:,1)), 'ko');
axis equal;


disp('Unfinished work');


    function rec = make_square(cen, dia)
        lx  = round( cen(1)-dia/2 );
        ly  = round( cen(2)-dia/2 );
        rec = [ lx, ly, lx+dia-1, ly+dia-1 ];
    end


end