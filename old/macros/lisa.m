function andrei( filename, im )

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

if ( nargin <= 1 )
    if ischar( filename )
        im        = tiffread( filename );
    else
        im        = filename;
        filename  = 'file not specified';
    end
end

if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

%==================find the background level:

%rect       = ROImouserect;
%blackarea  = crop( im, rect );
%base1      = mean( mean( blackarea ));
base1      = min(min( im ));
base2      = imbase( im );
disp( sprintf('base = %i,   %i', round(base1), round(base2) ) );

%==================   select a rectangular ROI:

showim( im );
drawnow;

%roi   = ROImouserect;
poly = ROImousepoly;
[mask, roi] = maskpolygon( poly );
%showim( mask );
close;

%==================   extract the image from the ROI:

subim  = ( crop( im, roi ) - base2 ) .* mask;
subim  = subim .* ( subim > 0 );

%==================   :

immin = min( min( subim ));
immax = max( max( subim ));


%==================   fit an ellipse to the sub-image

[cen, mom, rot, sv] = barycenter2( subim, 1 );

%==================   rotate the image with the angle found

angle = mom(1);

%==================   make a summary figure:

figid=figure('Name', filename, 'Position', [ 100, 100, 800, 400 ] );
set(gca, 'Position', [0.05 0.05 0.45 0.95]);
showim( subim, 'nofigure', 'axisticks', 'trueratio' );

