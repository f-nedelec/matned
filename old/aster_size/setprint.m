function setprint()
% setprint()
%
% prepares for printing on A4, preserving the figure aspect-ratio
% f.nedelec, nedelec@embl.de

set(gcf,'PaperType','A4');
set(gcf,'PaperOrientation','portrait');
%set(gcf,'PaperOrientation','landscape');
set(gcf,'PaperPositionMode','manual');

set(gcf,'PaperUnits','centimeters');
figpos = get(gcf, 'Position');

scaleX = figpos(3) / 21;
scaleY = figpos(4) / 29.7;
scale  = min( scaleX, scaleY );

left   =   21 / 2 - scale * figpos(3);
bottom = 29.7 / 2 - scale * figpos(4);

set(gcf, 'PaperPosition', [left, bottom, scale * figpos(3), scale * figpos(4) ]);


return;
