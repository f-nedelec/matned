function [filenb, names] = findfiles



%find all the image files:
list=dir('FITC*.tif');
filenb=length(list);
for i=1:filenb
   newname=list(i).name;
   names(i)=cellstr(newname);
end;
clear list;

return;

names(1)=cellstr('FITC20.tif');
names(2)=cellstr('FITC18.tif');
names(3)=cellstr('FITC49.tif');
names(4)=cellstr('FITC14.tif');
names(5)=cellstr('FITC23.tif');
names(6)=cellstr('FITC24.tif');
names(7)=cellstr('FITC45.tif');
names(8)=cellstr('FITC5-02.tif');
names(9)=cellstr('FITC8-16.tif');
names(10)=cellstr('FITC34.tif');

filenb=length(names);
return;
