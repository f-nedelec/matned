function files = gohta_dir()

files  = dir('data*');

nfiles = size( files, 1);
isd    = zeros(nfiles, 1);

for u = 1 : nfiles
    
    isd( u ) = files(u).isdir;

end

indx = find( isd == 1 );
files = files( indx );