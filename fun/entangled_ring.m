% Francois Nedelec, 16.08.2016

function entangled_ring

save_shape('flat.txt', 128, 5, 0.25, 0);
save_shape('coil.txt', 128, 5, 0.25, 0.1);


end


function save_shape(filename, n, ord, L, R)

t=2*pi*(1:n)/n;
u=ord*t;

x=cos(t).*(1+R*cos(u));
y=sin(t).*(1+R*cos(u));
z=R*sin(u);

fid=fopen(filename, 'w');
for i=1:length(x); fprintf(fid, '%f %f %f,\\\n', L*x(i), L*y(i), L*z(i)); end
fclose(fid);

end