function msbFile(make_file)

if nargin < 1
    make_file = 0;
end


cd('run0000');
[ bins, h_simF, hr_simF ] = contactTimeHistogram(0);
cd('..');

cd('run0001');
[ bins, h_simL, hr_simL ] = contactTimeHistogram(0);
cd('..');

cd('run0002');
[ bins, h_simFL, hr_simFL ] = contactTimeHistogram(0);
cd('..');


[ bins, h_exp, hr_exp ] = contactTimeHistogram(1);
    

%%

figure;
hb = bar(bins, h_exp, 'b');
set(hb, 'FaceColor', [0.5 0.5 0.5], 'LineWidth', 1);
hold on;


%lines for simulation data:
plot(bins, h_simF,  'r-', 'LineWidth', 3);
plot(bins, h_simL,  'b-', 'LineWidth', 3);
plot(bins, h_simFL, 'k-', 'LineWidth', 3);

xlim([0 max(bins)]);
xlabel('Contact time (s)', 'FontSize', 20);
ylabel('Normalized density / 10^{-3}', 'FontSize', 20);
set(gca, 'FontSize', 20);
set(gca, 'TickDir', 'out');



%% save the histogram in CSV format
if ( make_file )

    fid = fopen('histogram.csv', 'wt');
    fprintf(fid, 'contact time,  experimental, model-L, model-F, model-FL\n');

    for x=1:length(bins)
        fprintf(fid, '%8i, ', bins(x));
        fprintf(fid, '%8i, ', hr_exp(x));
        fprintf(fid, '%8i, ', hr_simL(x));
        fprintf(fid, '%8i, ', hr_simF(x));
        fprintf(fid, '%8i\n', hr_simFL(x));
    end
    
    fclose(fid);
    fprintf('Saved results in file histogram.csv\n');
    
end



end
