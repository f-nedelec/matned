function data = convert_fibers(data)

% Convert from Jon's data format to Francois's format
% F. Nedelec 31.03.2018

if isfield(data, 'mts')
    try
        % compatibility layer with READ_RAW_COORDS
        d = struct('id', 1:length(data.mts));
        for m = 1 : length(data.mts)
            d(m).id = m;
            d(m).pts = data.mts{m};
            d(m).type = data.labels(m);
        end
        data = d;
    catch
    end
end


end