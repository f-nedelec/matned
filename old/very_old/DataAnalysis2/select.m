function ds = select(data, indx, value)

% select data that verify:
% param( indx ) = value;

others = setdiff( [1:size(data,2)], indx );

j=1;
for i=1:size(data,1)
   
   sel = all( data(i,indx) == value );
   
   if ( sel )
      ds(j,:)     = data(i, others);
      j = j + 1;
   end
   
end
