function [ bins, h_sca, h ] = contactTimeHistogram(exp, make_plot)

% return a histogram of contact-times

if nargin < 2
    make_plot = 0;
end

dbin  = 6;
bins  = 0:dbin:350;
scale = 1e3;

%%
if ( exp == 1 )
    
    % Load data from D.Foetkhe:
    fid   = fopen('contactsExp.txt', 'rt');
    data  = textscan(fid, '%d %d %d', 'CommentStyle', '#');
    evts  = 3 * data{3};  % 1 frame = 3 seconds

    h     = histc(evts, bins);
    h_sca = scale * h / ( dbin * sum(h) );
    
        
else
    

    system('cut -c2- mtcontacts.sta | tail -n +2 > mtcontacts.cut');
    s = load('mtcontacts.cut');

    % select the catastrophe that have contacted the cortex:
    sel   = find( s(:,3)> 0 );
    % calculate contact time:
    evts  = s(sel,4) - s(sel,3);
    h     = histc(evts, bins);
    h_sca = scale * h / ( dbin * sum(h) );


end


%% Plot
if ( make_plot )
    figure;
    hb = bar(bins, h_sca, 'b');
    set(hb, 'FaceColor', [0.5 0.5 0.5], 'LineWidth', 1);

    h_max = 1.2 * max(h_sca);
    axis([0 max(bins) 0 h_max]);

    xlabel('Contact time (s)', 'FontSize', 20);
    ylabel('Normalized density / 10^{-3}', 'FontSize', 20);
    set(gca, 'FontSize', 20);
    set(gca, 'TickDir', 'out');
end


%%
fprintf( '%i events, average: %.2f sec\n', length(evts), mean(evts) );



end
