function im2 = filterspots( im, n )

% im2 = filterspots( im, n )
%
% find local extremum values of the image

if ( isfield(im,'data') ) im = im.data; end
if ( nargin < 2 ) n = 1; end

mask = maskcircle1( 2*n + 1 );
[ mdx, mdy ] = find( mask );
dd   = n + 1;

ismax = ones( size(im) );
ismin = ones( size(im) );

for indx = 1:length(mdx)
   
   dx = mdx(indx) - dd;
   dy = mdy(indx) - dd;
   
   lx = max(1, 1 - dx );
   ux = min(size(im,1), size(im,1) - dx);
   ly = max(1, 1 - dy);
   uy = min(size(im,2), size(im,2) - dy);
   
   ismax(lx:ux, ly:uy) = ismax(lx:ux, ly:uy) &...
      ( im(lx:ux, ly:uy) > im( lx+dx:ux+dx, ly+dy:uy+dy ) );
   
   ismin(lx:ux, ly:uy) = ismin(lx:ux, ly:uy) &...
      ( im(lx:ux, ly:uy) < im( lx+dx:ux+dx, ly+dy:uy+dy ) );
   
end

im2 = ismax + ismin;

return;