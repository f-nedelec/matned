
function res = nb_intersect(data, threshold, draw)
%
%
% F. Nedelec, 01.04.2018

if nargin < 3
    draw = 0;
end

n_mt = length(data);

res = 0;

for ii = 1:n_mt
    pp = data(ii).intersect;
    if ~isempty(pp)
        for jj = ii+1:n_mt
            qq = data(jj).intersect;
            if ~isempty(qq) && data(ii).type ~= data(jj).type
                if ( norm( pp - qq ) < threshold )
                    res = res + 1;
                    if draw 
                        PQ = vertcat(pp, qq);
                        plot3(PQ(:,1), PQ(:,2), PQ(:,3), '*');
                        line(PQ(:,1), PQ(:,2), PQ(:,3));
                    end
                end
            end
        end
    end
end

end