function hideki( filename, im )
% hideki() : macro to measure microtubule asters from fluorescence images
% the macro can be called with different arguments:
% usage:  1. hideki()
%         2. hideki(filename)
%         3. hideki(image_data)
%         4. hideki(filename, image_data )
%
% with case 1. : prompts for a file to open '*.tif' or '*.stk'
%           2. : open the image 'filename' in the current directory
%           3. : use image_data, filename is set to 'file not specified'
%           4. : use 'filename' with the data from 'image_data'
%
% description:
%
% the 'tiff' image is loaded, and displayed on the screen
% the user can then select a region of interest in two way:
%     a. select a square by click and drag
%     b. select a polygon by clicks, right-click or 'space' closes the polygon.
% the center C of the aster is found in this region of interest.
% the profile of fluorescence as a function of the distance from C is
% calculated. From this profile, two parameters are calculated, which for
% perfect asters (i.e. straight microtubules) should correspond to:
%       1. the average length of the microtubules (pixels)
%       2. the total mass of polymerised tubulin (arbitrary unit)
%
% a figure is produced which summarizes the process,
% also two files are created in the current directory:
%   file 'hideki_record.txt' records filename, clicks, radius and mass
%   file 'hideki_excel.txt' can be directly imported in excel
%
% pitfall:
% only works if the pixel values are proportional to what you measure
% does NOT work for DIC or Dark-field, but does for fluorescence
% the macro would work perfectly if all the microtubules are straight.
% (was tested with simulated data)
% it is rarely the case... in this case, the measure is an underestimation.
% the main problem seems to be that fluorescence in the center is much
% higher than expected from just the microtubules alone.
%
% contact: f. nedelec, 1999-2004 nedelec@embl.de

% adjustable parameter for the fluorescence arbitrary unit output:
fluo_scaling_exp = 4;
fluo_scaling = 10 ^ fluo_scaling_exp;

% with no argument, open a dialog to let the user choose a file:
if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

% with one argument, it might be a filename or the data:
if ( nargin <= 1 )
    if ischar( filename )
        im        = tiffread( filename );
    else
        im        = filename;
        filename  = 'file not specified';
    end
end

% tiffread puts the pixel values under the record .data, 
% in that case, we extract just the data:
if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

%==================find the background level:

base1      = min(min( im ));
base2      = imbase( im );
%disp( sprintf('base = %i,   %i', round(base1), round(base2) ) );

%==================   let the user select a ROI:

% showing the log of the pixel values is nicer, but much slower:
% disable, if speed is a problem:
% imlog = log( im );
% showim( imlog );

showim( im );
poly = ROImousepoly;
close;

%==================   extract the image from the ROI:

[mask, roi] = maskpolygon( poly );

subim  = crop( im, roi );
base3  = min(min( subim ));
subim  = mask .* subim + ( 1 - mask ) .* base2;

%==================   find the center of the aster, as the brightest spot:

%showim( subim );
center = findbrightspot( subim );

%==================   compute the maximum radius allowed by the ROI:

radius = floor( max( [ center, roi(3:4) - roi(1:2) - center ] ) );
%disp( sprintf('radius = %i ', round(radius) ) );

%==================   compute the radial profile of pixel value from the center:

% binning parameter:
bin = 4;

%[ val2, var2, area2 ] = distprofile( subim, center, bin,  radius );
[ val, var, area ]    = distprofile( subim, center, bin,  radius, mask );


%figure; plot( area ); hold on; plot( area2 ); plot(val); plot(val2);

base4 = min(val);
disp( 'check that all these values are in the same range:');
disp( sprintf('min %i, base %i, roi_min %i, profile_min %i',...
               round(base1), round(base2), round(base3), round(base4) ) );

% distance from the center in pixel units:
distance   = bin *( [ 1:length( val ) ] - 0.5 );
% correct the profile by subtracting the background level (base) 
% and multiplied by the area( distance ) to get fluorescence( distance )
profile    = ( val - base4 ) .* area;

%==================   mass of polymer:
%the total mass of the aster is the sum of all pixel values in the
%corrected profile. The factor fluo_scaling should be adjusted:
totalmass  = sum( profile ) / fluo_scaling;

%this is another way to get the same number:
% totalmass2 = sum( sum( subim .* mask ) ) - base4 * sum(sum(mask));
% totalmass2 = totalmass2 / fluo_scaling;
%disp( sprintf('check: mass  %.1f  ~  %.1f', totalmass, totalmass2 ) );

%==================   extract the mean radius of the aster:
% if all the microtubules are straight, the mean length is simply the
% profile divided by its value in zero, which should also be its maximum
% it is rarely exactly the case...
% the main problem seems to be that fluorescence in the center is lower
% than expected from just the microtubules alone. Either some microtubule
% do not start at the center, or the signal somehow saturates
% we that max(profile) and not profile(1) here.

profile    = profile ./ max(profile);
meanlength = bin * sum( profile );

%display to matlab command window:
mess1 = sprintf('sum intensity %6.1f  10^%i a.u.\n', totalmass, fluo_scaling_exp );
mess2 = sprintf('mean radius   %6.1f      pixels\n', meanlength );
disp([ mess1, mess2 ]);

%==================   make a summary figure:

figure('Name', filename, 'Position', [ 100, 100, 800, 400 ] );
set(gca, 'Position', [0.05 0.05 0.45 0.95]);
showim( log(subim), 'nofigure', 'axisticks', 'trueratio' );

%--------------plot roi-polygon:
plot( poly(:,2)-roi(2), poly(:,1)-roi(1), 'b-');

%--------------a cross at the center:
plot(center(2), center(1), 'gx', 'MarkerSize', 15, 'LineWidth', 1);

%--------------plot circle of radius 'meanlength':
cx = center(2) + meanlength * cos( 0:0.1:6.3 );
cy = center(1) + meanlength * sin( 0:0.1:6.3 );
plot( cx, cy, 'g-');

%--------------plot circle of radius 'radius', ie. the max allowed by ROI:
cx = center(2) + radius * cos( 0:0.1:6.3 );
cy = center(1) + radius * sin( 0:0.1:6.3 );
plot( cx, cy, 'w-');

%make a little figure with the fluorescence profile:
axes( 'Position', [ 0.58 0.12 0.4 0.7 ] );
plot( distance, profile );
axis( [ 0, radius, 0, max( profile ) ] );
hold on;

%make a vertical line at the mean radius:
plot( [ meanlength, meanlength], [ 0, max( profile ) ], 'g-', 'Linewidth',2 );

%add the two measures on the figure:
text( 0, 1.2, filename, 'FontSize', 14, 'Interpreter', 'none', 'FontWeight', 'bold');
ylabel( 'intensity  ( arbitrary units )', 'FontSize', 12 );
xlabel( 'distance   ( pixels )', 'FontSize', 12 );
text( 0, 1.05, [mess1, mess2], 'FontSize', 14, 'FontName', 'fixedWidth');

%prepare figure for printing:
setprint;

%=============record data to file 'hideki_record':

file_exist = exist('hideki_record.txt' , 'file');
file = fopen('hideki_record.txt', 'a');

if ( file == -1 )
    disp('error openning file <hideki_record.txt>');
else

    if ( file_exist == 0  )
        fprintf( file, '%% hideki macro logfile created on %s\n', date);
        fprintf( file, '%% %9s %20s  %6s  %6s    clicked polygon vertices : x, y\n', ...
            'date','filename', 'radius', 'mass' );
    end

    fprintf( file, '%11s %20s  %6.1f  %6.1f  ', date, ...
      sprintf('''%s''', filename), meanlength, totalmass );

    for i = 1 : size(poly, 1);
        fprintf( file, '  %.0f %.0f', poly(i,1), poly(i,2) );
    end
    fprintf( file, '\n');
    fclose( file );  
    
end

%=============record data to file 'hideki_excel':
file_exist = exist('hideki_excel.txt' , 'file');
file = fopen('hideki_excel.txt', 'a');

if ( file == -1 )
    beep;
    disp('error opening file <hideki_excel.txt> : check that it is not open by excel');
else
    
    if ( file_exist == 0  )
        fprintf( file, '%11s\t%20s\t%6.3f\t%6.3f\t%3.0f\t%3.0f\n',...
            'date','filename', 'radius', 'mass', 'center_x','center_y');
    end

    centerx = center(1) + roi(1) - 1;
    centery = center(2) + roi(2) - 1;

    fprintf( file, '%11s\t%20s\t%6.3f\t%6.3f\t%3.0f\t%3.0f\n', ...
        date, filename, meanlength, totalmass, centerx, centery );
    fclose( file );
    
end

