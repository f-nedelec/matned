function lambda = fit_exponential(data, seg, fig, col)

% Fit exponential distribution from truncated data
%
% lambda = fit_exponential(data, [min, max])
%
% Given a set of datapoints assumed to be exponentially distributed, 
% this will use the data within [min, max] to calculate the parameter
% of the exponential. The methods are described in:
%
% Fitting an Exponential Distribution
% Roberto Fraile and Eduardo Garcia-Ortega
% J. Appl. Meteor. 44, 1620?1625 (2005)
% http://journals.ametsoc.org/doi/pdf/10.1175/JAM2271.1
%
% Thorn, K. S., Ubersax, J. A. & Vale, R. D. 
% Engineering the processive run length of the kinesin motor. 
% J Cell Biol 151, 1093?1100 (2000).
%
% Example:
% e = -log(rand(100000,1));
% fit_exponential(e(logical(e>0.5)), [0.5, 1000])
%
% F. Nedelec, 2013 -- 2016

if nargin < 4
    col = [ 0 0 1 ];
end

if nargin < 3
    fig = 1;
end

%% if the data is representative, its mean is the coefficient of the exponential:

lambda_all = 1 / mean(data);

fprintf(1, 'Mean of data yields lambda = %f (1/lambda = %f)\n', lambda_all, 1.0/lambda_all);


%% if a segment is provided, we use Roberto Fraile's method

if nargin > 1
    
    lambda = lambda_all;
    
    xi = seg(1);
    xu = seg(2);
    
    data_in = data(logical(( xi <= data ) .* ( data < xu )));
    ex = mean(data_in);
   
    delta = 1;
    while delta > 1e-7
        
        ei = exp(-lambda*xi);
        eu = exp(-lambda*xu);
        lambda_new = ( ei - eu ) / ( (ex-xi)*ei - (ex-xu)*eu );
        delta = abs( lambda - lambda_new );
        lambda = lambda_new;
        %fprintf(2, '--- %f\n', lambda);
        
    end
    fprintf(1, 'Fraile et al. gives lambda = %f (1/lambda = %f)\n', lambda, 1.0/lambda);
    fprintf(1, '   %i points inside [%f %f]\n', numel(data_in), seg(1), seg(2));
else
    
    data_in = data;
    
end

%% Thorn's method:

if 0 
    % Thorn, K. S., Ubersax, J. A. & Vale, R. D.
    % Engineering the processive run length of the kinesin motor.
    % J Cell Biol 151, 1093?1100 (2000).
    
    % Only the lower cut-off is considered
    if nargin > 1
        
        xi = seg(1);
        data_sup = data(logical(xi <= data));
        
        n = length(data_sup);
        s = [0, reshape(sort(data_sup), 1, n) - xi];
        p = 1 - (0:n)/n;
        
        % calculate the mean of the shifted data:
        lambda_thorn = 1 / ( sum( [diff(s), 0] .* p ) );
        fprintf(1, 'Thorn et al.  gives lambda = %f (1/lambda = %f)\n', lambda_thorn, 1.0/lambda_thorn);
        fprintf(1, '   %i points above cutoff %f\n', n, seg(1));
        
    end
    
end

%%
if fig
    
    if fig == 1
        % make a new Figure:
        figure;
        hold on;
    end
    
    % bin the data:
    nbins = max( floor(numel(data_in)/25), 10 );
    [h, cx] = hist(data_in, nbins);
    
    xzero = seg(1);

    % normalization:
    ci = min(cx) - 0.5 * mean(diff(cx));
    cu = max(cx) + 0.5 * mean(diff(cx));
    
    vale = ( exp(-lambda*ci) - exp(-lambda*cu) ) / lambda;
    valh = mean(h) * ( cu - ci );
    
    h = h * ( vale / valh );
   
    p0 = plot(cx, h/exp(-lambda*xzero), 'o', 'LineWidth', 2, 'MarkerSize', 8, 'Color', col);
    l0 = sprintf('%i data points', numel(data_in));
 
    % plot fit:
    fx = seg(1):0.5:seg(2);
    p1 = plot(fx, exp(-lambda*fx)/exp(-lambda*xzero), '-', 'LineWidth', 2, 'Color', col);
    l1 = sprintf('exp(-%.3f*L)', lambda);

    if ( 0 )
        p2 = plot(fx, exp(-lambda_all*fx)/exp(-lambda_all*xzero), '--', 'Color', col);
        l2 = sprintf('exp(-%.3f*L)', lambda_all);

        p3 = plot(fx, exp(-lambda_thorn*fx)/exp(-lambda_thorn*xzero), ':', 'Color', col);
        l3 = sprintf('exp(-%.3f*L)', lambda_thorn);
        legend([p0, p1, p2, p3], l0, l1, l2, l3);
    else
        legend([p0, p1], l0, l1);
    end
    
    xlabel('Length');
    ylabel('Probability density');
    
end


end

