function q = quadratic_fit( im, w, iso )

% q = quadratic_fit( im, weight, iso )
%
% calculate the coefficients of the best fit for the data contained in `im',
% by  quad(X,Y) = a*X^2 + b*X*Y + c*Y^2 + d*X + e*Y + f
% returns q = [ a, b, c, d, e, f ]
%
% optional argument mask specifies the pixels that should be used for the fit
% 
% F. Nedelec, April 2014

if nargin < 3
    iso = 0;
end

if  nargin < 2  || isempty(w)
    w = ones(size(im));
end

%% horizontal/vertical lines:

hx    = 1:size(im,1);
hxx   = hx .^ 2;
hxxx  = hxx .* hx ;
hxxxx = hxx .* hxx;

vy    = (1:size(im,2))';
vyy   = vy .^ 2;
vyyy  = vyy .* vy;
vyyyy = vyy .* vyy;

%% sums of pixels coordinates:

sw     = sum( sum( w ));
sx     = sum( hx * w );
sy     = sum( w * vy );

sxx    = sum( hxx * w );
sxy    = hx * w * vy;
syy    = sum( w * vyy );

sxxx   = sum( hxxx * w );
sxxy   = hxx * w * vy;
sxyy   = hx * w * vyy;
syyy   = sum( w * vyyy );

sxxxx  = sum( hxxxx * w );
sxxxy  = hxxx * w * vy;
sxxyy  = hxx * w * vyy;
sxyyy  = hx * w * vyyy;
syyyy  = sum( w * vyyyy );


% sums of pixels values:
if ( isfield(im, 'data') ) 
    z = double(im.data) .* double(w);
else
    z = double(im) .* double(w);
end

sxxz   = sum( hxx * z );
sxyz   = hx * z * vy;
syyz   = sum( z * vyy );
sxz    = sum( hx * z );
syz    = sum( z * vy );
sz     = sum(sum( z ));

% solve system 

if iso > 0
    
    S = [ sxxxx+syyyy+2*sxxyy, sxxx+sxyy, sxxy+syyy, sxx+syy;...
          sxxx+sxyy, sxx, sxy, sx;...
          sxxy+syyy, sxy, syy, sy;...
          sxx+syy,   sx,  sy,  sw ];

    c = S \ [ sxxz + syyz; sxz; syz; sz ];

    q = [ c(1), 0, c(1), c(2), c(3), c(4) ];

else
    
    S = [ sxxxx, sxxxy, sxxyy, sxxx, sxxy, sxx; ...
          sxxxy, sxxyy, sxyyy, sxxy, sxyy, sxy; ...
          sxxyy, sxyyy, syyyy, sxyy, syyy, syy; ...
          sxxx,  sxxy,  sxyy,  sxx,  sxy,  sx ; ...
          sxxy,  sxyy,  syyy,  sxy,  syy,  sy ; ...
          sxx,   sxy,   syy,   sx,   sy,   sw];

    q = S \ [ sxxz; sxyz; syyz; sxz; syz; sz ];

end
    
%fprintf(' Quadratic fit: %.3f XX  %+.3f XY  %+.3f YY  %+.3f X  %+.3f Y  %+.3f\n', q);
    

end
   