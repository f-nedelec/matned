function plotTraits(n)

% plot many traits for each simulation, using little pie-charts
% F.nedelec, May 2007

if 1
    range = [1, 5, 0.1, 0.6];
    %size of the plot in pixels, + radius of pie charts
    sz = [ 1000, 625, 9, 11 ];
    extended = 0;
else
    range = [1, 5, 0.2, 1.0];
    sz = [ 1000, 1000, 9, 11 ];
    extended = 1;
end


if nargin < 1
    n = 0;
end

if ~ exist('traits.sta', 'file')
    error('traits.sta missing in %s: run collectTraits!\n', pwd);
else
    system('cut -c4- traits.sta > traits.cut');
    traits = load('traits.cut');
end

if ~ exist('pam.sta', 'file')
    %generate the parameter summary file 'pam.sta'
    system('tell.py mtdynspeed1 mtdyntrans1 > pam.sta');
end

if ~ exist('pam.cut', 'file')
    system('cut -c4- pam.sta | tail -n +2 > pam.cut');
end

pam = load('pam.cut');
vg  = pam(:,2) * 60;
ca  = pam(:,4) * 60;


if n > 0
    figure(1);
    trait = traits(:,n+1);
    plot( traits(:,1), trait, '.' );
    title(sprintf('trait %i',n));
    fprintf('trait %i = %f +/- %f\n', n, mean(trait), var(trait))
end


tsize = size(traits,1);
psize = size(pam, 1);

if tsize ~= psize
    error('size missmatch traits=%i pam=%i', tsize, psize);
end
if  any( pam(:,1) ~= traits(:,1) )
    error('missmatch between pam.sta and traits.sta');
end


%% ---- make a quick figure with all the points

figure(2);
clf;
axis manual;
hold on;
axis(range);


C    = checkTraits(traits);
if n > 0 
    Cs = C(:,n+1);
else
    Cs = ( sum(C(:,2:11), 2) == 10 );
end
bad  = find(Cs == 0);
good = find(Cs == 1);

plot( vg(bad),  ca(bad),  'kx' );
plot( vg(good), ca(good), 'go', 'MarkerFaceColor', 'g');

fprintf( '%i/%i successful simulations\n', size(good,1), size(C,1))

if n > 0
    return
end


%% ---make a nice figure with many pie-charts


figure(3);
clf;
set(gcf, 'Units', 'pixels', 'Position', [10, 110, 200+sz(1), sz(2)+175]);
set(gca, 'Units', 'pixels', 'position', [120,  100,  sz(1),  sz(2)])
set(gcf, 'Renderer', 'painters')

if 0
    plot(1,0.1, 'x');
    hold on;
    plot(1,0.6, 'x');
    plot(5,0.1, 'x');
    plot(5,0.6, 'x');
end

box on;
hold on;
axis manual;
axis(range);

set(gca, 'TickDir', 'out');

set(gca, 'XTick', [1;2;3;4;5]);
xlabel( 'Growth velocity (\mu m/min)', 'FontSize', 24);

if extended==0
    set(gca, 'YTick', [0.1; 0.2; 0.3; 0.4; 0.5; 0.6]);
else
    set(gca, 'YTick', [0.2; 0.4; 0.6; 0.8; 1.0]);
end
ylabel( 'Catastrophe rate (min^{-1})', 'FontSize', 24);
set(gca, 'FontSize', 24);


ratios = [ (range(2)-range(1)) / sz(1), (range(4)-range(3)) / sz(2) ];

sel = find( ca > 0.1 );


%limit plot to 1500 points:
%if size(sel,1) > 1500
%    sel = sel(1:1500);
%end

if n < 0
    for ii = 1:size(sel,1)
        s = sel(ii);
        plot(vg(s), ca(s), '.');
        mdisc(vg(s), ca(s), C(s,2:11), sz(3), ratios);
        text(vg(s), ca(s), sprintf('%04i', C(s,1)),'Color', 'b');
    end
else
    for i = 1:size(sel,1)
        s = sel(i);
        mpie(vg(s), ca(s), C(s,2:11), sz(3:4), ratios);
    end
end

drawnow;
end

%% --------------------------------------------------------------------------

function mpie(cx, cy, C, radius, ratio)

n  = size(C,2);
% 'n' pie-charts ordered clockwise
t  = pi*( 0.5 - 2*(0:n)/n );

w  = radius(1) * ratio(1);
h  = radius(1) * ratio(2);

px = cx + w*cos(t);
py = cy + h*sin(t);

if sum(C,2) == 10
    fill(px, py, 'g', 'EdgeColor', 'k');
else
    wp  = radius(2) * ratio(1);
    hp  = radius(2) * ratio(2);
    ppx = cx + wp*cos(t);
    ppy = cy + hp*sin(t);

    fill(ppx, ppy, [0 0 0]);

    for ii = 1:n
        if ~ C(ii)
            px = cx + [ w*cos(t(ii:ii+1)), 0 ];
            py = cy + [ h*sin(t(ii:ii+1)), 0 ];
            fill(px, py, [1, 1, 1], 'LineStyle', 'none');
        end
    end


end
end



%%
function mdisc(cx, cy, C, radius, ratio)

n  = size(C,2);
t  = pi*( 0.5 - 2*(0:n)/n );

w  = radius * ratio(1);
h  = radius * ratio(2);

px = cx + w*cos(t);
py = cy + h*sin(t);

if sum(C,2) == 10
    patch(px, py, [0,1,0], 'EdgeColor', 'w');
else
    patch(px, py, [0.5,0.5,0.5], 'EdgeColor', 'w');
end
end



