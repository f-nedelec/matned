function cen1 = dotfind( im )

if ( isfield( im, 'data' ) ) im = im.data;  end

above = im - base ;    
above = above .* ( above > 0 );
cen1  = barycenter1( above );
cen2  = cen1;

if ( confidence > 1.8 )
   found = 1;
else
   found = 0;
end


if (debug==3)
   show(above);
   plot(cen1(2), cen1(1),'bx','MarkerSize',10,'Linewidth',3);
end


return;
