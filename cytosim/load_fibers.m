function res = load_fibers(filename, first_id, scale)

% Copyright Laure Maillant and F. Nedelec, EMBL Heidelberg, 2014--2015
% Created June 2014 - Modified Jan 2015, July 2015, Sep 2017

% data = load_fibers(filename, first_id, scale)
% 
% Loads coordinates from (.mv3d format) file
% 
% load_fibers returns an array whose length is the number of different 
% filaments found in the input file.
% Each entry of the array contains a field `id` and a field 'pts' which
% is a Nx3 matrix, containing on each line the (x,y,z) coordinates of 
% one point of the filament.
%
% Example:
% fib = load_fibers('points.mv3d')

if nargin < 2
    first_id = 1;
end

if nargin < 1
    fprintf('You must provide a file name for input');
end

file = fopen(filename);

if file == -1
    error('Could not find file "%s"\n', filename);
end


%% Takes each line and save the data

% Initialisation of results
cur = [];
pts = zeros(64, 3);
inx = 0;

chunk_inx = 0;
chunk_max = 512;
chunk = cell(chunk_max, 1);

res = [];

while ~feof(file)
    
    tline = fgets(file);
    
    % Skip comments
    if tline(1) == '%'
        continue;
    end
    
    data = sscanf(tline, '%u %f %f %f %f');
    
    if numel(data) > 3
        id = data(1);
        pt = [ data(2) data(3) data(4) ];
    elseif numel(data) > 2
        id = data(1);
        pt = [ data(2) data(3) 0 ];
    else
        continue
    end
    
    % Convert point coordinates if requested:
    if nargin > 2
        pt = pt * scale;
    end
    
    if id == cur
        % extend coordinates
        inx = inx + 1;
        pts(inx, :) = pt;
    else
        if inx > 0
            store_record(cur, pts(1:inx, :));
        end
        % initiate new fiber
        cur = id;
        pts(1,:) = pt;
        inx = 1;
    end

end

fclose(file);

%record last entry:
store_record(cur, pts(1:inx, :));

if chunk_inx > 0
    res = vertcat(res, cell2mat(chunk(1:chunk_inx)));
end


%%
    function store_record(id, pts)
        %fprintf(2, 'store_record %i\n', id);
        mt.id  = id + first_id;
        mt.pts = pts;
        % add to `chunk`:
        chunk_inx = chunk_inx + 1;
        chunk{chunk_inx} = mt;
        % transfer 'chunk' into 'res' if full
        if chunk_inx >= chunk_max
            if isempty(res)
                res = cell2mat(chunk);
            else
                res = vertcat(res, cell2mat(chunk));
            end
            chunk_inx = 0;
        end
    end

end


