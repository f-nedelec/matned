function IM = iplread(filename, img_first, img_last)
% image = iplread(filename, first_image, last_image)
% Reads IPLab image files
% 
% F. Nedelec, 2002. Email nedelec@embl-heidelberg.de
% last modified September 6, 2002.

if (nargin == 0)
    [filename, pathname] = uigetfile('*.ipl;*.stk', 'select image file');
    cd( pathname );
end

if (nargin<=1)  img_first = 1; img_last = 5000; end
if (nargin==2)  img_last = img_first;           end

img_skip  = 0;
img_read  = 0;
stack_cnt = 1;

[file, message] = fopen(filename, 'r', 'l');
if file == -1
    if ( isempty(findstr(filename, '.')) )
        filename = [filename,'.ipl'];
        [file, message] = fopen(filename, 'r', 'l');
    end
    if file == -1
        error(['file <',filename,'> not found.']);
    end
end

BOS = 'b';              %imported from a mac...

%================= header:

IM.filename    = [ pwd, '\', filename ];
IM.version     = setstr(fread(file, 4, 'uchar', BOS ));
IM.file_format = fread(file, 1, 'uint8', BOS);
IM.data_type   = fread(file, 1, 'uint8', BOS);
IM.width       = fread(file, 1, 'int32', BOS);
IM.height      = fread(file, 1, 'int32', BOS);
IM.reserved_1  = fread(file, 6, 'uint8', BOS);
IM.nFrames     = fread(file, 1, 'uint16', BOS);
IM.reserved_2  = fread(file, 50, 'uint8', BOS);
IM.CLUT        = fread(file, 2048, 'uint8', BOS);

%disp(sprintf('start data at %i ', ftell( file )));

%=================  data
switch( IM.data_type )
case 0
    sample_type = 'uint8';
case 1
    sample_type = 'uint16';
case 2
    sample_type = 'uint32';
case 3
    sample_type = 'float';
otherwise
    error('data type not supported');
end    
    
    

size_frame      = IM.width * IM.height;
size_data       = size_frame * IM.nFrames;
raw_data        = fread( file, size_data, sample_type, BOS);

if  IM.nFrames == 1 
    
    IM.data = reshape( raw_data, IM.width, IM.height )';
    
else

    header = IM;
    clear IM;
    
    for i = 1 : header.nFrames
                
        frame_data  = raw_data( 1 + (i-1)*size_frame : i*size_frame );
        header.data = reshape( frame_data, header.width, header.height )';
        IM(i) = header;
        
    end
end