function alld = getalldata


for dir = [ 40:45, 72:74 ]
          
    name=sprintf('std%02i', dir );
    for disk = 1:3
        if ( disk == 1 ) cd I:nedelec; end
        if ( disk == 2 ) cd J:nedelec; end
        if ( disk == 3 ) cd K:nedelec; end
        if ( exist(name) == 7 )
            cd( name );
            break;
        end
    end
        
    dat  = load('tel');
      
    adat = zeros( size(dat) + [ 0 1 ] );
    adat(:, 1:size(dat,2) ) = dat;
    adat(:, size(adat,2) )  = dir;

    if ( 0 == exist( 'alld', 'var' ) )
        alld = adat;
    else
        alld = cat( 1, alld, adat );
    end
   
end
