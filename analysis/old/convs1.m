function f = convs1 ( h, c )

% f = conv1( h, c ) compute the convolution of h by c
% defined by f(x) = sum( c(i) * h(x+dx) ) / sum(c);
% the convolved function f has same size as h, and is not shifted.

if ( rem( length(c) + 1, 2 ) ) 
   disp( 'error in convs1: function c must have an odd size' );
   return;
end

dx = [length(c):-1:1] - ( length(c) + 1 )/2;

lower = 1 + max(dx);
upper = length(h) - max(dx);

hs = zeros( size(h) );

for i = 1:length(c);
    
   hs(lower:upper) = hs(lower:upper) + c(i) * h( lower+dx(i):-1:upper+dx(i) );
   
end

sumc = sum( c );

if sumc ~= 1
   f = hs ./ sumc;
else
   f = hs;
end