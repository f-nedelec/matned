function [ ool,pts ] = out_of_lineness2D(PTS,theta)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

cen=mean(PTS,2);
s=size(PTS, 2);
R=[cos(theta) -sin(theta); sin(theta) cos(theta)];
rot = R * ( PTS - cen*ones(1,s) );
ool = sum(abs(rot(2,:)))/s;
end

