function plot_profile(pth, opt)
%
%
% Francois's function to plot the profile of simulated spindles
%
% Nice 18.09.2012; Vienna 10.04.2014; Strasbourg 4.4.2015; HD 29.10.2015

if nargin < 2
    opt = 0;
end

if nargin < 1
    pth = '.';
end


%% only use data after 4000s

data = load_after(fullfile(pth, 'profile.txt'), 4000);

mxx = -data(1,1);
nbb = find(data(:,1)==mxx, 1, 'first');
posX = data(1:nbb, 1);

nbr = fix(size(data,1)/nbb);
lst = nbr * nbb;

left = reshape(data(1:lst,2), nbb, nbr);
right = reshape(data(1:lst,3), nbb, nbr);

%% make plot

global max_pos;

if opt
    figure('Name', pth, 'Position', [100 100 768 256]);
    subplot('Position', [0.07 0.15 0.4 0.7]);
    plot_polymer;
    xlim([-max_pos, max_pos]);
    subplot('Position', [0.55 0.15 0.4 0.7]);
    plot_polarity;
    xlim([-max_pos, max_pos]);
else
    figure('Name', pth);
    subplot(2,1,1);
    plot_polymer;
    xlim([-max_pos, max_pos]);
    subplot(2,1,2)
    plot_polarity;
    xlim([-max_pos, max_pos]);
end

return

%% SUB FUNCTIONS

    function plot_polymer()
        
        hl = plot_curves(posX, left, 'r');
        hr = plot_curves(posX, right, 'b');
        
        title('Polymer density');
        legend([hl; hr], 'left-pointing', 'right-pointing');
        xlabel('Position / um');
        ylabel('polymer density');

    end


    function plot_polarity()
        
        pol = ( right - left ) ./ ( right + left );
        plot_curves(posX, pol, 'k');
        
        if 1
            inx = logical(abs(posX) < 10);
            fit = polyfit(posX(inx), pol(inx), 1);
            % signal with Red background if polarity is aster-like
            if ( fit(1) > 0 )
                set(gcf, 'Color', [1, 0.5, 0.5]);
            end
        end
        
        title('Polymer polarity');
        ylim([-1.1 1.1]);
        xlabel('Position / um');
        ylabel('Polarity');

        return;
        %plot([-15 15], [0.7 0.3], 'b:', 'LineWidth', 3);
        try
            exp = load('brugues_p150.csv');
            expX = exp(:,1)-30;
            expY = 2*exp(:,2)-1;
            
            plot(expX, expY, 'b+', 'LineWidth', 3);
            errorbar(expX, expY, 2*exp(:,4), 'b+', 'LineWidth', 2);
            hold on;
        catch
            fprintf(2, 'no experimental data-file found\n');
        end
    end

end

%% MORE PLOTS

function h = plot_curves(pos, pol, color)

    avg = mean(pol, 2);
    
    if size(pol, 2) > 2
        dev = std(pol, 0, 2);
        % plot surface:
        patchX = cat(1, pos,     flip(pos));
        patchY = cat(1, avg-dev, flip(avg+dev));
    
        patch(patchX, patchY, color, 'FaceAlpha', 0.2, 'EdgeColor', color, 'EdgeAlpha', 0.5);
        %plot(patchX, patchY, '-');
        hold on;
    end
    
    plot(pos, pol, [color,':'], 'LineWidth', 0.3);
    hold on;
    h = plot(pos, avg, [color,'-'], 'LineWidth', 3);
    l = ylim;
    ylim([0, ceil(l(2)/100)*100+200]);
end
