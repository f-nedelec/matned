function result = autovitesse

fichier = dir('*.dat');

result = [];
for i = 1 : size( fichier, 1 )
    
    name = fichier(i).name;
    dat = load(name);
    
    r = vitesse( dat );
    if any( r > 200 )
        disp( ['bizare : ', name] )
    end
    result = cat(1, result, r );
    %disp(['<', name, '> -> ', num2str( size(r,1) ), ' differences']);
    
end

[h, xout] = hist( result );
bar(xout, h);
hold on;
plot( result(:,1), 1:size(result,1), '.');
