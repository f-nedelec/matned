function ismin = filterlocmin( im, n )

% ismin = filterlocmin( im, n )
%
% keep local minimums of the image
% returns the number of neighboor that it's strictly lower to

if ( isfield(im,'data') ) im = im.data; end
if ( nargin < 2 ) n = 1; end

mask = maskcircle1( 2*n + 1 );
mask( n+1, n+1 ) = 0;
[ mdx, mdy ] = find( mask );

mdx = mdx - n - 1;
mdy = mdy - n - 1;
ismin = zeros( size(im) );

for indx = 1:length(mdx)
   
   dx = mdx(indx);
   dy = mdy(indx);
   
   lx = max(1, 1 - dx );
   ux = min(size(im,1), size(im,1) - dx);
   ly = max(1, 1 - dy);
   uy = min(size(im,2), size(im,2) - dy);
      
   ismin(lx:ux, ly:uy) = ismin(lx:ux, ly:uy) +...
      ( im(lx:ux, ly:uy) < im( lx+dx:ux+dx, ly+dy:uy+dy ) );
   
end

return;