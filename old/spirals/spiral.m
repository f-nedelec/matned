function [radius, speed] = spiral(i, debug)

if nargin < 2 
    debug = 0;
end

if ~ exist('x', 'file')
    if ~ exist('result.out', 'file')
        error('result.out not found');
    end
    system('../../analyse0 tube > x');
end
    

data = load('x');
sel  = find( data(:,2) > 256 );
%extract time, x and y:
x = data(sel, [2, 2*i+1, 2*i+2]);

r = sqrt( x(:,2) .* x(:,2) + x(:,3) .* x(:,3));
radius = mean( r );

if debug
    figure;
    plot(x(:,1), r);
    ylabel('radius');
    xlabel('time');
end

n   = size(x,1);
tim = x(n,1) - x(1,1);
%the double of the area covered between each recorded position:
sec = x(1:n-1,2) .* x(2:n,3) - x(1:n-1,3) .* x(2:n,2);

if debug
    figure;
    plot(x(2:n,1), sec);
    ylabel('surface/move');
    xlabel('time');
end

turns = sum(sec) / ( radius * radius * 2 * pi );
speed = turns / tim;

fprintf( 'radius = %7.4f  speed = %8.4f turn/s\n', radius, speed);
